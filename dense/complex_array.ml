module Make
    (Float_array : Basic_intf.Lang.Array
                     with type 'a m = 'a
                      and type elt = float
                      and type index = int) : sig
  type t = { res : Float_array.t; ims : Float_array.t }

  include
    Basic_intf.Lang.Array
      with type 'a m = 'a
       and type elt = Complex.t
       and type t := t
       and type index = Float_array.index
end = struct
  type t = { res : Float_array.t; ims : Float_array.t }

  type 'a m = 'a

  type elt = Complex.t

  type index = Float_array.index

  let length { res; _ } = Float_array.length res

  let make len { Complex.re; im } =
    { res = Float_array.make len re; ims = Float_array.make len im }

  let copy { res; ims } =
    { res = Float_array.copy res; ims = Float_array.copy ims }

  let blit v1 o1 v2 o2 len =
    Float_array.blit v1.res o1 v2.res o2 len ;
    Float_array.blit v1.ims o1 v2.ims o2 len

  let sub v ofs len =
    { res = Float_array.sub v.res ofs len; ims = Float_array.sub v.ims ofs len }

  let unsafe_get { res; ims } i =
    let re = Float_array.unsafe_get res i in
    let im = Float_array.unsafe_get ims i in
    { Complex.re; im }

  let get { res; ims } i =
    let re = Float_array.get res i in
    let im = Float_array.get ims i in
    { Complex.re; im }

  let unsafe_set { res; ims } i { Complex.re; im } =
    Float_array.unsafe_set res i re ;
    Float_array.unsafe_set ims i im

  let set { res; ims } i { Complex.re; im } =
    Float_array.set res i re ;
    Float_array.set ims i im
end
[@@inline]

module Make_std (Float_array : sig
  include
    Basic_intf.Lang.Array
      with type 'a m = 'a
       and type elt = float
       and type index = int

  include Basic_intf.Std with type t := t
end) : sig
  type t = { res : Float_array.t; ims : Float_array.t }

  include
    Basic_intf.Lang.Array
      with type 'a m = 'a
       and type elt = Complex.t
       and type t := t
       and type index = Float_array.index

  include Basic_intf.Std with type t := t
end = struct
  include Make (Float_array)

  let hash (x : t) =
    let acc = ref 0 in
    for i = 0 to length x - 1 do
      acc := Hashtbl.hash (!acc, unsafe_get x i)
    done ;
    !acc

  let pp fmtr (x : t) =
    Format.open_hbox () ;
    for i = 0 to length x - 1 do
      Format.fprintf fmtr "%a@," Basic_impl.Lang.Complex.pp (unsafe_get x i)
    done ;
    Format.close_box ()

  let compare x1 x2 =
    let len1 = length x1 in
    let len2 = length x2 in
    let c = Int.compare len1 len2 in
    if c <> 0 then c
    else
      let exception Break of int in
      try
        for i = 0 to len1 - 1 do
          let f1 = unsafe_get x1 i in
          let f2 = unsafe_get x2 i in
          let c = Basic_impl.Lang.Complex.compare f1 f2 in
          if c <> 0 then raise (Break c) else ()
        done ;
        0
      with Break c -> c

  let equal x1 x2 = compare x1 x2 = 0
end
[@@inline]

module Native = Make_std (Float_array.Native)
module BA = Make_std (Float_array.BA)
