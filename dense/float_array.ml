module Native : sig
  include
    Basic_intf.Lang.Array
      with type 'a m = 'a
       and type t = Float.Array.t
       and type elt = float
       and type index = int

  include Basic_intf.Std with type t := t
end = struct
  type 'a m = 'a

  include Float.Array

  type elt = float

  type index = int

  let hash (x : t) =
    let acc = ref 0 in
    for i = 0 to length x - 1 do
      acc := Hashtbl.hash (!acc, unsafe_get x i)
    done ;
    !acc

  let pp_float fmtr x = Format.fprintf fmtr "%.2f" x

  let pp fmtr (x : t) =
    Format.open_hbox () ;
    for i = 0 to length x - 1 do
      Format.fprintf fmtr "%a " pp_float (unsafe_get x i)
    done ;
    Format.close_box ()

  let compare x1 x2 =
    let len1 = length x1 in
    let len2 = length x2 in
    let c = Int.compare len1 len2 in
    if c <> 0 then c
    else
      let exception Break of int in
      try
        for i = 0 to len1 - 1 do
          let f1 = unsafe_get x1 i in
          let f2 = unsafe_get x2 i in
          let c = Float.compare f1 f2 in
          if c <> 0 then raise (Break c) else ()
        done ;
        0
      with Break c -> c

  let equal x1 x2 = compare x1 x2 = 0
end

module BA : sig
  include
    Basic_intf.Lang.Array
      with type 'a m = 'a
       and type t =
            (float, Bigarray.float64_elt, Bigarray.c_layout) Bigarray.Array1.t
       and type elt = float
       and type index = int

  include Basic_intf.Std with type t := t
end = struct
  open Bigarray

  type 'a m = 'a

  type t = (float, float64_elt, c_layout) Array1.t

  type elt = float

  type index = int

  let length = Array1.dim

  let get = Array1.get

  let unsafe_get = Array1.unsafe_get

  let set = Array1.set

  let unsafe_set = Array1.unsafe_set

  let create_ dim = Array1.create Float64 C_layout dim

  let make dim elt =
    let arr = create_ dim in
    Array1.fill arr elt ;
    arr

  let copy (x : t) =
    let len = length x in
    let res = create_ len in
    Array1.blit x res ;
    res

  let blit (src : t) o1 (dst : t) o2 len =
    let src_overlay = Array1.sub src o1 len in
    let dst_overlay = Array1.sub dst o2 len in
    Array1.blit src_overlay dst_overlay

  let sub (x : t) (ofs : int) (len : int) = Array1.sub x ofs len

  let compare x y =
    let l1 = length x in
    let l2 = length y in
    let c = Int.compare l1 l2 in
    if c <> 0 then c
    else
      let dim = l1 in
      let exception Break of int in
      try
        for i = 0 to dim - 1 do
          let xi = unsafe_get x i in
          let yi = unsafe_get y i in
          let c = Float.compare xi yi in
          if c <> 0 then raise (Break c)
        done ;
        0
      with Break c -> c

  let equal x y = compare x y = 0

  let hash x =
    let dim = length x in
    let acc = ref 0 in
    for i = 0 to dim - 1 do
      let xi = unsafe_get x i in
      acc := Hashtbl.hash (xi, !acc)
    done ;
    !acc

  let pp fmtr x =
    (* TODO: sensible pretty-printing *)
    Format.open_hbox () ;
    for i = 0 to length x - 1 do
      Format.fprintf fmtr "%f@ " (unsafe_get x i)
    done ;
    Format.close_box ()
end
