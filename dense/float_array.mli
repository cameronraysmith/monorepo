module Native : sig
  include
    Basic_intf.Lang.Array
      with type 'a m = 'a
       and type t = Float.Array.t
       and type elt = float
       and type index = int

  include Basic_intf.Std with type t := t
end

module BA : sig
  include
    Basic_intf.Lang.Array
      with type 'a m = 'a
       and type t =
            (float, Bigarray.float64_elt, Bigarray.c_layout) Bigarray.Array1.t
       and type elt = float
       and type index = int

  include Basic_intf.Std with type t := t
end
