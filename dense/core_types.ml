exception Dimensions_mismatch = Linalg.Intf.Dimensions_mismatch

exception Out_of_bounds = Linalg.Intf.Out_of_bounds
