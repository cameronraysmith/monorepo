open Linalg
module G = Grad.Float

let _cube =
  let open G.Generic_dimension in
  let x = var ~tag:"var" ~dimension:1 in
  mul x (mul x x)

let rosenbrock v =
  let open G.Generic_dimension in
  let x = proj v 0 in
  let y = proj v 1 in
  let first_term =
    let one = const (Vec.Float.const Linalg.Tensor.Int.scalar 1.0) in
    let a = sub one x in
    mul a a
  in
  let second_term =
    let b = sub y (mul x x) in
    mul (const (Vec.Float.const Linalg.Tensor.Int.scalar 100.0)) (mul b b)
  in
  add first_term second_term

let () =
  let oc = open_out "graph.dot" in
  G.Export.to_dot ~mode:G.Export.Full ~input_dim:2 rosenbrock oc ;
  close_out oc
