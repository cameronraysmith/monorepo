(* ------------------------------------------------------------------------- *)
(* Computing second derivatives *)

module G = Grad.Float
module Ring = G.Scalar
module BL = Basic_impl.Lang
module Arr = BL.Make_array (Ring)
module G2 = Grad.Native_impl.Make (Ring) (BL.Make_storage (Ring)) (Arr)

let term (x : G2.Scalar.t) : G2.Scalar.t =
  let open G2.Scalar in
  let y = mul x x in
  let y = mul x y in
  y

let _debug () =
  let f x = G2.Scalar.to_generic (term (G2.Scalar.of_generic x)) in
  let oc = open_out "/tmp/term.dot" in
  G2.Export.to_dot ~mode:G2.Export.Full ~input_dim:1 f oc ;
  close_out oc

let (forward, first_derivative) = G2.generate_ss term

let _debug () =
  let f x = G.Scalar.to_generic (first_derivative (G.Scalar.of_generic x)) in
  let oc = open_out "/tmp/derivative.dot" in
  G.Export.to_dot ~mode:G.Export.Full ~input_dim:1 f oc ;
  close_out oc

let _debug () =
  let f x = G.Scalar.to_generic (forward (G.Scalar.of_generic x)) in
  let oc = open_out "/tmp/forward.dot" in
  G.Export.to_dot ~mode:G.Export.Full ~input_dim:1 f oc ;
  close_out oc

let (forward, _) = G.generate_ss forward

let (first_derivative, second_derivative) = G.generate_ss first_derivative

let inputs = Array.to_list @@ Array.init 100 (fun i -> float_of_int i)

let outputs = List.map forward inputs

let outputs' = List.map first_derivative inputs

let outputs'' = List.map second_derivative inputs

let data x y =
  Plot.Data.of_list (List.map (fun (x, y) -> Plot.r2 x y) (List.combine x y))

let () =
  let open Plot in
  let target = png ~png_file:"double_derivative" () in
  run
    ~target
    exec
    (plot2
       ~xaxis:"x"
       ~yaxis:"y"
       ~title:"double derivative"
       [ Line.line_2d ~points:(data inputs outputs) ();
         Line.line_2d ~points:(data inputs outputs') ();
         Line.line_2d ~points:(data inputs outputs'') () ])
