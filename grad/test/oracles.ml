module T = Proptest
module G = Grad.Rational

let rec pow x n =
  if n = 0 then Q.one else if n = 1 then x else Q.mul x (pow x (n - 1))

let array_eq eq x y =
  if Array.length x <> Array.length y then false
  else
    let exception Break in
    try
      for i = 0 to Array.length x - 1 do
        if eq x.(i) y.(i) then () else raise Break
      done ;
      true
    with Break -> false

let ( * ) = G.Scalar.mul

let ( + ) = G.Scalar.add

let ( - ) x y = G.Scalar.(add x (neg y))

let near_enough ~loc ?(name = "no-name") target_x target_y arr =
  let x = Float.Array.get arr 0 in
  let y = Float.Array.get arr 1 in
  if
    Basic_impl.Reals.Float.(
      abs_float (x -. target_x) < 0.01 && abs_float (y -. target_y) < 0.01)
  then ()
  else (
    Format.eprintf
      "%s: test \"%s\" failed, expected (%f, %f), got (%f, %f)@."
      loc
      name
      target_x
      target_y
      x
      y ;
    assert false)

module Rosenbrock = struct
  let f (v : G.Vector.t) =
    let open G.Scalar in
    let x = G.Vector.get v 0 in
    let y = G.Vector.get v 1 in
    let first_term =
      let a = one - x in
      a * a
    in
    let second_term =
      let b = y - (x * x) in
      inj (Q.of_int 100) * (b * b)
    in
    first_term + second_term

  let (f, grad) = G.generate_vs ~input_dim:2 f

  let rosenbrock x y =
    let open Q in
    pow (one - x) 2 + (of_int 100 * pow (y - pow x 2) 2)

  let rosenbrock_grad pos =
    let open Q in
    let x = pos.(0) in
    let y = pos.(1) in
    let two = of_int 2 in
    let two_hundred = of_int 200 in
    [| two * ((two_hundred * pow x 3) - (two_hundred * x * y) + x - one);
       two_hundred * (y - pow x 2)
    |]

  let () =
    T.Generic.add_test
      "rosenbrock value"
      Crowbar.(
        map [T.Generators.Q.gen; T.Generators.Q.gen] @@ fun x y ->
        let pos = [| x; y |] in
        let ev_f = f pos in
        Q.equal ev_f (rosenbrock x y))

  let () =
    T.Generic.add_test
      "rosenbrock grad"
      Crowbar.(
        map [T.Generators.Q.gen; T.Generators.Q.gen] @@ fun x y ->
        let grad_storage = [| Q.zero; Q.zero |] in
        let pos = [| x; y |] in
        grad pos grad_storage ;
        let ev_grad = rosenbrock_grad pos in
        array_eq Q.equal ev_grad grad_storage)
end
