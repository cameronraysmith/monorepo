(** Specializing {!Graph.Make} to native execution. *)

module Native = Basic_impl.Lang

(** [Node_state] implements a bijection between the type [Graph.state]
    and a subset of the integers. *)
module Node_state = struct
  type t = Graph.state

  let all = [| Graph.Invalid; Graph.Valid |]

  let enum (x : t) = match x with Graph.Invalid -> 0 | Graph.Valid -> 1
end

(** Instantiate algodiff for native execution *)

(** [Node_state_enum] provides primitives for dispatching over node states. *)
module Node_state_enum = Native.Make_enum (Node_state)

(** [Node_state_storage] provides primitives for mutable storage cells
    storing values of type [Graph.state]. *)
module Node_state_storage = Native.Make_storage (Node_state)

(** [Make] specializes {!Graph.Make} to native execution. *)
module Make
    (R : Basic_intf.Lang.Ring with type 'a m = 'a)
    (R_storage : Basic_intf.Lang.Storage with type 'a m = 'a and type elt = R.t)
    (A : Basic_intf.Lang.Array
           with type 'a m = 'a
            and type index = int
            and type elt = R.t) =
  Graph.Make (Native.Empty) (Native.Codegen) (Linalg.Tensor.Int) (Native.Bool)
    (R)
    (R_storage)
    (Node_state_enum)
    (Node_state_storage)
    (Native.Int)
    (Native.Int)
    (Native.Make_const (Native.Int))
    (Native.Exn)
    (Native.Sequencing)
    (Native.Lambda)
    (Native.Product)
    (A)
[@@inline]

(** OK *)
module Float =
  Make (Native.Float) (Native.Float_storage) (Dense.Float_array.Native)

(** Ok *)
module Rational =
  Make
    (Native.Rational)
    (Native.Make_storage (Native.Rational))
    (Native.Make_array (Native.Rational))
