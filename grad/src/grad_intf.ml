(** Types and module types exported by [Grad] *)

(** Module type implemented by [Graph.Make] *)
module type S = sig
  (** The type of the monad handling code generation. *)
  type 'a k

  (** The type of programs evaluating to ['a]. *)
  type 'a m

  (** The type of shapes. *)
  type 'a shape

  (** The type of scalars. *)
  type elt

  (** The type of integer indexes. *)
  type index

  (** The type of arrays. *)
  type arr

  type vec := (index shape, index m, elt m) Linalg.Intf.vec

  type mat := ((index * index) shape, (index * index) m, elt m) Linalg.Intf.vec

  (** [node] is the internal type for computational graphs.

      Values of this type should not be directly manipulated by the user, except when
      introducing new kind of nodes using [create]. *)
  type node

  (** The output dimension of a node can depend in a non-trivial way of the
      dimension of some input. The [Output_dim] module contains the
      few ways one can specify this relationship. *)
  module Output_dim : sig
    (** The type of an output dimension specification. *)
    type t

    (** [same_as n] specifies that the output dimension is equal
        to the dimension of [backward.(n)] (see [create]) *)
    val same_as : int -> t

    (** [sum] specifies that the output dimension is equal to the
        sum of all input dimensions.  *)
    val sum : t

    (** [one] specifies that the output dimension is equal to [one]
        (that is, the node is scalar-valued). *)
    val one : t

    (** [dim n] specifies that the output dimension is equal to [n.] *)
    val dim : index m -> t

    (** [custom f] specifies that the output dimension is equal to [f]
        applied to the vector of dimensions of elements of [backward]
        (see [create]). *)
    val custom : (index m array -> index m) -> t
  end

  (** [create ~tag ~backward ~transfer ~jacobian ~output_dim] creates a new node.
      - [tag] allows to specify a name for the node, useful for debugging.
      - [backward] is the array of upstream nodes that feed into the node we're creating.
      - [transfer] specifies what the function computes when evaluated. [transfer] is called
        with an an array of length equal to that of [backward]. Each vector in the array
        passed to [transfer] has dimension corresponding to that of the corresponding
        element of [backward].
      - [jacobian] is the jacobian associated to [transfer].
      - [output_dim] specifies the output dimension of the node (this can't be guessed from the
        type of [transfer] alone).

      @raise [Invalid_output_dimension_specification] if [output_dim = Same_as { index }]
      with [index] not in the range of [backward]. *)
  val create :
    tag:string ->
    backward:node array ->
    transfer:(vec array -> vec k) ->
    jacobian:(vec array -> mat k) ->
    output_dim:Output_dim.t ->
    node

  (** Same as [create] except the [transfer] and [jacobian] are
      given direct access to the underlying data. *)
  val unsafe_create :
    tag:string ->
    backward:node array ->
    transfer:(inp:arr m array -> out:arr m -> unit m k) ->
    jacobian:(arr m array -> mat k) ->
    output_dim:Output_dim.t ->
    node

  (** [Generic_dimension] provides some low-level combinators for creating computational graphs.
      These primitives are meant for low-level use, exposing them to the user is highly
      discouraged.
   *)
  module Generic_dimension : sig
    (** Constant node. Should be interpreted as the constant function,
        always equal to the given vector overlay. *)
    val const : ?tag:string -> vec -> node

    (** Variable node. For {b internal use only}. *)
    val var : tag:string -> dimension:index m -> node

    (** Pointwise addition. *)
    val add : ?tag:string -> node -> node -> node

    (** Pointwise subtraction. *)
    val sub : ?tag:string -> node -> node -> node

    (** Pointwise multiplication. *)
    val mul : ?tag:string -> node -> node -> node

    (** Pointwise negation. *)
    val neg : ?tag:string -> node -> node

    (** Project out a given component of a node at the specified index. *)
    val proj : ?tag:string -> node -> index m -> node

    (** From a function from nodes to nodes, generate the forward and backward
        implementation in the target language. *)
    val generate :
      input_dim:index m ->
      (node -> node k) ->
      ((arr -> arr -> unit) * (arr -> arr -> unit)) m k
  end

  (** This modules implements a ring of scalars. Functions defined on this ring
      are differetiable by construction.

      We'll call elements of this ring "differentiable scalars" for convenience. *)
  module Scalar : sig
    (** The type of differentiable scalars *)
    type t

    (** Inject a constant as a differentiable scalar. *)
    val inj : elt m -> t

    (** Get the current value of a differentiable scalar. *)
    val prj : t -> elt m

    (** Cast to the generic type of nodes. *)
    val to_generic : t -> node

    (** Cast from the generic type of nodes.

        @raise Invalid_argument if the output dimension of the given node is not
        equal to [I_ring.one]. *)
    val of_generic : node -> t

    (** Basic scalar operations. *)
    include Basic_intf.Lang.Ring with type 'a m = 'a and type t := t
  end

  (** This modules implements pointwise operations over vectors.
      Functions defined using these operations are differentiable by construction.

      We'll call elements of this ring "differentiable vectors" for convenience. *)
  module Vector : sig
    (** The type of differentiable vectors. *)
    type t

    (** Inject an array as a differentiable vector. *)
    val inj : arr m -> t

    (** Get the current value of a differentiable vector. *)
    val prj : t -> arr m

    (** Inject a vector overlay as a constant differentiable vector.
        Should be interpreted as a constant function, always equal
        to the value specified by the given vector overlay. *)
    val const : vec -> t

    (** [zero n] behaves as a constant function equal to the [n]-dimensional
        zero vector. *)
    val zero : index m -> t

    (** [one n] behaves as a constant function equal to the [n]-dimensional
        vector with all components equal to one. *)
    val one : index m -> t

    (** Pointwise addition.

        @raise Linalg.Intf.Dimensions_mismatch if the given arguments do not have the same dimensions.
     *)
    val add : t -> t -> t

    (** Pointwise subtraction.

        @raise Linalg.Intf.Dimensions_mismatch if the given arguments do not have the same dimensions.
     *)
    val sub : t -> t -> t

    (** Pointwise negation. *)
    val neg : t -> t

    (** Pointwise multiplication.

        @raise Linalg.Intf.Dimensions_mismatch if the given arguments do not have the same dimensions.
     *)
    val mul : t -> t -> t

    (** Project out an differentiable scalar out of a differentiable vector at the
        specified index.

        @raise Linalg.Intf.Out_of_bounds if the index is not in the [0, dim-1] interval, where [dim]
        is the output dimension of the differentiable vector.
     *)
    val get : t -> index m -> Scalar.t

    (** Concatenate two vectors. *)
    val concat : t -> t -> t

    (** Restrict a vector to the sub-vector of length [len]
        starting at offset [ofs]. *)
    val restrict : ofs:index m -> len:index m -> t -> t

    (** Cast to the generic type of nodes. *)
    val to_generic : t -> node

    (** Cast from the generic type of nodes.

        @raise Invalid_argument if the output dimension of the given node is not
        equal to [I_ring.one]. *)
    val of_generic : node -> t

    (** Allow this vector implementation to comply to some signatures in [Basic_intf] *)
    type 'a m = 'a
  end

  (** Export the structure of a computational graph.

      Dev note: this functionality is still extremely basic and mostly useful for debugging purposes. *)
  module Export : sig
    type node_info = { tag : string; id : int; output_dim : index m }

    (** If [Backward_only] is given to [compute_graph], only the nodes upstream of the initial
        argument are returned in the resulting graph.

        If [Full] is specified, the full connected component is produced.*)
    type mode = Backward_only | Full

    type graph

    val compute_graph : mode -> node -> graph

    val to_adjacency_list : graph -> (node_info * node_info list) array

    val to_dot :
      ?name:string ->
      mode:mode ->
      input_dim:index m ->
      (node -> node) ->
      out_channel ->
      unit
  end

  (** Generate forward and backward implementation for a scalar-to-scalar function. *)
  val generate_ss :
    (Scalar.t -> Scalar.t k) -> ((elt -> elt) * (elt -> elt)) m k

  (** Generate forward and backward implementation for a scalar-to-vector function. *)
  val generate_sv :
    (Scalar.t -> Vector.t k) ->
    ((elt -> arr -> unit) * (elt -> arr -> unit)) m k

  (** Generate forward and backward implementation for a vector-to-scalar function. *)
  val generate_vs :
    input_dim:index m ->
    (Vector.t -> Scalar.t k) ->
    ((arr -> elt) * (arr -> arr -> unit)) m k

  (** Generate forward and backward implementation for a vector-to-vector function. *)
  val generate_vv :
    input_dim:index m ->
    (Vector.t -> Vector.t k) ->
    ((arr -> arr -> unit) * (arr -> arr -> unit)) m k
end
