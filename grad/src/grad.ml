(** The [prbnmcn-grad] algorithmic differentiation library. *)

(** {1:introduction Introduction}

    [prbnmcn-grad] ([grad] for short) is a library allowing to
    perform algorithmic differentiation. The library is functorized
    over the typed syntax of the language in which the code computing
    the jacobians is produced, as well as over the base ring of scalars.
    - Jacobians can be executed natively when the syntax is implemented directly
      using [OCaml] constructs.
    - Code generation is obtained when implementing the syntax using
      some target language's AST or IR.
    - Computing higher-order derivatives is achieved by iterating
      functor applications.

    [grad] provides a functor {!Make} allowing to instantiate these schemes.
    For convenience, the [Float] and [Rational] modules implement
    algorithmic differentiation over the rings of floats and (arbitrary-precision)
    rational numbers. *)

module Graph = Graph

(** Generic backward-mode jacobian computation. *)
module Make = Graph.Make

(** Module type produced by {!Make}  *)
module Intf = Grad_intf

module Native_impl = Native_impl

(** Native AD based on the ring of floats, using dense arrays. *)
module Float = Native_impl.Float

(** Native AD based on the ring of arbitrary precision rationals, using dense arrays. *)
module Rational = Native_impl.Rational
