#include <caml/mlvalues.h>
#include <caml/memory.h>
#include <caml/alloc.h>
#include <caml/custom.h>
#include <caml/bigarray.h>
#include <caml/fail.h>

#include <math.h>
#include <complex.h>
#include <stdio.h>
#include <assert.h>


#define Treal double

#if defined(__STRICT_ANSI__)
#define OWL_INLINE __inline__
#else
#define OWL_INLINE inline
#endif

// calculate the number of elements given a bigarray
int c_ndarray_numel (struct caml_ba_array *X) {
     int n = 1;

     for (int i = 0; i < X->num_dims; i ++)
          n *= X->dim[i];

     return n;
}

// calculate the stride of a given dimension of a bigarray
int c_ndarray_stride_dim (struct caml_ba_array *X, int d) {
     int s = 1;

     for (int i = X->num_dims - 1; i > d; i--)
          s *= X->dim[i];

     return s;
}

// calculate the slice size of a given dimension of a bigarray
int c_ndarray_slice_dim (struct caml_ba_array *X, int d) {
     int s = 1;

     for (int i = X->num_dims - 1; i >= d; i--)
          s *= X->dim[i];

     return s;
}

// calculate the stride size of all dimensions of a bigarray
void c_ndarray_stride (struct caml_ba_array *X, int *stride) {
     int i = X->num_dims - 1;
     *(stride + i) = 1;

     for ( ; i > 0; i--)
          *(stride + i - 1) = *(stride + i) * X->dim[i];
}

// copy x to y with given offset and stride
OWL_INLINE void owl_float64_copy (int N, double* x, int ofsx, int incx, double* y, int ofsy, int incy) {
     for (int i = 0; i < N; i++) {
          *(y + ofsy) = *(x + ofsx);
          ofsx += incx;
          ofsy += incy;
     }
}

void owl_float64_copy (int N, double* x, int ofsx, int incx, double* y, int ofsy, int incy);

// copy x to y with given offset and stride
OWL_INLINE void owl_complex64_copy (int N, _Complex double* x, int ofsx, int incx, _Complex double* y, int ofsy, int incy) {
     for (int i = 0; i < N; i++) {
          *(y + ofsy) = *(x + ofsx);
          ofsx += incx;
          ofsy += incy;
     }
}

void owl_complex64_copy (int N, _Complex double* x, int ofsx, int incx, _Complex double* y, int ofsy, int incy);

#define COMPLEX_COPY owl_complex64_copy
#define REAL_COPY owl_float64_copy

#include "owl_fftpack_impl.h"
