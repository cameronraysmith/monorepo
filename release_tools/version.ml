type cnstrnt = Lt | Le | Eq | Gt | Ge

type t = cnstrnt * string

let cnstrnt_to_string (c : cnstrnt) =
  match c with Lt -> "<" | Le -> "<=" | Eq -> "=" | Gt -> ">" | Ge -> ">="

let of_string s =
  let open Sexplib in
  let sexp = Sexp.of_string s in
  match sexp with
  | Sexp.(List [Atom ("<" | "<=" | "=" | ">" | ">="); Atom _v]) ->
      sexp (* TODO check that _v is a version *)
  | _ -> invalid_arg "Version.of_string"

let to_sexp (c, s) =
  let open Sexplib in
  Sexp.(List [Atom (cnstrnt_to_string c); Atom s])
