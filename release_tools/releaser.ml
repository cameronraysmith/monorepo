let global =
  Dune_file.
    { authors = "Ilias Garnier";
      email = "igarnier@protonmail.com";
      github_root = "igarnier";
      version = "0.0.1"
    }

let write_back path sexps =
  Helpers.with_out_channel path ~flags:[Open_trunc; Open_append] ~f:(fun oc ->
      let fmtr = Format.formatter_of_out_channel oc in
      Format.pp_print_list
        ~pp_sep:(fun fmtr () -> Format.fprintf fmtr "@.")
        (Sexplib.Sexp.pp_hum_indent 2)
        fmtr
        sexps ;
      Format.fprintf fmtr "@.")

let normalize directory =
  List.iter
    (fun x ->
      match x with
      | Files.Opam _ -> ()
      | Files.Dune _ -> ()
      | Files.Dune_project path -> (
          let sexps =
            Helpers.with_in_channel path ~f:Sexplib.Sexp.input_sexps
          in
          match Dune_file.name_of_project sexps with
          | None -> Format.eprintf "Ignoring %s@." path
          | Some name ->
              Format.eprintf "Processing %s@." path ;
              let project = { Dune_file.name } in
              let sexps =
                Dune_file.normalize_dune_project global project sexps
              in
              write_back path sexps))
    (Files.search directory)

let rename_dune ~subst ~dune_file =
  let dune = Helpers.with_in_channel dune_file ~f:Sexplib.Sexp.input_sexps in
  write_back dune_file (Dune_file.rename_dune ~dune ~subst)

let rename_dune_project ~subst ~dune_project_file =
  let dune_project =
    Helpers.with_in_channel dune_project_file ~f:Sexplib.Sexp.input_sexps
  in
  write_back
    dune_project_file
    (Dune_file.rename_dune_project ~dune_project ~subst)

let parse_line s =
  match String.split_on_char ' ' s with
  | [x; y] -> (x, y)
  | _ -> invalid_arg (Printf.sprintf "parse_line: %s" s)

let load_map ~file =
  let lines = ref [] in
  Helpers.with_in_channel file ~f:(fun ic ->
      try
        while true do
          lines := input_line ic :: !lines
        done ;
        assert false
      with End_of_file -> List.rev !lines |> List.map parse_line)

let () =
  match List.tl @@ Array.to_list Sys.argv with
  | ["normalize"; directory] ->
      Format.eprintf
        "Normalizing dune-project files recursively from directory %s@."
        directory ;
      normalize directory
  | ["rename"; directories; "using"; file] ->
      Format.eprintf
        "Performing renaming using subst file %s on directory(ies) %s@."
        file
        directories ;
      let subst = load_map ~file in
      let directories = String.split_on_char ',' directories in
      List.iter
        (fun directory ->
          let targets = Files.search directory in
          List.iter
            (fun target ->
              match target with
              | Files.Opam opam -> Unix.unlink opam
              | Files.Dune dune_file -> rename_dune ~subst ~dune_file
              | Files.Dune_project dune_project_file ->
                  rename_dune_project ~subst ~dune_project_file)
            targets ;
          normalize directory)
        directories
  | ["remove"; "dependency"; name; "in"; directory] ->
      let targets = Files.search directory in
      List.iter
        (fun target ->
          match target with
          | Files.Opam _ -> ()
          | Files.Dune _ -> ()
          | Files.Dune_project dune_project_file ->
              let dune_project =
                Helpers.with_in_channel
                  dune_project_file
                  ~f:Sexplib.Sexp.input_sexps
              in
              let dune_project =
                Dune_file.remove_dependency name dune_project
              in
              write_back dune_project_file dune_project)
        targets
  | ["add"; "dependency"; name; "constraint"; op; version; "in"; directory] ->
      let targets = Files.search directory in
      List.iter
        (fun target ->
          match target with
          | Files.Opam _ -> ()
          | Files.Dune _ -> ()
          | Files.Dune_project dune_project_file ->
              let dune_project =
                Helpers.with_in_channel
                  dune_project_file
                  ~f:Sexplib.Sexp.input_sexps
              in
              let dune_project =
                Dune_file.add_dependency
                  name
                  (Version.of_string (Printf.sprintf "(%s %s)" op version))
                  dune_project
              in
              write_back dune_project_file dune_project)
        targets
  | ["homepage"; dune_project_file] -> (
      let dune_project =
        Helpers.with_in_channel dune_project_file ~f:Sexplib.Sexp.input_sexps
      in
      match Dune_file.get1 Homepage dune_project with
      | Some homepage -> Format.printf "%s%!" homepage
      | None -> exit 1)
  | ["name"; dune_project_file] -> (
      let dune_project =
        Helpers.with_in_channel dune_project_file ~f:Sexplib.Sexp.input_sexps
      in
      match Dune_file.get1 Name dune_project with
      | Some homepage -> Format.printf "%s%!" homepage
      | None -> exit 1)
  | args ->
      Format.eprintf "unrecognized command <%s>@." (String.concat "_" args) ;
      exit 1
