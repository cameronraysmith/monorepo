open Sexplib

let sf = Printf.sprintf

let dune_version = "2.8"

type key =
  | Source
  | Bug_reports
  | License
  | Authors
  | Maintainers
  | Package
  | Version
  | Generate_opam_file
  | Name
  | Public_name
  | Homepage
  | Lang
  | Use_standard_c_flags

let key_all =
  [ Source;
    Bug_reports;
    License;
    Authors;
    Maintainers;
    Package;
    Version;
    Generate_opam_file;
    Name;
    Public_name;
    Homepage;
    Lang;
    Use_standard_c_flags ]

let dune_project_keys =
  [ Source;
    Bug_reports;
    License;
    Authors;
    Maintainers;
    Package;
    Version;
    Generate_opam_file;
    Name;
    Homepage;
    Lang;
    Use_standard_c_flags ]

let dune_keys = [Name; Public_name]

let show_key (k : key) =
  match k with
  | Source -> "source"
  | Bug_reports -> "bug_reports"
  | License -> "license"
  | Authors -> "authors"
  | Maintainers -> "maintainers"
  | Package -> "package"
  | Version -> "version"
  | Generate_opam_file -> "generate_opam_files"
  | Name -> "name"
  | Public_name -> "public_name"
  | Homepage -> "homepage"
  | Lang -> "lang"
  | Use_standard_c_flags -> "use_standard_c_and_cxx_flags"

let key_of_string s =
  match s with
  | "source" -> Some Source
  | "bug_reports" -> Some Bug_reports
  | "license" -> Some License
  | "authors" -> Some Authors
  | "maintainers" -> Some Maintainers
  | "package" -> Some Package
  | "version" -> Some Version
  | "generate_opam_file" -> Some Generate_opam_file
  | "name" -> Some Name
  | "public_name" -> Some Public_name
  | "homepage" -> Some Homepage
  | "lang" -> Some Lang
  | _ -> None

type error = Missing of key * Sexp.t | Invalid of key * Sexp.t

exception Missing_field of key

type global =
  { authors : string; email : string; github_root : string; version : string }

type dune_project = { name : string }

(* ------------------------------------------------------------------------- *)
(* canonical fields *)

let homepage global dune_project =
  Sexp.List
    [ Atom (show_key Homepage);
      Conv.sexp_of_string
        (sf "http://github.com/%s/%s" global.github_root dune_project.name) ]

let bug_reports global dune_project =
  Sexp.List
    [ Atom (show_key Bug_reports);
      Conv.sexp_of_string
        (sf "http://github.com/%s/%s" global.github_root dune_project.name) ]

let source global dune_project =
  Sexp.List
    [ Atom (show_key Source);
      List
        [ Atom "uri";
          Conv.sexp_of_string
            (sf
               "git+https://github.com/%s/%s"
               global.github_root
               dune_project.name) ] ]

let license = Sexp.List [Atom (show_key License); Atom "MIT"]

let authors global =
  Sexp.List [Atom (show_key Authors); Conv.sexp_of_string global.authors]

let maintainers global =
  Sexp.List [Atom (show_key Maintainers); Conv.sexp_of_string global.email]

let version global =
  Sexp.List [Atom (show_key Version); Conv.sexp_of_string global.version]

let lang = Sexp.List [Atom (show_key Lang); Atom "dune"; Atom dune_version]

let generate_opam_files =
  Sexp.List [Atom (show_key Generate_opam_file); Conv.sexp_of_bool true]

let standard_c_flags =
  Sexp.List [Atom (show_key Use_standard_c_flags); Conv.sexp_of_bool true]

let canonical_field global dune_project (k : key) =
  match k with
  | Source -> Some (source global dune_project)
  | Bug_reports -> Some (bug_reports global dune_project)
  | License -> Some license
  | Authors -> Some (authors global)
  | Maintainers -> Some (maintainers global)
  | Version -> Some (version global)
  | Generate_opam_file -> Some generate_opam_files
  | Package -> None
  | Name -> None
  | Public_name -> Some (Conv.sexp_of_string dune_project.name)
  | Homepage -> Some (homepage global dune_project)
  | Lang -> Some lang
  | Use_standard_c_flags -> Some standard_c_flags

(* ------------------------------------------------------------------------- *)

let assoc k items =
  List.find_opt
    (fun sexp ->
      match Path.get ~path:[Path.Pos 0] sexp with
      | exception _ -> false
      | Atom n when String.equal n (show_key k) -> true
      | _ -> false)
    items

let modify key items f =
  List.filter_map
    (fun sexp ->
      match sexp with
      | Sexp.List (Atom n :: rest) when String.equal n key ->
          Option.map (fun rest -> Sexp.List (Atom n :: rest)) (f rest)
      | _ -> Some sexp)
    items

let get1 k items =
  Option.map
    (function Sexp.List [_; Atom value] -> value | _ -> assert false)
    (assoc k items)

(* ------------------------------------------------------------------------- *)
(* Filtering out/adding dependencies in dune-project *)

let remove_dependency name items =
  modify (show_key Package) items @@ fun package_fields ->
  Option.some
  @@ modify "depends" package_fields
  @@ fun dependencies ->
  let remaining_dependencies =
    List.filter
      (fun sexp ->
        match sexp with
        | Sexp.List (Atom dep_name :: _) -> not (String.equal name dep_name)
        | _ -> assert false)
      dependencies
  in
  match remaining_dependencies with
  | [] -> None
  | _ -> Some remaining_dependencies

let add_dependency name version items =
  let sexp = Sexp.List [Atom name; version] in
  modify (show_key Package) items @@ fun package_fields ->
  let depends_opt =
    List.find_opt
      (fun sexp ->
        match Path.get ~path:[Path.Pos 0] sexp with
        | exception _ -> false
        | Atom n when String.equal n "depends" -> true
        | _ -> false)
      package_fields
  in
  match depends_opt with
  | Some _ ->
      Option.some
      @@ modify "depends" package_fields
      @@ fun dependencies -> Some (sexp :: dependencies)
  | None -> Option.some @@ package_fields @ [Sexp.List [Atom "depends"; sexp]]

(* ------------------------------------------------------------------------- *)

let compute_actions global dune_project items =
  List.fold_left
    (fun actions k ->
      match assoc k items with
      | None -> (
          match canonical_field global dune_project k with
          | Some canonical -> Missing (k, canonical) :: actions
          | None -> actions)
      | Some field -> (
          match canonical_field global dune_project k with
          | Some canonical ->
              if not (Sexp.equal canonical field) then
                Invalid (k, canonical) :: actions
              else actions
          | None -> actions))
    []
    dune_project_keys

let replace_item k replacement items =
  List.map
    (fun sexp ->
      match Path.get ~path:[Path.Pos 0] sexp with
      | exception _ -> sexp
      | Atom n when String.equal n (show_key k) -> replacement
      | _ -> sexp)
    items

let perform_action actions items =
  List.fold_left
    (fun (new_items, existing) action ->
      match action with
      | Missing (_, replacement) -> (replacement :: new_items, existing)
      | Invalid (k, replacement) ->
          (new_items, replace_item k replacement existing))
    ([], items)
    actions

let normalize_dune_project global dune_project items =
  let actions = compute_actions global dune_project items in
  let (new_items, existing) = perform_action actions items in
  existing @ new_items

let name_of_project items =
  List.find_map
    (fun sexp ->
      match Path.get ~path:[Path.Pos 0] sexp with
      | exception _ -> None
      | Atom n when String.equal n "name" ->
          Some (Path.get ~path:[Path.Pos 1] sexp)
      | _ -> None)
    items
  |> Option.map Conv.string_of_sexp

(* ------------------------------------------------------------------------- *)
(* Renaming dependencies *)

module String_map = Map.Make (String)
module String_set = Set.Make (String)

type map = string String_map.t

let maybe_subst subst s = String_map.find_opt s subst

let rec rename_rec except subst (sexp : Sexp.t) =
  match sexp with
  | Sexp.Atom s -> (
      match maybe_subst subst s with None -> sexp | Some s -> Sexp.Atom s)
  | Sexp.List [] | Sexp.List [_] -> sexp
  | Sexp.List (Atom hd :: _ as sexps) ->
      if String_set.mem hd except then sexp
      else Sexp.List (List.map (rename_rec except subst) sexps)
  | Sexp.List sexps -> Sexp.List (List.map (rename_rec except subst) sexps)

let rename_list except subst sexps = List.map (rename_rec except subst) sexps

let rename_dune ~dune ~subst =
  let subst = String_map.of_seq (List.to_seq subst) in
  let exceptions = String_set.of_list ["name"] in
  rename_list exceptions subst dune

let rename_dune_project ~dune_project ~subst =
  let subst = String_map.of_seq (List.to_seq subst) in
  let exceptions = String_set.empty in
  rename_list exceptions subst dune_project
