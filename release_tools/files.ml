type t = Opam of string | Dune of string | Dune_project of string
[@@deriving show]

let opam_regexp = Str.regexp {|.*\.opam|}

let build_dir_regexp = Str.regexp ".*_build.*"

let load fname =
  Helpers.with_in_channel fname ~f:(fun ic ->
      let buffer = Buffer.create 8192 in
      try
        while true do
          Buffer.add_channel buffer ic 8192
        done ;
        assert false
      with End_of_file -> Buffer.contents buffer)

let iter_dir f dirname =
  let open Unix in
  let d = opendir dirname in
  try
    while true do
      f (readdir d)
    done
  with End_of_file -> closedir d

let rec search_rec path acc =
  let open Unix in
  if Str.string_match build_dir_regexp path 0 then ()
  else
    let infos = lstat path in
    match infos.st_kind with
    | S_REG -> (
        match Filename.basename path with
        | "dune" -> acc := Dune path :: !acc
        | "dune-project" -> acc := Dune_project path :: !acc
        | something_else ->
            if Str.string_match opam_regexp something_else 0 then
              acc := Opam path :: !acc
            else ())
    | S_LNK -> ()
    | S_DIR ->
        iter_dir
          (fun file ->
            if
              file <> Filename.current_dir_name
              && file <> Filename.parent_dir_name
            then search_rec (Filename.concat path file) acc)
          path
    | _ -> prerr_endline ("Can't cope with special file " ^ path)

let search root =
  let paths = ref [] in
  search_rec root paths ;
  !paths

let copy =
  let buffer_size = 8192 in
  let buffer = Bytes.create buffer_size in
  fun input_name output_name ->
    let open Unix in
    let fd_in = openfile input_name [O_RDONLY] 0 in
    let fd_out = openfile output_name [O_WRONLY; O_CREAT; O_TRUNC] 0o660 in
    let rec copy_loop () =
      match read fd_in buffer 0 buffer_size with
      | 0 -> ()
      | r ->
          ignore (write fd_out buffer 0 r) ;
          copy_loop ()
    in
    copy_loop () ;
    close fd_in ;
    close fd_out

let move ~src ~dst =
  copy src dst ;
  Unix.unlink src
