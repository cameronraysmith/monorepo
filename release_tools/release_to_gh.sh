#!/bin/bash

set -e

TARGET=$1
VERSION=$2
BRANCH=v$VERSION
REPO=$(dune exec ./release_tools/releaser.exe -- homepage $1/dune-project)

cp -r $TARGET /tmp

cd /tmp/$TARGET

echo "Now in directory $PWD"

git init .

echo "Inited git"

git add .

git status

echo "Preparing commit with message $VERSION"

git commit -m $VERSION

git checkout -b $BRANCH

git tag -a $VERSION

git branch -f trunk $BRANCH

git remote add origin $REPO

dune-release lint

git push --force --atomic origin $BRANCH $VERSION

git push --force origin trunk

opam publish --msg-file=CHANGES.md
