let with_in_channel ~f name =
  match open_in name with
  | exception (Sys_error s as exn) ->
      Format.eprintf "with_in_channel: error when opening %s (%s)" name s ;
      raise exn
  | oc -> (
      try
        let x = f oc in
        close_in oc ;
        x
      with exn ->
        close_in oc ;
        raise exn)

let with_out_channel ?(flags = []) ?(mode = 644) ~f name =
  match open_out_gen flags mode name with
  | exception (Sys_error s as exn) ->
      Format.eprintf "with_out_channel: error when opening %s (%s)" name s ;
      raise exn
  | ic -> (
      try
        let x = f ic in
        close_out ic ;
        x
      with exn ->
        close_out ic ;
        raise exn)

let file_copy =
  let buffer_size = 8192 in
  let buffer = Bytes.create buffer_size in
  fun input_name output_name ->
    let open Unix in
    let fd_in = openfile input_name [O_RDONLY] 0 in
    let fd_out = openfile output_name [O_WRONLY; O_CREAT; O_TRUNC] 0o660 in
    let rec copy_loop () =
      match read fd_in buffer 0 buffer_size with
      | 0 -> ()
      | r ->
          ignore (write fd_out buffer 0 r) ;
          copy_loop ()
    in
    copy_loop () ;
    close fd_in ;
    close fd_out
