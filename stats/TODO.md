# TODO
- categorical: use array of bindings instead of list
- Bernouilli -> Bernoulli
- More tests
- More `Gen.t` & pdfs: gamma, poisson, beta, simplex, dirichlet
- Use `linalg` for `Fin`
- Graph module: split it in dedicated lib?
- Use flat float arrays
