## 0.0.2
- Introduce dependency on `basic-structures.0.0.2`
- Add type `pos` in `Intf.Tensor`
- Introduce `Tensor.unsafe_sub`
- Introduce `Vec.Make_core, Vec.Make_core_native`

## 0.0.1
- First release
