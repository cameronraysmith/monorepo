prbnmcn-simplifier
------------------

This library implements some basic optimization passes on the intermediate
language used by the `prbnmcn-*` family of packages.
