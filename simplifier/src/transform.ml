open Basic_intf.Lang

module Map (Domain : sig
  type 'a m
end) (Range : sig
  type 'a m
end) (T : functor (X : Empty with type 'a m = 'a Domain.m) -> sig
  type 'a m = 'a Range.m

  val inj : 'a X.m -> 'a m

  val prj : 'a m -> 'a X.m
end) =
struct
  module Sequencing (X : Sequencing with type 'a m = 'a Domain.m) :
    Sequencing with type 'a m = 'a T(X).m = struct
    include T (X)

    let seq m f = inj (X.seq (prj m) (fun () -> prj (f ())))

    let ( let* ) m f =
      inj
        X.(
          let* x = prj m in
          prj (f (inj x)))

    let unit = inj X.unit
  end

  module Loop (X : Loop with type 'a m = 'a Domain.m) :
    Loop with type 'a m = 'a T(X).m and type index = X.index = struct
    include T (X)

    type index = X.index

    let for_ ~start ~stop body =
      inj
        (X.for_ ~start:(prj start) ~stop:(prj stop) (fun index ->
             prj (body (inj index))))

    let while_ ~cond body =
      inj (X.while_ ~cond:(fun () -> prj (cond ())) (fun () -> prj (body ())))
  end

  module Const (X : Const with type 'a m = 'a Domain.m) :
    Const with type 'a m = 'a T(X).m and type t = X.t = struct
    include T (X)

    type t = X.t

    let const x = inj (X.const x)
  end

  module Lambda (X : Lambda with type 'a m = 'a Domain.m) :
    Lambda with type 'a m = 'a T(X).m = struct
    include T (X)

    let lam f = inj (X.lam (fun x -> prj (f (inj x))))

    let app f x = inj (X.app (prj f) (prj x))
  end

  module Product (X : Product with type 'a m = 'a Domain.m) :
    Product with type 'a m = 'a T(X).m = struct
    include T (X)

    let prod l r = inj (X.prod (prj l) (prj r))

    let fst p = inj (X.fst (prj p))

    let snd p = inj (X.snd (prj p))
  end

  module Storage (X : Storage with type 'a m = 'a Domain.m) :
    Storage with type 'a m = 'a T(X).m and type t = X.t and type elt = X.elt =
  struct
    include T (X)

    type t = X.t

    type elt = X.elt

    let create elt = inj (X.create (prj elt))

    let set r x = inj (X.set (prj r) (prj x))

    let get r = inj (X.get (prj r))
  end

  module Bool (X : Bool with type 'a m = 'a Domain.m) :
    Bool with type 'a m = 'a T(X).m = struct
    include T (X)

    let true_ = inj X.true_

    let false_ = inj X.false_

    let ( || ) x y = inj X.(prj x || prj y)

    let ( && ) x y = inj X.(prj x && prj y)

    let dispatch b f = inj (X.dispatch (prj b) (fun b -> prj (f b)))
  end

  module Enum (X : Enum with type 'a m = 'a Domain.m) :
    Enum with type 'a m = 'a T(X).m and type t = X.t = struct
    include T (X)

    type t = X.t

    let all = X.all

    let enum = X.enum

    let const x = inj (X.const x)

    let dispatch e f = inj (X.dispatch (prj e) (fun e -> prj (f e)))
  end

  module Array (X : Array with type 'a m = 'a Domain.m) :
    Array
      with type 'a m = 'a T(X).m
       and type t = X.t
       and type elt = X.elt
       and type index = X.index = struct
    include T (X)

    type t = X.t

    type elt = X.elt

    type index = X.index

    let get a i = inj (X.get (prj a) (prj i))

    let unsafe_get a i = inj (X.unsafe_get (prj a) (prj i))

    let set a i v = inj (X.set (prj a) (prj i) (prj v))

    let unsafe_set a i v = inj (X.unsafe_set (prj a) (prj i) (prj v))

    let make i e = inj (X.make (prj i) (prj e))

    let length a = inj (X.length (prj a))

    let copy x = inj (X.copy (prj x))

    let blit a1 ofs1 a2 ofs2 len =
      inj (X.blit (prj a1) (prj ofs1) (prj a2) (prj ofs2) (prj len))

    let sub a o l = inj (X.sub (prj a) (prj o) (prj l))
  end

  module Ring (X : Ring with type 'a m = 'a Domain.m) :
    Ring with type 'a m = 'a T(X).m and type t = X.t = struct
    include T (X)

    type t = X.t

    let zero = inj X.zero

    let one = inj X.one

    let add x y = inj (X.add (prj x) (prj y))

    let sub x y = inj (X.sub (prj x) (prj y))

    let mul x y = inj (X.mul (prj x) (prj y))

    let neg x = inj (X.neg (prj x))

    let of_int i = inj (X.of_int i)
  end

  module Field (X : Field with type 'a m = 'a Domain.m) :
    Field with type 'a m = 'a T(X).m and type t = X.t = struct
    include T (X)

    type t = X.t

    let zero = inj X.zero

    let one = inj X.one

    let add x y = inj (X.add (prj x) (prj y))

    let sub x y = inj (X.sub (prj x) (prj y))

    let mul x y = inj (X.mul (prj x) (prj y))

    let neg x = inj (X.neg (prj x))

    let div x y = inj (X.div (prj x) (prj y))

    let of_int i = inj (X.of_int i)
  end

  module Exn (X : Exn with type 'a m = 'a Domain.m) :
    Exn with type 'a m = 'a T(X).m = struct
    include T (X)

    let raise_ e = inj (X.raise_ e)
  end

  module Infix_order (X : Infix_order with type 'a m = 'a Domain.m) :
    Infix_order with type 'a m = 'a T(X).m and type t = X.t = struct
    include T (X)

    type t = X.t

    let ( < ) x y = inj X.(prj x < prj y)

    let ( > ) x y = inj X.(prj x > prj y)

    let ( <= ) x y = inj X.(prj x <= prj y)

    let ( >= ) x y = inj X.(prj x >= prj y)

    let ( = ) x y = inj X.(prj x = prj y)

    let ( <> ) x y = inj X.(prj x <> prj y)
  end
end
