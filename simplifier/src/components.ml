open Basic_intf.Lang

module type S = sig
  type 'a base

  type 'a m

  val prj : 'a m -> 'a base

  val inj : 'a base -> 'a m

  type r

  type e

  module Repr : Empty with type 'a m = 'a m

  module Bool : Bool with type 'a m = 'a Repr.m

  module Int : Ring with type 'a m = 'a Repr.m and type t = r

  module Int_ord : Infix_order with type 'a m = 'a Repr.m with type t = r

  module Int_cst : Const with type 'a m = 'a Repr.m with type t = r

  module Field : Field with type 'a m = 'a Repr.m and type t = e

  module Field_ord : Infix_order with type 'a m = 'a Repr.m and type t = e

  module Field_cst : Const with type 'a m = 'a Repr.m and type t = e

  module Loop : Loop with type 'a m = 'a Repr.m with type index = r

  module Array :
    Array with type 'a m = 'a Repr.m with type index = r and type elt = e

  module Sequencing : Sequencing with type 'a m = 'a Repr.m

  module Product : Product with type 'a m = 'a Repr.m

  module Lambda : Lambda with type 'a m = 'a Repr.m

  module Exn : Exn with type 'a m = 'a Repr.m
end

module type Static_order = Infix_order with type 'a m = 'a

module type Static_ring = Ring with type 'a m = 'a

module type Type = sig
  type t
end

type static_order = (module Static_order)

type static_ring = (module Static_ring)

module type Pass = sig
  type r

  type e

  module F : functor (C : S with type r = r and type e = e) ->
    S with type 'a base = 'a C.base and type r = r and type e = e
end

type ('r, 'e) pass = (module Pass with type r = 'r and type e = 'e)

(* ------------------------------------------------------------------------- *)

module Identity (R : Type) (E : Type) :
  Pass with type r = R.t and type e = E.t = struct
  type r = R.t

  type e = E.t

  module F (C : S with type r = r and type e = e) = C
end

(* ------------------------------------------------------------------------- *)

let sequential (type r e) ((module P1) : (r, e) pass)
    ((module P2) : (r, e) pass) =
  let module Result = struct
    type nonrec r = r

    type nonrec e = e

    module F (X : S with type r = r and type e = e) = P1.F (P2.F (X))
  end in
  ((module Result) : (r, e) pass)

let rec compose : type r e. (r, e) pass list -> (r, e) pass =
  fun (type r e) passes ->
   match passes with
   | [] ->
       let module Id =
         Identity
           (struct
             type t = r
           end)
           (struct
             type t = e
           end)
       in
       ((module Id) : (r, e) pass)
   | [last] -> last
   | hd :: tl -> sequential hd (compose tl)

(* ------------------------------------------------------------------------- *)

module Constant_folding_bool (Static_order : Static_order) (E : Type) :
  Pass with type r = Static_order.t and type e = E.t = struct
  type r = Static_order.t

  type e = E.t

  module F (C : S with type r = r and type e = e) = struct
    module Simpl =
      Passes.Bool_constant_folding (C.Repr) (C.Bool) (C.Int_ord) (C.Int_cst)
        (Static_order)

    type 'a m = 'a Simpl.Empty.m

    type 'a base = 'a C.base

    type nonrec r = r

    type nonrec e = e

    let prj x = C.prj (Simpl.prj x)

    let inj x = Simpl.inj (C.inj x)

    module Map = Simpl.Mapper
    module Repr = Simpl.Empty
    module Bool = Simpl.Bool
    module Int = Map.Ring (C.Int)
    module Int_ord = Simpl.Infix_order
    module Int_cst = Simpl.Const
    module Field = Map.Field (C.Field)
    module Field_ord = Map.Infix_order (C.Field_ord)
    module Field_cst = Map.Const (C.Field_cst)
    module Loop = Map.Loop (C.Loop)
    module Array = Map.Array (C.Array)
    module Sequencing = Map.Sequencing (C.Sequencing)
    module Product = Map.Product (C.Product)
    module Lambda = Map.Lambda (C.Lambda)
    module Exn = Map.Exn (C.Exn)
  end
end

let constant_folding_bool (type r e) (module O : Static_order with type t = r)
    (module E : Type with type t = e) : (r, e) pass =
  (module Constant_folding_bool (O) (E))

(* ------------------------------------------------------------------------- *)

module Constant_folding_ring (Static_ring : Static_ring) (E : Type) :
  Pass with type r = Static_ring.t and type e = E.t = struct
  type r = Static_ring.t

  type e = E.t

  module F (C : S with type r = r and type e = e) = struct
    module Simpl =
      Passes.Ring_constant_folding (C.Repr) (C.Int) (C.Int_cst) (C.Sequencing)
        (Static_ring)

    type 'a m = 'a Simpl.Empty.m

    type 'a base = 'a C.base

    type nonrec r = r

    type nonrec e = e

    let prj x = C.prj (Simpl.prj x)

    let inj x = Simpl.inj (C.inj x)

    module Map = Simpl.Mapper
    module Repr = Simpl.Empty
    module Bool = Map.Bool (C.Bool)
    module Int = Simpl.Ring
    module Int_ord = Map.Infix_order (C.Int_ord)
    module Int_cst = Simpl.Const
    module Field = Map.Field (C.Field)
    module Field_ord = Map.Infix_order (C.Field_ord)
    module Field_cst = Map.Const (C.Field_cst)
    module Loop = Map.Loop (C.Loop)
    module Array = Map.Array (C.Array)
    module Sequencing = Simpl.Sequencing
    module Product = Map.Product (C.Product)
    module Lambda = Map.Lambda (C.Lambda)
    module Exn = Map.Exn (C.Exn)
  end
end

let constant_folding_ring (type r e) (module O : Static_ring with type t = r)
    (module E : Type with type t = e) : (r, e) pass =
  (module Constant_folding_ring (O) (E))

(* ------------------------------------------------------------------------- *)

module Array_simplification (R : Type) (E : Type) :
  Pass with type r = R.t and type e = E.t = struct
  type r = R.t

  type e = E.t

  module F (C : S with type r = r and type e = e) = struct
    module Simpl = Passes.Simplify_array (C.Repr) (C.Sequencing) (C.Array)

    type 'a m = 'a Simpl.Empty.m

    type 'a base = 'a C.base

    type nonrec r = r

    type nonrec e = e

    let prj x = C.prj (Simpl.prj x)

    let inj x = Simpl.inj (C.inj x)

    module Map = Simpl.Mapper
    module Repr = Simpl.Empty
    module Bool = Map.Bool (C.Bool)
    module Int = Map.Ring (C.Int)
    module Int_ord = Map.Infix_order (C.Int_ord)
    module Int_cst = Map.Const (C.Int_cst)
    module Field = Map.Field (C.Field)
    module Field_ord = Map.Infix_order (C.Field_ord)
    module Field_cst = Map.Const (C.Field_cst)
    module Loop = Map.Loop (C.Loop)
    module Array = Simpl.Array
    module Sequencing = Simpl.Sequencing
    module Product = Map.Product (C.Product)
    module Lambda = Map.Lambda (C.Lambda)
    module Exn = Map.Exn (C.Exn)
  end
end

let array_simplification (type r e) (module R : Type with type t = r)
    (module E : Type with type t = e) : (r, e) pass =
  (module Array_simplification (R) (E))

(* ------------------------------------------------------------------------- *)

module Pair_simplification (R : Type) (E : Type) :
  Pass with type r = R.t and type e = E.t = struct
  type r = R.t

  type e = E.t

  module F (C : S with type r = r and type e = e) = struct
    module Simpl = Passes.Simplify_pair (C.Repr) (C.Product) (C.Sequencing)

    type 'a m = 'a Simpl.Empty.m

    type 'a base = 'a C.base

    type nonrec r = r

    type nonrec e = e

    let prj x = C.prj (Simpl.prj x)

    let inj x = Simpl.inj (C.inj x)

    module Map = Simpl.Mapper
    module Repr = Simpl.Empty
    module Bool = Map.Bool (C.Bool)
    module Int = Map.Ring (C.Int)
    module Int_ord = Map.Infix_order (C.Int_ord)
    module Int_cst = Map.Const (C.Int_cst)
    module Field = Map.Field (C.Field)
    module Field_ord = Map.Infix_order (C.Field_ord)
    module Field_cst = Map.Const (C.Field_cst)
    module Loop = Map.Loop (C.Loop)
    module Array = Map.Array (C.Array)
    module Sequencing = Simpl.Sequencing
    module Product = Simpl.Product
    module Lambda = Map.Lambda (C.Lambda)
    module Exn = Map.Exn (C.Exn)
  end
end

let pair_simplification (type r e) (module R : Type with type t = r)
    (module E : Type with type t = e) : (r, e) pass =
  (module Pair_simplification (R) (E))

(* ------------------------------------------------------------------------- *)

module Loop_unrolling (P : sig
  val max_unroll : int
end)
(E : Type) : Pass with type r = int and type e = E.t = struct
  type r = int

  type e = E.t

  module F (C : S with type r = r and type e = e) = struct
    module Simpl =
      Passes.Loop_unrolling (P) (C.Repr) (C.Int_cst) (C.Loop) (C.Sequencing)

    type 'a m = 'a Simpl.Empty.m

    type 'a base = 'a C.base

    type nonrec r = r

    type nonrec e = e

    let prj x = C.prj (Simpl.prj x)

    let inj x = Simpl.inj (C.inj x)

    module Map = Simpl.Mapper
    module Repr = Simpl.Empty
    module Bool = Map.Bool (C.Bool)
    module Int = Map.Ring (C.Int)
    module Int_ord = Map.Infix_order (C.Int_ord)
    module Int_cst = Simpl.Const
    module Field = Map.Field (C.Field)
    module Field_ord = Map.Infix_order (C.Field_ord)
    module Field_cst = Map.Const (C.Field_cst)
    module Loop = Simpl.Loop
    module Array = Map.Array (C.Array)
    module Sequencing = Map.Sequencing (C.Sequencing)
    module Product = Map.Product (C.Product)
    module Lambda = Map.Lambda (C.Lambda)
    module Exn = Map.Exn (C.Exn)
  end
end

let loop_unrolling (type e) (max_unroll : int) (module E : Type with type t = e)
    : (int, e) pass =
  (module Loop_unrolling
            (struct
              let max_unroll = max_unroll
            end)
            (E))

(* ------------------------------------------------------------------------- *)

module Dead_code (R : Type) (E : Type) :
  Pass with type r = R.t and type e = E.t = struct
  type r = R.t

  type e = E.t

  module F (C : S with type r = r and type e = e) = struct
    module Simpl =
      Passes.Dead_code_elimination (C.Repr) (C.Bool) (C.Int) (C.Int_ord)
        (C.Int_cst)
        (C.Field)
        (C.Field_ord)
        (C.Field_cst)
        (C.Product)
        (C.Sequencing)

    type 'a m = 'a Simpl.Empty.m

    type 'a base = 'a C.base

    type nonrec r = r

    type nonrec e = e

    let prj x = C.prj (Simpl.prj x)

    let inj x = Simpl.inj (C.inj x)

    module Map = Simpl.Mapper
    module Repr = Simpl.Empty
    module Bool = Simpl.Bool
    module Int = Simpl.Int
    module Int_ord = Simpl.Int_ord
    module Int_cst = Simpl.Int_cst
    module Field = Simpl.Field
    module Field_ord = Simpl.Field_ord
    module Field_cst = Map.Const (C.Field_cst)
    module Loop = Map.Loop (C.Loop)
    module Array = Map.Array (C.Array)
    module Sequencing = Simpl.Sequencing
    module Product = Simpl.Product
    module Lambda = Map.Lambda (C.Lambda)
    module Exn = Map.Exn (C.Exn)
  end
end

let dead_code (type r e) (module R : Type with type t = r)
    (module E : Type with type t = e) : (r, e) pass =
  (module Dead_code (R) (E))
