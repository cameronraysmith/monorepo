open Basic_intf.Lang

(* ------------------------------------------------------------------------- *)
(* Static array length propagation *)

module Simplify_array
    (Repr : Empty)
    (M : Sequencing with type 'a m = 'a Repr.m)
    (A : Array with type 'a m = 'a Repr.m) =
struct
  module Domain = Repr

  module Range = struct
    type _ m =
      | U : 'a Domain.m -> 'a m
      | Array : { length : A.index Domain.m; array : A.t Domain.m } -> A.t m
  end

  let prj (type a) (x : a Range.m) : a Domain.m =
    match x with U e -> (e : a Domain.m) | Array { array; _ } -> array

  let inj (type a) (x : a Domain.m) : a Range.m = U x

  module Id =
  functor
    (Domain : sig
       type 'a m = 'a Domain.m
     end)
    ->
    struct
      type 'a m = 'a Range.m

      let prj : 'a Range.m -> 'a Domain.m = prj

      let inj : 'a Domain.m -> 'a Range.m = inj
    end

  module Empty = Range
  module Mapper = Transform.Map (Domain) (Range) (Id)

  module Array :
    Array
      with type 'a m = 'a Range.m
       and type t = A.t
       and type elt = A.elt
       and type index = A.index = struct
    include Mapper.Array (A)

    let copy a =
      match a with
      | Range.Array { length; array } ->
          Range.Array { length; array = A.copy array }
      | Range.U array -> inj (A.copy array)

    let make i e =
      let length = prj i in
      let init = prj e in
      let array = A.make length init in
      Range.Array { length; array }

    let length a =
      match a with
      | Range.Array { length; _ } -> inj length
      | Range.U array -> inj (A.length array)
  end

  module Sequencing : Sequencing with type 'a m = 'a Range.m = struct
    include Mapper.Sequencing (M)

    let seq : type a. unit Range.m -> (unit -> a Range.m) -> a Range.m =
     fun m f ->
      match f () with
      | Range.U x -> inj (M.seq (prj m) (fun () -> x))
      | Range.Array { length; array } ->
          Array { length; array = M.seq (prj m) (fun () -> array) }

    let ( let* ) : type a b. a Range.m -> (a Range.m -> b Range.m) -> b Range.m
        =
     fun m f ->
      match m with
      | Range.U x ->
          inj
          @@ M.(
               let* a = x in
               prj (f (inj a)))
      | Range.Array { length; array } ->
          inj
          @@ M.(
               let* a = array in
               prj (f (Array { length; array = a })))

    let unit = inj M.unit
  end
end

(* ------------------------------------------------------------------------- *)
(* Pair folding *)

module Simplify_pair
    (Repr : Empty)
    (P : Product with type 'a m = 'a Repr.m)
    (M : Sequencing with type 'a m = 'a Repr.m) =
struct
  module Domain = Repr

  module Range = struct
    type _ m =
      | U : 'a Domain.m -> 'a m
      | Pair :
          { l : 'a m; r : 'b m; name_opt : ('a * 'b) Repr.m option }
          -> ('a * 'b) m
  end

  let rec prj : type a. a Range.m -> a Domain.m =
    fun (type a) (x : a Range.m) ->
     match x with
     | U e -> (e : a Domain.m)
     | Pair { l; r; name_opt } -> (
         match name_opt with
         | None ->
             let l = prj l in
             let r = prj r in
             P.prod l r
         | Some name -> name)

  let inj (type a) (x : a Domain.m) : a Range.m = U x

  module Id =
  functor
    (Domain : sig
       type 'a m = 'a Domain.m
     end)
    ->
    struct
      type 'a m = 'a Range.m

      let prj : 'a Range.m -> 'a Domain.m = prj

      let inj : 'a Domain.m -> 'a Range.m = inj
    end

  module Empty = Range
  module Mapper = Transform.Map (Domain) (Range) (Id)

  module Product : Product with type 'a m = 'a Range.m = struct
    include Mapper.Product (P)

    let prod l r = Range.Pair { l; r; name_opt = None }

    let fst x =
      match x with Range.U x -> inj (P.fst x) | Range.Pair { l; _ } -> l

    let snd x =
      match x with Range.U x -> inj (P.snd x) | Range.Pair { r; _ } -> r
  end

  module Sequencing : Sequencing with type 'a m = 'a Range.m = struct
    include Mapper.Sequencing (M)

    let seq : type a. unit Range.m -> (unit -> a Range.m) -> a Range.m =
     fun m f -> inj (M.seq (prj m) (fun () -> prj (f ())))

    let ( let* ) : type a b. a Range.m -> (a Range.m -> b Range.m) -> b Range.m
        =
     fun m f ->
      match m with
      | Range.U x ->
          inj
          @@ M.(
               let* a = x in
               prj (f (inj a)))
      | Range.Pair { l; r; name_opt = _ } ->
          inj
          @@ M.(
               let* a = prj m in
               prj (f (Pair { l; r; name_opt = Some a })))

    let unit = inj M.unit
  end
end

(* ------------------------------------------------------------------------- *)
(* Ring constant folding *)

module Ring_constant_folding
    (Repr : Empty)
    (R : Ring with type 'a m = 'a Repr.m)
    (C : Const with type 'a m = 'a Repr.m and type t = R.t)
    (M : Sequencing with type 'a m = 'a Repr.m)
    (Static_ring : Ring with type 'a m = 'a and type t = R.t) =
struct
  module Domain = Repr

  module Range = struct
    type _ m = U : 'a Domain.m -> 'a m | Const : R.t -> R.t m
  end

  let prj : type a. a Range.m -> a Domain.m =
    fun (type a) (x : a Range.m) ->
     match x with U e -> (e : a Domain.m) | Const i -> C.const i

  let inj (type a) (x : a Domain.m) : a Range.m = U x

  module Id =
  functor
    (Domain : sig
       type 'a m = 'a Domain.m
     end)
    ->
    struct
      type 'a m = 'a Range.m

      let prj : 'a Range.m -> 'a Domain.m = prj

      let inj : 'a Domain.m -> 'a Range.m = inj
    end

  module Empty = Range
  module Mapper = Transform.Map (Domain) (Range) (Id)

  let static_index_binop :
      stc:(R.t -> R.t -> R.t) ->
      dyn:(R.t Domain.m -> R.t Domain.m -> R.t Domain.m) ->
      R.t Range.m ->
      R.t Range.m ->
      R.t Range.m =
   fun ~stc ~dyn x y ->
    let open Range in
    match (x, y) with
    | (Const x, Const y) -> Const (stc x y)
    | (Const x, U y) -> inj @@ dyn (C.const x) y
    | (U x, Const y) -> inj @@ dyn x (C.const y)
    | (U x, U y) -> inj @@ dyn x y

  let static_index_unop :
      stc:(R.t -> R.t) ->
      dyn:(R.t Domain.m -> R.t Domain.m) ->
      R.t Range.m ->
      R.t Range.m =
   fun ~stc ~dyn x ->
    let open Range in
    match x with Const x -> Const (stc x) | U x -> inj @@ dyn x

  module Const : Const with type 'a m = 'a Range.m and type t = R.t = struct
    include Mapper.Const (C)

    let const x = Range.Const x
  end

  module Ring : Ring with type 'a m = 'a Range.m and type t = R.t = struct
    include Mapper.Ring (R)

    let zero = Range.Const Static_ring.zero

    let one = Range.Const Static_ring.one

    let add = static_index_binop ~stc:Static_ring.add ~dyn:R.add

    let mul = static_index_binop ~stc:Static_ring.mul ~dyn:R.mul

    let neg = static_index_unop ~stc:Static_ring.neg ~dyn:R.neg
  end

  module Sequencing : Sequencing with type 'a m = 'a Range.m = struct
    include Mapper.Sequencing (M)

    let seq : type a. unit Range.m -> (unit -> a Range.m) -> a Range.m =
     fun m f -> inj (M.seq (prj m) (fun () -> prj (f ())))

    let ( let* ) : type a b. a Range.m -> (a Range.m -> b Range.m) -> b Range.m
        =
     fun m f ->
      match m with
      | Range.U x ->
          inj
          @@ M.(
               let* a = x in
               prj (f (inj a)))
      | Range.Const _c -> f m

    let unit = inj M.unit
  end
end

(* ------------------------------------------------------------------------- *)
(* Static branch evaluation *)

module Bool_constant_folding
    (Repr : Empty)
    (B : Bool with type 'a m = 'a Repr.m)
    (O : Infix_order with type 'a m = 'a Repr.m)
    (C : Const with type 'a m = 'a Repr.m and type t = O.t)
    (Static_order : Infix_order with type 'a m = 'a and type t = O.t) =
struct
  module Domain = Repr

  module Range = struct
    type _ m =
      | U : 'a Domain.m -> 'a m
      | Bool : bool -> bool m
      | Elt : O.t -> O.t m
  end

  let prj : type a. a Range.m -> a Domain.m =
    fun (type a) (x : a Range.m) ->
     match x with
     | U e -> (e : a Domain.m)
     | Bool b -> if b then B.true_ else B.false_
     | Elt i -> C.const i

  let inj (type a) (x : a Domain.m) : a Range.m = U x

  module Id =
  functor
    (Domain : sig
       type 'a m = 'a Domain.m
     end)
    ->
    struct
      type 'a m = 'a Range.m

      let prj : 'a Range.m -> 'a Domain.m = prj

      let inj : 'a Domain.m -> 'a Range.m = inj
    end

  module Empty = Range
  module Mapper = Transform.Map (Domain) (Range) (Id)

  let static_index_binop :
      stc:(O.t -> O.t -> bool) ->
      dyn:(O.t Domain.m -> O.t Domain.m -> bool Domain.m) ->
      O.t Range.m ->
      O.t Range.m ->
      bool Range.m =
   fun ~stc ~dyn x y ->
    let open Range in
    match (x, y) with
    | (Elt x, Elt y) -> Bool (stc x y)
    | _ -> inj (dyn (prj x) (prj y))

  module Bool : Bool with type 'a m = 'a Range.m = struct
    include Mapper.Bool (B)

    let true_ = Range.Bool true

    let false_ = Range.Bool false

    let ( || ) x y =
      let open Range in
      match (x, y) with
      | (Bool true, _) | (_, Bool true) -> Bool true
      | (Bool x, Bool y) -> Bool Stdlib.(x || y)
      | _ -> inj B.(prj x || prj y)

    let ( && ) x y =
      let open Range in
      match (x, y) with
      | (Bool false, _) | (_, Bool false) -> Bool false
      | (Bool x, Bool y) -> Bool Stdlib.(x && y)
      | _ -> inj B.(prj x && prj y)

    let dispatch : type a. bool m -> (bool -> a m) -> a m =
     fun x f ->
      let open Range in
      match x with
      | Bool true -> f true
      | Bool false -> f false
      | x -> inj (B.dispatch (prj x) (fun b -> prj (f b)))
  end

  module Const : Const with type 'a m = 'a Range.m and type t = O.t = struct
    include Mapper.Const (C)

    let const x = Range.Elt x
  end

  module Infix_order :
    Infix_order with type 'a m = 'a Range.m and type t = O.t = struct
    include Mapper.Infix_order (O)

    let ( < ) = static_index_binop ~stc:Stdlib.( < ) ~dyn:O.( < )

    let ( > ) = static_index_binop ~stc:Stdlib.( > ) ~dyn:O.( > )

    let ( <= ) = static_index_binop ~stc:Stdlib.( <= ) ~dyn:O.( <= )

    let ( >= ) = static_index_binop ~stc:Stdlib.( >= ) ~dyn:O.( >= )

    let ( = ) = static_index_binop ~stc:Stdlib.( = ) ~dyn:O.( = )

    let ( <> ) = static_index_binop ~stc:Stdlib.( <> ) ~dyn:O.( <> )
  end
end

(* ------------------------------------------------------------------------- *)
(* Loop unrolling *)

module Loop_unrolling (Param : sig
  val max_unroll : int
end)
(Repr : Empty)
(C : Const with type 'a m = 'a Repr.m and type t = int)
(F : Loop with type 'a m = 'a Repr.m and type index = C.t)
(M : Sequencing with type 'a m = 'a Repr.m) =
struct
  module Domain = Repr

  module Range = struct
    type _ m = U : 'a Domain.m -> 'a m | Const : C.t -> C.t m
  end

  let prj : type a. a Range.m -> a Domain.m =
    fun (type a) (x : a Range.m) ->
     match x with U e -> (e : a Domain.m) | Const i -> C.const i

  let inj (type a) (x : a Domain.m) : a Range.m = U x

  module Id =
  functor
    (Domain : sig
       type 'a m = 'a Domain.m
     end)
    ->
    struct
      type 'a m = 'a Range.m

      let prj : 'a Range.m -> 'a Domain.m = prj

      let inj : 'a Domain.m -> 'a Range.m = inj
    end

  module Empty = Range
  module Mapper = Transform.Map (Domain) (Range) (Id)

  module Const : Const with type 'a m = 'a Range.m and type t = C.t = struct
    include Mapper.Const (C)

    let const x = Range.Const x
  end

  module Loop : Loop with type 'a m = 'a Range.m and type index = C.t = struct
    include Mapper.Loop (F)

    let rec unroll start stop body =
      if start = stop then prj (body (Range.Const start))
      else
        M.seq
          (prj (body (Range.Const start)))
          (fun () -> unroll (start + 1) stop body)

    let for_ ~start ~stop body =
      let default () =
        F.for_ ~start:(prj start) ~stop:(prj stop) (fun i -> prj (body (inj i)))
      in
      match (start, stop) with
      | (Range.Const start, Range.Const stop) ->
          if stop < start then inj M.unit
          else if stop = start then body (Range.Const start)
          else if stop - start <= Param.max_unroll then
            inj @@ unroll start stop body
          else inj @@ default ()
      | _ -> inj @@ default ()
  end
end

(* ------------------------------------------------------------------------- *)
(* Purity analysis / Dead code elimination *)

module Dead_code_elimination
    (Repr : Empty)
    (B : Bool with type 'a m = 'a Repr.m)
    (I : Ring with type 'a m = 'a Repr.m)
    (I_ord : Infix_order with type 'a m = 'a Repr.m and type t = I.t)
    (I_cst : Const with type 'a m = 'a Repr.m and type t = I.t)
    (F : Field with type 'a m = 'a Repr.m)
    (F_ord : Infix_order with type 'a m = 'a Repr.m and type t = F.t)
    (F_cst : Const with type 'a m = 'a Repr.m and type t = F.t)
    (P : Product with type 'a m = 'a Repr.m)
    (M : Sequencing with type 'a m = 'a Repr.m) =
struct
  module Domain = Repr

  let gen =
    let x = ref 0 in
    fun () ->
      let v = !x in
      incr x ;
      v

  type purity = Pure | Impure

  (* Dev notes:

     When encountering \gamma |- [| let x = m in f |], we need to know whether or not
     we can safely remove the binding of [|m|] to [|x|].

     This is possible when the two following conditions are met:
     - m is side-effect free
     - x is not used in \gamma, x |- [|f|]

     How do we know that x is not used in [|f|]?
     - we can have each expression [|e|] contain an over-approximation of the variables it contains
       - it's hard to stay precise though: when injecting values into constructs not handled explicitly
         by the analysis, we lose all information contained in the abstract domain; so this
         forces us to pretty much handle all the language. This goes against the philosophy of
         having easily extensible analysis that only work on the relevant sub-part of the
         language.
     - we can hack a mutable flag in [|x|] that is set to true whenever [|x|] is used.
       - let's assume we take this route and we discover that [|x|] is not used in [|f|].
         Then we can remove the let-binding from [m] to [x].
         But since we have this term, maybe some variables that we tagged as 'used' because
         of their presence in [m] are not used anymore!
         And we can't simply unset them as used:
         - they might be used elsewhere (in [f], or in the surrounding context)
         - we don't have access to them directly in the first place

     => I don't see any way to keep both composionality and precision ATM. The following
     implements a sound but less powerful rule: delete a binding if it is not used _at all_
     (so we drop the inductive part). One can iterate the pass until reaching a fixpoint...
  *)

  type 'a record =
    { value : 'a Repr.m; purity : purity; id : int; mutable used : bool }

  module Range = struct
    type 'a m = 'a record
  end

  let prj : type a. a Range.m -> a Domain.m =
    fun (type a) (x : a Range.m) ->
     x.used <- true ;
     x.value

  let create purity value = { value; purity; id = gen (); used = true }

  let inj (type a) (value : a Domain.m) : a Range.m =
    (* impure value: no need to handle dependency tracking (?) *)
    create Impure value

  let pure (type a) (value : a Domain.m) : a Range.m = create Pure value

  let impure (type a) (value : a Domain.m) : a Range.m = create Impure value

  (* constants are always pure and depend on nothing *)
  let constant (type a) (value : a Domain.m) : a Range.m = create Pure value

  let purity_join (p1 : purity) (p2 : purity) =
    match (p1, p2) with (Pure, Pure) -> Pure | _ -> Impure

  let map_pure (type a) f (v : a Range.m) =
    v.used <- true ;
    let value = f v.value in
    let purity = v.purity in
    create purity value

  let map2_pure (type a b) f (v1 : a Range.m) (v2 : b Range.m) =
    v1.used <- true ;
    v2.used <- true ;
    let value = f v1.value v2.value in
    let purity = purity_join v1.purity v2.purity in
    create purity value

  module Id =
  functor
    (Domain : sig
       type 'a m = 'a Domain.m
     end)
    ->
    struct
      type 'a m = 'a Range.m

      let prj : 'a Range.m -> 'a Domain.m = prj

      (* by default, everything is impure *)
      let inj : 'a Domain.m -> 'a Range.m = inj
    end

  module Empty = Range
  module Mapper = Transform.Map (Domain) (Range) (Id)

  module Bool : Bool with type 'a m = 'a Range.m = struct
    include Mapper.Bool (B)

    let true_ = constant B.true_

    let false_ = constant B.false_

    let ( || ) x y = map2_pure B.( || ) x y

    let ( && ) x y = map2_pure B.( && ) x y

    let dispatch : type a. bool m -> (bool -> a m) -> a m =
     fun x f ->
      let branch1 = f true in
      let branch2 = f false in
      let purity =
        List.fold_left purity_join Pure [branch1.purity; branch2.purity]
      in
      create
        purity
        (B.dispatch (prj x) (function
            | true -> prj branch1
            | false -> prj branch2))
  end

  module Int : Ring with type 'a m = 'a Range.m and type t = I.t = struct
    include Mapper.Ring (I)

    let zero = constant I.zero

    let one = constant I.one

    let add x y = map2_pure I.add x y

    let neg x = map_pure I.neg x

    let mul x y = map2_pure I.mul x y

    let sub x y = map2_pure I.sub x y
  end

  module Int_ord :
    Infix_order with type 'a m = 'a Range.m and type t = I_ord.t = struct
    include Mapper.Infix_order (I_ord)

    let ( < ) x y = map2_pure I_ord.( < ) x y

    let ( > ) x y = map2_pure I_ord.( > ) x y

    let ( <= ) x y = map2_pure I_ord.( <= ) x y

    let ( >= ) x y = map2_pure I_ord.( >= ) x y

    let ( = ) x y = map2_pure I_ord.( = ) x y

    let ( <> ) x y = map2_pure I_ord.( <> ) x y
  end

  module Int_cst : Const with type 'a m = 'a Range.m and type t = I_cst.t =
  struct
    include Mapper.Const (I_cst)

    let const x = constant (I_cst.const x)
  end

  module Field : Field with type 'a m = 'a Range.m and type t = F.t = struct
    include Mapper.Field (F)

    let zero = constant F.zero

    let one = constant F.one

    let add x y = map2_pure F.add x y

    let neg x = map_pure F.neg x

    let mul x y = map2_pure F.mul x y

    let sub x y = map2_pure F.sub x y

    let div x y = map2_pure F.div x y
  end

  module Field_ord :
    Infix_order with type 'a m = 'a Range.m and type t = F_ord.t = struct
    include Mapper.Infix_order (F_ord)

    let ( < ) x y = map2_pure F_ord.( < ) x y

    let ( > ) x y = map2_pure F_ord.( > ) x y

    let ( <= ) x y = map2_pure F_ord.( <= ) x y

    let ( >= ) x y = map2_pure F_ord.( >= ) x y

    let ( = ) x y = map2_pure F_ord.( = ) x y

    let ( <> ) x y = map2_pure F_ord.( <> ) x y
  end

  module Product : Product with type 'a m = 'a Range.m = struct
    include Mapper.Product (P)

    let prod l r = map2_pure P.prod l r

    let fst x = map_pure P.fst x

    let snd x = map_pure P.snd x
  end

  module Sequencing : Sequencing with type 'a m = 'a Range.m = struct
    include Mapper.Sequencing (M)

    let seq : type a. unit Range.m -> (unit -> a Range.m) -> a Range.m =
     fun m f -> inj @@ M.seq (prj m) (fun () -> prj (f ()))

    (** We start building a let-binding as usual: in particular, we
       work in the scope of the binding variable [x]. We then
       instantiate the body of [f] with a box containing [x]
       as a value.
       - If it turns out [x] was not used when constructing
         the body after all, we escape the body with a local exception.
       - Otherwise, we return the term as usual.

       The working assumption is that [x] is either used:
       - as the return value of [f] (which technically counts as a "use")
       - or as an argument to [prj] somewhere in the body of [f]
       - or through one of the submodule defined in this optimization pass, which
         must take care of properly dealing with dependencies

       The rationale for this assumption is that if neither point above
       is the case, it means that the value carried in the box is never used to
       construct a term in the Domain language. *)

    let ( let* ) : type a b. a Range.m -> (a Range.m -> b Range.m) -> b Range.m
        =
     fun m_rec f ->
      match m_rec.purity with
      | Impure ->
          let result =
            M.(
              let* x = prj m_rec in
              let record = { m_rec with value = x } in
              let body = f record in
              prj body)
          in
          impure result
      | Pure -> (
          let exception Escape of b Range.m in
          try
            let purity = ref [] in
            let result =
              M.(
                let* x = m_rec.value in
                let record = create Pure x in
                record.used <- false ;
                let body = f record in
                purity := body.purity :: !purity ;
                let returned = body.id = record.id in
                let is_used = record.used || returned in
                if is_used then prj body else raise (Escape body))
            in
            let purity = List.fold_left purity_join Pure !purity in
            create purity result
          with Escape res -> res)

    let unit = constant M.unit
  end
end
