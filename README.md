# Monorepo

This monorepo gathers several OCaml packages.

- `basic-structures`: standard interfaces used throughout all packages below
- `clustering`: a clustering library implementing k-means, k-medoids and agglomerative clustering
- `dense`: some helpers to construct dense arrays of various types
- `gnuplot`: a thin overlay over gnuplot
- `linalg`: backend-independent linear algebra library
- `mcts`: Monte-Carlo tree search with UCB1 bandits
- `numerics`: gradient descent algorithms, including a proximal gradient descent implementation
- `ocaml-repr`: code generation helpers (OCaml target)
- `ppl`: experimental probabilistic programming DSL, in the style of Adam Scibior's `monad-bayes`
- `simplifier`: a library containing some composable optimization passes
- `statz`: statistics library
- `tangent`: generic algorithmic differentiation, with capabilities for code generation
- `ucb1`: UCB1 bandits
