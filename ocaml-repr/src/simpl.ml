open Simplifier
open Basic_intf.Lang
module Ring = Basic_impl.Lang.Int

module Make_float
    (Ring : Ring with type 'a m = 'a Ocaml_lang.Repr.m and type t = int) =
struct
  module Base :
    Components.S
      with type r = Ring.t
       and type e = float
       and type 'a base = 'a Ocaml_lang.Repr.m
       and type 'a m = 'a Ocaml_lang.Repr.m = struct
    type 'a m = 'a Ocaml_lang.Repr.m

    type 'a base = 'a m

    type r = Ring.t

    type e = float

    let prj x = x

    let inj x = x

    open Ocaml_lang
    module Repr = Repr
    module Bool = Bool
    module Int = Int
    module Int_ord = Int_order
    module Int_cst = Int_const
    module Field = Float
    module Field_ord = Float_order
    module Field_cst = Float_const
    module Loop = Loop

    module Array = Make_array (struct
      type t = float
    end)

    module Sequencing = Sequencing
    module Product = Product
    module Lambda = Lambda
    module Exn = Exn
  end

  module Apply
      (Pipeline : Components.Pass with type r = Ring.t and type e = float) :
    Components.S
      with type 'a base = 'a Base.base
       and type r = Pipeline.r
       and type e = Pipeline.e =
    Pipeline.F (Base)
end
