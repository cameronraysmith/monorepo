(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2021 Ilias Garnier                                          *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module type Build_parameters = sig
  val plugin_module_name : string
end

module OCamlopt (P : Build_parameters) = struct
  module Backend = struct
    let symbol_for_global' = Compilenv.symbol_for_global'

    let closure_symbol = Compilenv.closure_symbol

    let really_import_approx = Import_approx.really_import_approx

    let import_symbol = Import_approx.import_symbol

    let size_int = Arch.size_int

    let big_endian = Arch.big_endian

    let max_sensible_number_of_arguments =
      (* The "-1" is to allow for a potential closure environment parameter. *)
      Proc.max_arguments_for_tailcalls - 1
  end

  let backend = (module Backend : Backend_intf.S)

  module Plugin_cmi : sig
    val load_embedded_cmi : string -> unit

    val create_plugin_cmi : dir:string -> unit

    val module_name : string
  end = struct
    type t = { filename : string; cmi : Cmi_format.cmi_infos }

    let module_name = P.plugin_module_name

    let preloaded_cmis = Hashtbl.create 11

    let load_embedded_cmi content =
      let content = Bytes.of_string content in
      (* Read cmi magic *)
      let magic_len = String.length Config.cmi_magic_number in
      let magic = Bytes.sub content 0 magic_len in
      assert (Bytes.equal magic (Bytes.of_string Config.cmi_magic_number)) ;
      (* Read cmi_name and cmi_sign *)
      let pos = magic_len in
      let (cmi_name, cmi_sign) = Marshal.from_bytes content pos in
      let pos = pos + Marshal.total_size content pos in
      (* Read cmi_crcs *)
      let cmi_crcs = Marshal.from_bytes content pos in
      let pos = pos + Marshal.total_size content pos in
      (* Read cmi_flags *)
      let cmi_flags = Marshal.from_bytes content pos in
      (* TODO check crcs... *)
      Hashtbl.add
        preloaded_cmis
        (String.capitalize_ascii P.plugin_module_name)
        { filename = P.plugin_module_name ^ ".cmi";
          cmi = { cmi_name; cmi_sign; cmi_crcs; cmi_flags }
        }

    let create_plugin_cmi ~dir =
      let plugin_cmi =
        Hashtbl.find
          preloaded_cmis
          (String.capitalize_ascii P.plugin_module_name)
      in
      let cmi_file =
        Filename.concat dir (Format.asprintf "%s.cmi" P.plugin_module_name)
      in
      let _crc =
        Misc.output_to_file_via_temporary
          ~mode:[Open_binary]
          cmi_file
          (fun temp_filename oc ->
            Cmi_format.output_cmi temp_filename oc plugin_cmi.cmi)
      in
      ()
  end

  let link_shared output objects =
    Compenv.(readenv Format.err_formatter Before_link) ;
    Compmisc.init_path () ;
    Asmlink.link_shared ~ppf_dump:Format.err_formatter objects output ;
    Warnings.check_fatal ()

  let compile_ml source_file =
    let output_prefix = Filename.chop_extension source_file in

    let () =
      let open Clflags in
      native_code := true ;
      default_simplify_rounds := 3 ;
      use_inlining_arguments_set o3_arguments ;
      use_inlining_arguments_set ~round:1 o2_arguments ;
      use_inlining_arguments_set ~round:0 o1_arguments
    in

    Clflags.compile_only := true ;
    Clflags.include_dirs :=
      Filename.get_temp_dir_name () :: !Clflags.include_dirs ;
    Compenv.(readenv Format.err_formatter (Before_compile source_file)) ;
    Optcompile.implementation
      ~backend
      ~source_file
      ~output_prefix
      ~start_from:Clflags.Compiler_pass.Parsing ;
    Clflags.for_package := None ;
    output_prefix ^ ".cmx"

  let compile file = compile_ml file
end
