open Linalg
module NM = Numerics.Impl.Float
module R = NM.Regression
module BL = Basic_impl.Lang
module Vec_backend =
  Vec.Array_backed (BL.Empty) (BL.Codegen) (Tensor.Int)
    (Dense.Float_array.Native)

module Helpers_native =
  Regression_helpers.Make
    (BL.Empty)
    (BL.Make_const (struct
      type t = float
    end))
    (BL.Make_const (struct
      type t = int
    end))
    (NM)

let near_enough ?(name = "no-name") target_x target_y arr =
  let x = arr.(0) in
  let y = arr.(1) in
  Format.printf "Test \"%s\" " name ;
  if
    Basic_impl.Reals.Float.(
      abs_float (x -. target_x) < 0.1 && abs_float (y -. target_y) < 0.1)
  then Format.printf "passed@."
  else (
    Format.printf
      " failed, expected (%f, %f), got (%f, %f)@."
      target_x
      target_y
      x
      y ;
    assert false)

let distance target_x target_y arr =
  let arr_0 = Float.Array.get arr 0 in
  let arr_1 = Float.Array.get arr 1 in
  sqrt @@ (((arr_0 -. target_x) ** 2.) +. ((arr_1 -. target_y) ** 2.))

let perform_test_proximal ?(niter = 3000.0) rng_state =
  let (coeff1, coeff2, (problem, inputs, outputs)) =
    Helpers_native.prepare_problem ~dimension:10 rng_state
  in
  let inputs_mat =
    NM.Mat.make Linalg.Tensor.Int.(rank_two 2 (Array.length inputs))
    @@ fun (c, r) -> inputs.(r).(c)
  in
  let stopping =
    let open NM.Gradient_descent.Stopping in
    conj (gradient_norm ~bound:0.0001) (max_iter ~niter)
  in

  let proximal_result =
    let shape = Mat.Float.idim inputs_mat in
    let cols = Tensor.Int.(numel (fst shape)) in
    let rows = Tensor.Int.(numel (snd shape)) in
    let inputs = Float.Array.make (Tensor.Int.numel shape) 0.0 in
    let omat = NM.Mat_backend.out_of_array shape inputs in
    let outputs = Float.Array.map_from_array Fun.id outputs in
    NM.Mat.(omat := inputs_mat) ;
    let init = Float.Array.make cols 0.0 in

    R.least_squares_proximal
      ~stopping
      ~proximal:NM.Gradient_descent.Proximal.identity
      ~cols
      ~rows
      ()
      inputs
      outputs
      init
  in
  let pdist = distance coeff1 coeff2 proximal_result in
  Format.printf
    "%s: dist = %f, result = %a@."
    problem
    pdist
    Dense.Float_array.Native.pp
    proximal_result

let perform_test ?(niter = 1500.0) rng_state =
  let open Helpers_native in
  let (coeff1, coeff2, (problem, inputs, outputs)) =
    Helpers_native.prepare_problem ~dimension:10 rng_state
  in
  let inputs_mat =
    NM.Mat.make Linalg.Tensor.Int.(rank_two 2 (Array.length inputs))
    @@ fun (c, r) -> inputs.(r).(c)
  in
  let stopping =
    let open NM.Gradient_descent.Stopping in
    conj (gradient_norm ~bound:0.001) (max_iter ~niter)
  in
  let samples = ref [] in
  ArrayLabels.iter Rate.all ~f:(fun rate_spec ->
      let rate = Rate.make rate_spec in
      ArrayLabels.iter Momentum.all ~f:(fun momentum_spec ->
          let () =
            Format.printf
              "rate = %a, momentum = %a on problem %s:@."
              Rate.pp
              rate_spec
              Momentum.pp
              momentum_spec
              problem
          in
          let momentum = Momentum.make momentum_spec in
          let result_native =
            let shape = Mat.Float.idim inputs_mat in
            let cols = Tensor.Int.(numel (fst shape)) in
            let rows = Tensor.Int.(numel (snd shape)) in
            let inputs = Float.Array.make (Tensor.Int.numel shape) 0.0 in
            let omat = NM.Mat_backend.out_of_array shape inputs in
            let outputs = Float.Array.map_from_array Fun.id outputs in
            NM.Mat.(omat := inputs_mat) ;
            let init = Float.Array.make cols 0.0 in

            R.least_squares_generic
              ~stopping
              ~rate
              ~momentum
              ~cols
              ~rows
              ()
              inputs
              outputs
              init
          in

          let dist = distance coeff1 coeff2 result_native in
          let () = Format.printf "%f@." dist in
          samples :=
            (Rate.show rate_spec, Momentum.show momentum_spec, problem, dist)
            :: !samples)) ;
  !samples

let rng_state = Random.State.make [| 0x1337; 0x533D |]

let () = Format.printf "regression_benchmarks: proximal test@."

let%expect_test "regression_benchmarks: proximal" =
  for _i = 1 to 100 do
    perform_test_proximal rng_state
  done ;
  [%expect
    {|
    "171.693239 + 680.387389 * Regression_helpers.Log": dist = 0.000000, result =
    171.69 680.39
    "561.883108 + 372.223107 * Regression_helpers.Const": dist = 134.109872, result =
    467.05 467.05
    "866.876049 + 503.771818 * Regression_helpers.Log": dist = 0.000000, result =
    866.88 503.77
    "319.486272 + 173.479722 * Regression_helpers.Linear": dist = 0.000000, result =
    319.49 173.48
    "18.013197 + 675.141914 * Regression_helpers.NlogN": dist = 0.000000, result =
    18.01 675.14
    "174.296349 + 730.839520 * Regression_helpers.NlogN": dist = 0.000000, result =
    174.30 730.84
    "289.884216 + 344.817060 * Regression_helpers.NlogN": dist = 0.000000, result =
    289.88 344.82
    "861.677904 + 984.527582 * Regression_helpers.Square": dist = 5.092485, result =
    856.59 984.53
    "235.125737 + 933.771968 * Regression_helpers.Const": dist = 494.017487, result =
    584.45 584.45
    "206.516341 + 116.129986 * Regression_helpers.Const": dist = 63.912805, result =
    161.32 161.32
    "370.877934 + 584.963093 * Regression_helpers.Square": dist = 0.000153, result =
    370.88 584.96
    "777.373606 + 220.651632 * Regression_helpers.Square": dist = 0.000000, result =
    777.37 220.65
    "229.753813 + 292.459935 * Regression_helpers.Const": dist = 44.339924, result =
    261.11 261.11
    "676.372121 + 624.185793 * Regression_helpers.Linear": dist = 0.000000, result =
    676.37 624.19
    "681.214179 + 884.569632 * Regression_helpers.Const": dist = 143.794020, result =
    782.89 782.89
    "571.817132 + 306.625700 * Regression_helpers.Const": dist = 187.518660, result =
    439.22 439.22
    "618.530160 + 2.429895 * Regression_helpers.Square": dist = 0.000000, result =
    618.53 2.43
    "660.280997 + 194.903806 * Regression_helpers.NlogN": dist = 0.000000, result =
    660.28 194.90
    "301.172900 + 306.713206 * Regression_helpers.Linear": dist = 0.000000, result =
    301.17 306.71
    "481.314800 + 623.912252 * Regression_helpers.Const": dist = 100.831626, result =
    552.61 552.61
    "274.595106 + 683.318735 * Regression_helpers.Const": dist = 289.011249, result =
    478.96 478.96
    "290.991319 + 222.443009 * Regression_helpers.Log": dist = 0.000000, result =
    290.99 222.44
    "261.495355 + 855.221497 * Regression_helpers.Const": dist = 419.827781, result =
    558.36 558.36
    "848.993652 + 487.720242 * Regression_helpers.Linear": dist = 0.000000, result =
    848.99 487.72
    "801.446379 + 777.538662 * Regression_helpers.Const": dist = 16.905309, result =
    789.49 789.49
    "891.800485 + 601.451768 * Regression_helpers.Square": dist = 0.000556, result =
    891.80 601.45
    "685.884399 + 883.188649 * Regression_helpers.Linear": dist = 0.000000, result =
    685.88 883.19
    "153.489366 + 679.175193 * Regression_helpers.Log": dist = 0.000000, result =
    153.49 679.18
    "355.785911 + 371.429492 * Regression_helpers.Square": dist = 0.000380, result =
    355.79 371.43
    "757.441050 + 785.533157 * Regression_helpers.Linear": dist = 0.000000, result =
    757.44 785.53
    "593.085587 + 942.403640 * Regression_helpers.NlogN": dist = 0.000000, result =
    593.09 942.40
    "979.066279 + 559.065862 * Regression_helpers.Linear": dist = 0.000000, result =
    979.07 559.07
    "26.612147 + 971.093413 * Regression_helpers.Linear": dist = 0.000000, result =
    26.61 971.09
    "131.713318 + 279.247614 * Regression_helpers.Log": dist = 0.000013, result =
    131.71 279.25
    "232.102143 + 134.202303 * Regression_helpers.Const": dist = 69.225641, result =
    183.15 183.15
    "507.001850 + 945.099176 * Regression_helpers.Square": dist = 1.020792, result =
    505.98 945.10
    "964.562514 + 841.070843 * Regression_helpers.Linear": dist = 0.000000, result =
    964.56 841.07
    "163.505090 + 50.813034 * Regression_helpers.Square": dist = 0.000259, result =
    163.50 50.81
    "54.158733 + 595.119891 * Regression_helpers.Log": dist = 0.000026, result =
    54.16 595.12
    "968.706083 + 902.964252 * Regression_helpers.NlogN": dist = 0.000000, result =
    968.71 902.96
    "766.093623 + 504.900564 * Regression_helpers.Square": dist = 0.001843, result =
    766.09 504.90
    "636.916390 + 831.208924 * Regression_helpers.NlogN": dist = 0.000000, result =
    636.92 831.21
    "428.047851 + 677.803635 * Regression_helpers.Linear": dist = 0.000000, result =
    428.05 677.80
    "696.947967 + 988.845350 * Regression_helpers.NlogN": dist = 0.000000, result =
    696.95 988.85
    "310.684436 + 989.630716 * Regression_helpers.Const": dist = 480.087518, result =
    650.16 650.16
    "240.784896 + 12.977721 * Regression_helpers.Log": dist = 0.000000, result =
    240.78 12.98
    "847.390014 + 230.522454 * Regression_helpers.Const": dist = 436.191235, result =
    538.96 538.96
    "838.810991 + 494.660347 * Regression_helpers.Const": dist = 243.351254, result =
    666.74 666.74
    "243.603571 + 833.579586 * Regression_helpers.Log": dist = 0.000000, result =
    243.60 833.58
    "829.972980 + 885.683395 * Regression_helpers.Square": dist = 0.014603, result =
    829.96 885.68
    "209.661401 + 282.547303 * Regression_helpers.NlogN": dist = 0.000000, result =
    209.66 282.55
    "850.937408 + 413.440111 * Regression_helpers.NlogN": dist = 0.000000, result =
    850.94 413.44
    "115.411636 + 184.889813 * Regression_helpers.Linear": dist = 0.000000, result =
    115.41 184.89
    "810.544627 + 125.630093 * Regression_helpers.Const": dist = 484.307711, result =
    468.09 468.09
    "317.545080 + 385.393012 * Regression_helpers.Log": dist = 0.000004, result =
    317.55 385.39
    "706.909881 + 642.701894 * Regression_helpers.Log": dist = 0.000000, result =
    706.91 642.70
    "819.869509 + 761.832368 * Regression_helpers.Square": dist = 0.000003, result =
    819.87 761.83
    "100.892980 + 360.491401 * Regression_helpers.Linear": dist = 0.000000, result =
    100.89 360.49
    "710.111081 + 368.638890 * Regression_helpers.Const": dist = 241.457302, result =
    539.37 539.37
    "817.650046 + 551.651905 * Regression_helpers.Log": dist = 0.000000, result =
    817.65 551.65
    "498.603099 + 885.609294 * Regression_helpers.Square": dist = 0.006194, result =
    498.60 885.61
    "285.674483 + 243.803820 * Regression_helpers.NlogN": dist = 0.000000, result =
    285.67 243.80
    "931.973255 + 91.549031 * Regression_helpers.Log": dist = 0.000000, result =
    931.97 91.55
    "818.225890 + 977.390204 * Regression_helpers.NlogN": dist = 0.000000, result =
    818.23 977.39
    "171.985208 + 719.953751 * Regression_helpers.NlogN": dist = 0.000000, result =
    171.99 719.95
    "831.559774 + 672.854802 * Regression_helpers.Square": dist = 0.004117, result =
    831.56 672.85
    "120.942025 + 967.845038 * Regression_helpers.NlogN": dist = 0.000000, result =
    120.94 967.85
    "876.411810 + 182.908344 * Regression_helpers.Linear": dist = 0.000000, result =
    876.41 182.91
    "706.743310 + 272.918888 * Regression_helpers.Linear": dist = 0.000000, result =
    706.74 272.92
    "825.155761 + 53.298663 * Regression_helpers.Log": dist = 0.000479, result =
    825.16 53.30
    "658.571740 + 961.065912 * Regression_helpers.NlogN": dist = 0.000000, result =
    658.57 961.07
    "408.595224 + 951.556021 * Regression_helpers.Linear": dist = 0.000000, result =
    408.60 951.56
    "292.875478 + 136.302873 * Regression_helpers.Log": dist = 0.000125, result =
    292.88 136.30
    "16.281181 + 277.821008 * Regression_helpers.Square": dist = 16.219528, result =
    0.06 277.82
    "541.136953 + 13.649645 * Regression_helpers.Const": dist = 372.989852, result =
    277.39 277.39
    "132.934610 + 620.063140 * Regression_helpers.NlogN": dist = 0.000000, result =
    132.93 620.06
    "572.715648 + 123.464815 * Regression_helpers.Log": dist = 0.000000, result =
    572.72 123.46
    "845.821420 + 694.781971 * Regression_helpers.NlogN": dist = 0.000000, result =
    845.82 694.78
    "658.815323 + 448.513391 * Regression_helpers.Square": dist = 0.000115, result =
    658.82 448.51
    "509.749165 + 938.406151 * Regression_helpers.Const": dist = 303.106262, result =
    724.08 724.08
    "365.304746 + 900.765844 * Regression_helpers.Const": dist = 378.628174, result =
    633.04 633.04
    "729.286001 + 728.463829 * Regression_helpers.Const": dist = 0.581363, result =
    728.87 728.87
    "190.655203 + 825.257512 * Regression_helpers.NlogN": dist = 0.000000, result =
    190.66 825.26
    "35.622297 + 695.420430 * Regression_helpers.NlogN": dist = 0.000000, result =
    35.62 695.42
    "576.631241 + 765.723746 * Regression_helpers.Const": dist = 133.708592, result =
    671.18 671.18
    "68.142083 + 128.532734 * Regression_helpers.Square": dist = 0.133062, result =
    68.01 128.53
    "428.646585 + 84.102057 * Regression_helpers.Const": dist = 243.629772, result =
    256.37 256.37
    "749.611757 + 791.820669 * Regression_helpers.Linear": dist = 0.000000, result =
    749.61 791.82
    "823.803238 + 586.337533 * Regression_helpers.Const": dist = 167.913610, result =
    705.07 705.07
    "290.214170 + 841.149239 * Regression_helpers.Const": dist = 389.569923, result =
    565.68 565.68
    "109.564708 + 829.077719 * Regression_helpers.Square": dist = 0.000078, result =
    109.56 829.08
    "533.364683 + 168.094208 * Regression_helpers.Linear": dist = 0.000000, result =
    533.36 168.09
    "992.168385 + 386.601782 * Regression_helpers.Linear": dist = 0.000000, result =
    992.17 386.60
    "202.190923 + 216.401751 * Regression_helpers.Log": dist = 0.000068, result =
    202.19 216.40
    "91.522090 + 492.359700 * Regression_helpers.Const": dist = 283.434992, result =
    291.94 291.94
    "42.129489 + 636.594642 * Regression_helpers.Linear": dist = 0.000000, result =
    42.13 636.59
    "913.040254 + 548.831294 * Regression_helpers.Log": dist = 0.000165, result =
    913.04 548.83
    "959.765135 + 294.690081 * Regression_helpers.Linear": dist = 0.000000, result =
    959.77 294.69
    "946.587648 + 263.456879 * Regression_helpers.Square": dist = 0.000001, result =
    946.59 263.46
    "628.608711 + 758.616810 * Regression_helpers.Linear": dist = 0.000000, result =
    628.61 758.62 |}]

let () = Format.printf "regression_benchmarks: vanilla@."

let%expect_test "regression_benchmarks: vanilla" =
  ignore @@ perform_test ~niter:100.0 rng_state ;
  [%expect
    {|
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Rmsprop {decay = 0.2} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    462.858534
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Rmsprop {decay = 0.5} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    422.375164
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Rmsprop {decay = 0.9} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    208.078547
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.5}, momentum = Regression_helpers.Adagrad on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    467.925556
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.5}, momentum = Regression_helpers.Adam on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    369.534430
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Nesterov {momentum = 0.2} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    545.254218
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Nesterov {momentum = 0.5} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    767.651638
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Nesterov {momentum = 0.9} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    910.885903
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Rmsprop {decay = 0.2} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    440.076859
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Rmsprop {decay = 0.5} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    418.353764
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Rmsprop {decay = 0.9} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    236.475029
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.9}, momentum = Regression_helpers.Adagrad on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    467.925556
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.9}, momentum = Regression_helpers.Adam on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    361.548461
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Nesterov {momentum = 0.2} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    905.144512
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Nesterov {momentum = 0.5} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    813.941400
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Nesterov {momentum = 0.9} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    909.042744
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Rmsprop {decay = 0.2} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    462.975569
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Rmsprop {decay = 0.5} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    392.436709
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Rmsprop {decay = 0.9} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    112.481882
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.5}, momentum = Regression_helpers.Adagrad on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    467.925556
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.5}, momentum = Regression_helpers.Adam on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    370.813006
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Nesterov {momentum = 0.2} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    907.046581
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Nesterov {momentum = 0.5} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    844.498094
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Nesterov {momentum = 0.9} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    863.708769
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Rmsprop {decay = 0.2} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    460.983604
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Rmsprop {decay = 0.5} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    276.501376
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Rmsprop {decay = 0.9} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    207.378878
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.9}, momentum = Regression_helpers.Adagrad on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    467.925556
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.9}, momentum = Regression_helpers.Adam on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    361.843959
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Nesterov {momentum = 0.2} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    401.149518
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Nesterov {momentum = 0.5} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    853.457593
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Nesterov {momentum = 0.9} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    909.196427
    rate = Regression_helpers.Nonmonotone {gamma = 0.2}, momentum = Regression_helpers.Rmsprop {
                                                                      decay = 0.2} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    441.512724
    rate = Regression_helpers.Nonmonotone {gamma = 0.2}, momentum = Regression_helpers.Rmsprop {
                                                                      decay = 0.5} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    358.516279
    rate = Regression_helpers.Nonmonotone {gamma = 0.2}, momentum = Regression_helpers.Rmsprop {
                                                                      decay = 0.9} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    200.561379
    rate = Regression_helpers.Nonmonotone {gamma = 0.2}, momentum = Regression_helpers.Adagrad on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    467.925556
    rate = Regression_helpers.Nonmonotone {gamma = 0.2}, momentum = Regression_helpers.Adam on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    357.051512
    rate = Regression_helpers.Nonmonotone {gamma = 0.2}, momentum = Regression_helpers.Nesterov {
                                                                      momentum =
                                                                      0.2} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    890.208661
    rate = Regression_helpers.Nonmonotone {gamma = 0.2}, momentum = Regression_helpers.Nesterov {
                                                                      momentum =
                                                                      0.5} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    879.412848
    rate = Regression_helpers.Nonmonotone {gamma = 0.2}, momentum = Regression_helpers.Nesterov {
                                                                      momentum =
                                                                      0.9} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    37628916.737204
    rate = Regression_helpers.Nonmonotone {gamma = 0.5}, momentum = Regression_helpers.Rmsprop {
                                                                      decay = 0.2} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    441.512724
    rate = Regression_helpers.Nonmonotone {gamma = 0.5}, momentum = Regression_helpers.Rmsprop {
                                                                      decay = 0.5} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    358.516279
    rate = Regression_helpers.Nonmonotone {gamma = 0.5}, momentum = Regression_helpers.Rmsprop {
                                                                      decay = 0.9} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    217.338142
    rate = Regression_helpers.Nonmonotone {gamma = 0.5}, momentum = Regression_helpers.Adagrad on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    467.925556
    rate = Regression_helpers.Nonmonotone {gamma = 0.5}, momentum = Regression_helpers.Adam on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    357.051512
    rate = Regression_helpers.Nonmonotone {gamma = 0.5}, momentum = Regression_helpers.Nesterov {
                                                                      momentum =
                                                                      0.2} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    864.982822
    rate = Regression_helpers.Nonmonotone {gamma = 0.5}, momentum = Regression_helpers.Nesterov {
                                                                      momentum =
                                                                      0.5} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    881.223930
    rate = Regression_helpers.Nonmonotone {gamma = 0.5}, momentum = Regression_helpers.Nesterov {
                                                                      momentum =
                                                                      0.9} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    87865452.074529
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.2}, momentum =
    Regression_helpers.Rmsprop {decay = 0.2} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    441.512724
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.2}, momentum =
    Regression_helpers.Rmsprop {decay = 0.5} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    358.516279
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.2}, momentum =
    Regression_helpers.Rmsprop {decay = 0.9} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    200.561379
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.2}, momentum = Regression_helpers.Adagrad on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    467.925556
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.2}, momentum = Regression_helpers.Adam on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    357.051512
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.2}, momentum =
    Regression_helpers.Nesterov {momentum = 0.2} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    890.208661
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.2}, momentum =
    Regression_helpers.Nesterov {momentum = 0.5} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    879.412848
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.2}, momentum =
    Regression_helpers.Nesterov {momentum = 0.9} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    37628916.737204
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.5}, momentum =
    Regression_helpers.Rmsprop {decay = 0.2} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    441.512724
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.5}, momentum =
    Regression_helpers.Rmsprop {decay = 0.5} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    358.516279
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.5}, momentum =
    Regression_helpers.Rmsprop {decay = 0.9} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    217.338142
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.5}, momentum = Regression_helpers.Adagrad on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    467.925556
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.5}, momentum = Regression_helpers.Adam on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    357.051512
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.5}, momentum =
    Regression_helpers.Nesterov {momentum = 0.2} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    864.982822
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.5}, momentum =
    Regression_helpers.Nesterov {momentum = 0.5} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    881.223930
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.5}, momentum =
    Regression_helpers.Nesterov {momentum = 0.9} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    87865452.074529
    rate = Regression_helpers.Constant {alpha = 1.}, momentum = Regression_helpers.Rmsprop {
                                                                  decay = 0.2} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    880.371105
    rate = Regression_helpers.Constant {alpha = 1.}, momentum = Regression_helpers.Rmsprop {
                                                                  decay = 0.5} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    879.895545
    rate = Regression_helpers.Constant {alpha = 1.}, momentum = Regression_helpers.Rmsprop {
                                                                  decay = 0.9} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    871.432354
    rate = Regression_helpers.Constant {alpha = 1.}, momentum = Regression_helpers.Adagrad on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    987.394140
    rate = Regression_helpers.Constant {alpha = 1.}, momentum = Regression_helpers.Adam on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    885.182402
    rate = Regression_helpers.Constant {alpha = 1.}, momentum = Regression_helpers.Nesterov {
                                                                  momentum = 0.2} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    inf
    rate = Regression_helpers.Constant {alpha = 1.}, momentum = Regression_helpers.Nesterov {
                                                                  momentum = 0.5} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    inf
    rate = Regression_helpers.Constant {alpha = 1.}, momentum = Regression_helpers.Nesterov {
                                                                  momentum = 0.9} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    inf
    rate = Regression_helpers.Constant {alpha = 0.1}, momentum = Regression_helpers.Rmsprop {
                                                                   decay = 0.2} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    998.548267
    rate = Regression_helpers.Constant {alpha = 0.1}, momentum = Regression_helpers.Rmsprop {
                                                                   decay = 0.5} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    998.476509
    rate = Regression_helpers.Constant {alpha = 0.1}, momentum = Regression_helpers.Rmsprop {
                                                                   decay = 0.9} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    997.381344
    rate = Regression_helpers.Constant {alpha = 0.1}, momentum = Regression_helpers.Adagrad on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    1009.360861
    rate = Regression_helpers.Constant {alpha = 0.1}, momentum = Regression_helpers.Adam on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    998.610927
    rate = Regression_helpers.Constant {alpha = 0.1}, momentum = Regression_helpers.Nesterov {
                                                                   momentum = 0.2} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    inf
    rate = Regression_helpers.Constant {alpha = 0.1}, momentum = Regression_helpers.Nesterov {
                                                                   momentum = 0.5} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    inf
    rate = Regression_helpers.Constant {alpha = 0.1}, momentum = Regression_helpers.Nesterov {
                                                                   momentum = 0.9} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    25226897365592121702096763411084360828943297969806923814937777583548680342205956668481044851086567941097135495135980176533453133481883760048390275072.000000
    rate = Regression_helpers.Constant {alpha = 0.01}, momentum = Regression_helpers.Rmsprop {
                                                                    decay = 0.2} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    1010.496640
    rate = Regression_helpers.Constant {alpha = 0.01}, momentum = Regression_helpers.Rmsprop {
                                                                    decay = 0.5} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    1010.489247
    rate = Regression_helpers.Constant {alpha = 0.01}, momentum = Regression_helpers.Rmsprop {
                                                                    decay = 0.9} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    1010.377498
    rate = Regression_helpers.Constant {alpha = 0.01}, momentum = Regression_helpers.Adagrad on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    1011.578836
    rate = Regression_helpers.Constant {alpha = 0.01}, momentum = Regression_helpers.Adam on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    1010.498980
    rate = Regression_helpers.Constant {alpha = 0.01}, momentum = Regression_helpers.Nesterov {
                                                                    momentum =
                                                                    0.2} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    477561507312832227497309706683381031105714242419905354774805361274468101592172364019068722371212558457747402274456650851281431056479913246720.000000
    rate = Regression_helpers.Constant {alpha = 0.01}, momentum = Regression_helpers.Nesterov {
                                                                    momentum =
                                                                    0.5} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    45555310020443467623895602984623073458347218936279301993614107849146011304957243841158362620289157348604001646836986579412297187328.000000
    rate = Regression_helpers.Constant {alpha = 0.01}, momentum = Regression_helpers.Nesterov {
                                                                    momentum =
                                                                    0.9} on problem "918.630451 + 424.156769 * Regression_helpers.Linear":
    903073853758453738124046967084364873829388092304646177313671550798978112740473904733351669704650639015936.000000 |}]

let () = Format.printf "regression_benchmarks: finished@."
