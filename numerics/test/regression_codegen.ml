open Linalg
module NM = Numerics.Impl.Float
module R = NM.Regression
module Helpers_native = Regression_helpers.Native
module Num = Numerics_ocaml.Make (Numerics_ocaml.Default_pipeline)
module Float_const = Num.Lang.Field_cst
module Int_const = Num.Lang.Int_cst
module Helpers_meta =
  Regression_helpers.Make (Num.Lang.Repr) (Float_const) (Int_const) (Num.Impl)

let pp fmtr (a : float array) =
  Format.fprintf fmtr "[| " ;
  Format.pp_print_list
    ~pp_sep:(fun fmtr () -> Format.fprintf fmtr "; ")
    Format.pp_print_float
    fmtr
    (Array.to_list a) ;
  Format.fprintf fmtr " |]"

let pp' fmtr (a : floatarray) =
  Format.fprintf fmtr "[| " ;
  Format.pp_print_list
    ~pp_sep:(fun fmtr () -> Format.fprintf fmtr "; ")
    Format.pp_print_float
    fmtr
    (Float.Array.to_list a) ;
  Format.fprintf fmtr " |]"

let test_equivalence ?(niter = 1500.0) rng_state =
  let (_, _, (problem, inputs, outputs)) =
    Helpers_native.prepare_problem ~dimension:10 rng_state
  in
  let () =
    Format.printf
      "inputs: %d x %d, outputs = %d@."
      (Array.length inputs.(0))
      (Array.length inputs)
      (Array.length outputs)
  in
  ArrayLabels.iter Helpers_native.Rate.all ~f:(fun rate_spec ->
      ArrayLabels.iter Helpers_native.Momentum.all ~f:(fun momentum_spec ->
          let () =
            Format.printf
              "rate = %a, momentum = %a on problem %s:@."
              Regression_helpers.pp_rate
              rate_spec
              Regression_helpers.pp_momentum
              momentum_spec
              problem
          in

          let inputs_mat =
            NM.Mat.make Linalg.Tensor.Int.(rank_two 2 (Array.length inputs))
            @@ fun (c, r) -> inputs.(r).(c)
          in

          let t1 = Unix.gettimeofday () in

          let result =
            let open Helpers_native in
            let shape = Mat.Float.idim inputs_mat in
            let stopping =
              let open NM.Gradient_descent.Stopping in
              conj (gradient_norm ~bound:0.001) (max_iter ~niter)
            in
            let momentum = Momentum.make momentum_spec in
            let rate = Rate.make rate_spec in
            let cols = Tensor.Int.(numel (fst shape)) in
            let rows = Tensor.Int.(numel (snd shape)) in
            let inputs = Float.Array.make (Tensor.Int.numel shape) 0.0 in
            let omat = NM.Mat_backend.out_of_array shape inputs in
            let outputs = Float.Array.map_from_array Fun.id outputs in
            NM.Mat.(omat := inputs_mat) ;
            let init = Float.Array.make cols 0.0 in

            R.least_squares_generic
              ~stopping
              ~rate
              ~momentum
              ~cols
              ~rows
              ()
              inputs
              outputs
              init
          in

          let t2 = Unix.gettimeofday () in

          let result_dynlinked =
            let shape = Mat.Float.idim inputs_mat in
            let cols = Linalg.Tensor.Int.(numel (fst shape)) in
            let rows = Linalg.Tensor.Int.(numel (snd shape)) in

            let open Helpers_meta in
            let stopping =
              let open Num.Impl.Gradient_descent.Stopping in
              conj
                (gradient_norm ~bound:(Float_const.const 0.001))
                (max_iter ~niter:(Float_const.const niter))
            in
            let momentum = Momentum.make momentum_spec in
            let rate = Helpers_meta.Rate.make rate_spec in
            let inputs =
              Float.Array.map_from_array
                Fun.id
                (Array.make (Linalg.Tensor.Int.numel shape) 0.0)
            in
            let omat = NM.Mat_backend.out_of_array shape inputs in
            NM.Mat.(omat := inputs_mat) ;
            let inputs = Float.Array.map_to_array Fun.id inputs in
            let outputs = Array.copy outputs in
            let init = Array.make cols 0.0 in

            Float.Array.map_from_array Fun.id
            @@ Num.Regression.least_squares_generic
                 ~stopping
                 ~rate
                 ~momentum
                 ~cols
                 ~rows
                 inputs
                 outputs
                 init
          in
          let t3 = Unix.gettimeofday () in
          for i = 0 to Float.Array.length result - 1 do
            Format.printf "%f " (Float.Array.get result i)
          done ;
          Format.printf "@." ;
          for i = 0 to Float.Array.length result_dynlinked - 1 do
            Format.printf "%f " (Float.Array.get result_dynlinked i)
          done ;
          Format.printf "@." ;
          Log.debug "native: %f, generated: %f@." (t2 -. t1) (t3 -. t2)))

let%expect_test "regression_codegen: equivalence with native mode" =
  Log.set_log_level ERROR ;
  test_equivalence (Random.State.make [| 0x1337; 0x533D |]) ;
  [%expect
    {|
    inputs: 2 x 10, outputs = 10
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Rmsprop {decay = 0.2} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.693913 680.387224
    0.000000 0.000000
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Rmsprop {decay = 0.5} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.693508 680.387324
    171.693508 680.387324
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Rmsprop {decay = 0.9} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.693759 680.387265
    171.693759 680.387265
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.5}, momentum = Regression_helpers.Adagrad on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    302.890854 648.267771
    302.890854 648.267771
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.5}, momentum = Regression_helpers.Adam on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.692781 680.387499
    171.692781 680.387499
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Nesterov {momentum = 0.2} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.683141 680.389886
    171.683141 680.389886
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Nesterov {momentum = 0.5} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.693551 680.387327
    171.693551 680.387327
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Nesterov {momentum = 0.9} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    168.351425 681.180930
    168.351425 681.180930
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Rmsprop {decay = 0.2} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.693533 680.387321
    171.693533 680.387321
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Rmsprop {decay = 0.5} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.693515 680.387325
    171.693515 680.387325
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Rmsprop {decay = 0.9} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.693511 680.387326
    171.693511 680.387326
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.9}, momentum = Regression_helpers.Adagrad on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    302.890854 648.267771
    302.890854 648.267771
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.9}, momentum = Regression_helpers.Adam on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.692836 680.387488
    171.692836 680.387488
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Nesterov {momentum = 0.2} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.685681 680.389147
    171.685681 680.389147
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Nesterov {momentum = 0.5} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.692461 680.387571
    171.692461 680.387571
    rate = Regression_helpers.Backtracking {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Nesterov {momentum = 0.9} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    168.897437 681.045388
    168.897437 681.045388
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Rmsprop {decay = 0.2} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.694138 680.387170
    171.694138 680.387170
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Rmsprop {decay = 0.5} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.692367 680.387601
    171.692367 680.387601
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Rmsprop {decay = 0.9} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.693822 680.387249
    171.693822 680.387249
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.5}, momentum = Regression_helpers.Adagrad on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    302.890854 648.267771
    302.890854 648.267771
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.5}, momentum = Regression_helpers.Adam on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.692583 680.387550
    171.692583 680.387550
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Nesterov {momentum = 0.2} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.692383 680.387597
    171.692383 680.387597
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Nesterov {momentum = 0.5} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.693055 680.387431
    171.693055 680.387431
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.5}, momentum =
    Regression_helpers.Nesterov {momentum = 0.9} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.692376 680.387599
    171.692376 680.387599
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Rmsprop {decay = 0.2} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.693531 680.387318
    171.693531 680.387318
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Rmsprop {decay = 0.5} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.692656 680.387529
    171.692656 680.387529
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Rmsprop {decay = 0.9} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.692769 680.387506
    171.692769 680.387506
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.9}, momentum = Regression_helpers.Adagrad on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    302.890854 648.267771
    302.890854 648.267771
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.9}, momentum = Regression_helpers.Adam on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.692831 680.387487
    171.692831 680.387487
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Nesterov {momentum = 0.2} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.692501 680.387572
    171.692501 680.387572
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Nesterov {momentum = 0.5} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.692944 680.387459
    171.692944 680.387459
    rate = Regression_helpers.Adaptive {c = 0.1; gamma = 0.9}, momentum =
    Regression_helpers.Nesterov {momentum = 0.9} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.691884 680.387720
    171.691884 680.387720
    rate = Regression_helpers.Nonmonotone {gamma = 0.2}, momentum = Regression_helpers.Rmsprop {
                                                                      decay = 0.2} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.730287 680.378425
    171.730287 680.378425
    rate = Regression_helpers.Nonmonotone {gamma = 0.2}, momentum = Regression_helpers.Rmsprop {
                                                                      decay = 0.5} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.719980 680.380919
    171.719980 680.380919
    rate = Regression_helpers.Nonmonotone {gamma = 0.2}, momentum = Regression_helpers.Rmsprop {
                                                                      decay = 0.9} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.692759 680.387503
    171.692759 680.387503
    rate = Regression_helpers.Nonmonotone {gamma = 0.2}, momentum = Regression_helpers.Adagrad on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    302.890854 648.267771
    302.890854 648.267771
    rate = Regression_helpers.Nonmonotone {gamma = 0.2}, momentum = Regression_helpers.Adam on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.692796 680.387495
    171.692796 680.387495
    rate = Regression_helpers.Nonmonotone {gamma = 0.2}, momentum = Regression_helpers.Nesterov {
                                                                      momentum =
                                                                      0.2} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.692975 680.387413
    171.692975 680.387413
    rate = Regression_helpers.Nonmonotone {gamma = 0.2}, momentum = Regression_helpers.Nesterov {
                                                                      momentum =
                                                                      0.5} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.693886 680.387112
    171.693886 680.387112
    rate = Regression_helpers.Nonmonotone {gamma = 0.2}, momentum = Regression_helpers.Nesterov {
                                                                      momentum =
                                                                      0.9} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    -1848444773277570913672036965102754819811589371716651231224004608.000000 -7613980690614112761922565114225560595320732569767820279605624832.000000
    -1848444773277570913672036965102754819811589371716651231224004608.000000 -7613980690614112761922565114225560595320732569767820279605624832.000000
    rate = Regression_helpers.Nonmonotone {gamma = 0.5}, momentum = Regression_helpers.Rmsprop {
                                                                      decay = 0.2} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.693694 680.387278
    171.693694 680.387278
    rate = Regression_helpers.Nonmonotone {gamma = 0.5}, momentum = Regression_helpers.Rmsprop {
                                                                      decay = 0.5} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.693339 680.387364
    171.693339 680.387364
    rate = Regression_helpers.Nonmonotone {gamma = 0.5}, momentum = Regression_helpers.Rmsprop {
                                                                      decay = 0.9} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.690671 680.388016
    171.690671 680.388016
    rate = Regression_helpers.Nonmonotone {gamma = 0.5}, momentum = Regression_helpers.Adagrad on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    302.890854 648.267771
    302.890854 648.267771
    rate = Regression_helpers.Nonmonotone {gamma = 0.5}, momentum = Regression_helpers.Adam on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.692802 680.387494
    171.692802 680.387494
    rate = Regression_helpers.Nonmonotone {gamma = 0.5}, momentum = Regression_helpers.Nesterov {
                                                                      momentum =
                                                                      0.2} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.693215 680.387250
    171.693215 680.387250
    rate = Regression_helpers.Nonmonotone {gamma = 0.5}, momentum = Regression_helpers.Nesterov {
                                                                      momentum =
                                                                      0.5} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.743991 680.596445
    171.743991 680.596445
    rate = Regression_helpers.Nonmonotone {gamma = 0.5}, momentum = Regression_helpers.Nesterov {
                                                                      momentum =
                                                                      0.9} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    -448312161795851994672311625925852484309315800232043898207303550325543398552076328751857664.000000 -1846655195020257839547059559028553029436840450932269672459436604368193445326816479922880512.000000
    -448312161795851994672311625925852484309315800232043898207303550325543398552076328751857664.000000 -1846655195020257839547059559028553029436840450932269672459436604368193445326816479922880512.000000
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.2}, momentum =
    Regression_helpers.Rmsprop {decay = 0.2} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.730287 680.378425
    171.730287 680.378425
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.2}, momentum =
    Regression_helpers.Rmsprop {decay = 0.5} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.719980 680.380919
    171.719980 680.380919
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.2}, momentum =
    Regression_helpers.Rmsprop {decay = 0.9} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.692759 680.387503
    171.692759 680.387503
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.2}, momentum = Regression_helpers.Adagrad on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    302.890854 648.267771
    302.890854 648.267771
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.2}, momentum = Regression_helpers.Adam on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.692796 680.387495
    171.692796 680.387495
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.2}, momentum =
    Regression_helpers.Nesterov {momentum = 0.2} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.692975 680.387413
    171.692975 680.387413
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.2}, momentum =
    Regression_helpers.Nesterov {momentum = 0.5} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.693886 680.387112
    171.693886 680.387112
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.2}, momentum =
    Regression_helpers.Nesterov {momentum = 0.9} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    -1848444773277570913672036965102754819811589371716651231224004608.000000 -7613980690614112761922565114225560595320732569767820279605624832.000000
    -1848444773277570913672036965102754819811589371716651231224004608.000000 -7613980690614112761922565114225560595320732569767820279605624832.000000
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.5}, momentum =
    Regression_helpers.Rmsprop {decay = 0.2} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.693694 680.387278
    171.693694 680.387278
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.5}, momentum =
    Regression_helpers.Rmsprop {decay = 0.5} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.693339 680.387364
    171.693339 680.387364
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.5}, momentum =
    Regression_helpers.Rmsprop {decay = 0.9} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.690671 680.388016
    171.690671 680.388016
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.5}, momentum = Regression_helpers.Adagrad on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    302.890854 648.267771
    302.890854 648.267771
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.5}, momentum = Regression_helpers.Adam on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.692802 680.387494
    171.692802 680.387494
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.5}, momentum =
    Regression_helpers.Nesterov {momentum = 0.2} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.693215 680.387250
    171.693215 680.387250
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.5}, momentum =
    Regression_helpers.Nesterov {momentum = 0.5} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.743991 680.596445
    171.743991 680.596445
    rate = Regression_helpers.Nonmonotone_adaptive {gamma = 0.5}, momentum =
    Regression_helpers.Nesterov {momentum = 0.9} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    -448312161795851994672311625925852484309315800232043898207303550325543398552076328751857664.000000 -1846655195020257839547059559028553029436840450932269672459436604368193445326816479922880512.000000
    -448312161795851994672311625925852484309315800232043898207303550325543398552076328751857664.000000 -1846655195020257839547059559028553029436840450932269672459436604368193445326816479922880512.000000
    rate = Regression_helpers.Constant {alpha = 1.}, momentum = Regression_helpers.Rmsprop {
                                                                  decay = 0.2} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    172.395710 679.593079
    172.395710 679.593079
    rate = Regression_helpers.Constant {alpha = 1.}, momentum = Regression_helpers.Rmsprop {
                                                                  decay = 0.5} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.246616 679.874323
    171.246616 679.874323
    rate = Regression_helpers.Constant {alpha = 1.}, momentum = Regression_helpers.Rmsprop {
                                                                  decay = 0.9} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    171.201240 679.885431
    171.201240 679.885431
    rate = Regression_helpers.Constant {alpha = 1.}, momentum = Regression_helpers.Adagrad on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    74.209940 74.225543
    74.209940 74.225543
    rate = Regression_helpers.Constant {alpha = 1.}, momentum = Regression_helpers.Adam on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    366.696570 632.794939
    366.696570 632.794939
    rate = Regression_helpers.Constant {alpha = 1.}, momentum = Regression_helpers.Nesterov {
                                                                  momentum = 0.2} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    inf nan
    inf nan
    rate = Regression_helpers.Constant {alpha = 1.}, momentum = Regression_helpers.Nesterov {
                                                                  momentum = 0.5} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    -nan -nan
    -nan -nan
    rate = Regression_helpers.Constant {alpha = 1.}, momentum = Regression_helpers.Nesterov {
                                                                  momentum = 0.9} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    98577988549845523753024099557439725430266209669863360638381722803445336873565074970955746736406567176718664311217941427525712954482762440634074456806311621162387588591569779279536749453080462443116181708539450436224739293543830230443334587018029852006644176716811167425013285943859022311245097983602736496640.000000 -nan
    98577988549845523753024099557439725430266209669863360638381722803445336873565074970955746736406567176718664311217941427525712954482762440634074456806311621162387588591569779279536749453080462443116181708539450436224739293543830230443334587018029852006644176716811167425013285943859022311245097983602736496640.000000 -nan
    rate = Regression_helpers.Constant {alpha = 0.1}, momentum = Regression_helpers.Rmsprop {
                                                                   decay = 0.2} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    150.006841 150.006912
    150.006841 150.006912
    rate = Regression_helpers.Constant {alpha = 0.1}, momentum = Regression_helpers.Rmsprop {
                                                                   decay = 0.5} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    150.040160 150.040444
    150.040160 150.040444
    rate = Regression_helpers.Constant {alpha = 0.1}, momentum = Regression_helpers.Rmsprop {
                                                                   decay = 0.9} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    150.644514 150.647065
    150.644514 150.647065
    rate = Regression_helpers.Constant {alpha = 0.1}, momentum = Regression_helpers.Adagrad on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    7.583881 7.584022
    7.583881 7.584022
    rate = Regression_helpers.Constant {alpha = 0.1}, momentum = Regression_helpers.Adam on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    147.641932 147.664235
    147.641932 147.664235
    rate = Regression_helpers.Constant {alpha = 0.1}, momentum = Regression_helpers.Nesterov {
                                                                   momentum = 0.2} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    nan nan
    nan nan
    rate = Regression_helpers.Constant {alpha = 0.1}, momentum = Regression_helpers.Nesterov {
                                                                   momentum = 0.5} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    nan nan
    nan nan
    rate = Regression_helpers.Constant {alpha = 0.1}, momentum = Regression_helpers.Nesterov {
                                                                   momentum = 0.9} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    -inf nan
    -inf nan
    rate = Regression_helpers.Constant {alpha = 0.01}, momentum = Regression_helpers.Rmsprop {
                                                                    decay = 0.2} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    15.001371 15.001371
    15.001371 15.001371
    rate = Regression_helpers.Constant {alpha = 0.01}, momentum = Regression_helpers.Rmsprop {
                                                                    decay = 0.5} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    15.006762 15.006764
    15.006762 15.006764
    rate = Regression_helpers.Constant {alpha = 0.01}, momentum = Regression_helpers.Rmsprop {
                                                                    decay = 0.9} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    15.089057 15.089076
    15.089057 15.089076
    rate = Regression_helpers.Constant {alpha = 0.01}, momentum = Regression_helpers.Adagrad on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    0.759949 0.759951
    0.759949 0.759951
    rate = Regression_helpers.Constant {alpha = 0.01}, momentum = Regression_helpers.Adam on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    14.979121 14.979292
    14.979121 14.979292
    rate = Regression_helpers.Constant {alpha = 0.01}, momentum = Regression_helpers.Nesterov {
                                                                    momentum =
                                                                    0.2} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    -996098835354801655130499904225534096439001704190699382026730644599440093448317135178860109401167311185454110253927490699183999626202365013223494963826542932770105287201747553226267223674041480581069236344984949545889890325000660330192638157262278676259990233494258777165628760004456754556146961850919026688.000000 nan
    -996098835354801655130499904225534096439001704190699382026730644599440093448317135178860109401167311185454110253927490699183999626202365013223494963826542932770105287201747553226267223674041480581069236344984949545889890325000660330192638157262278676259990233494258777165628760004456754556146961850919026688.000000 nan
    rate = Regression_helpers.Constant {alpha = 0.01}, momentum = Regression_helpers.Nesterov {
                                                                    momentum =
                                                                    0.5} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    -9034356007816875491724105843455411289217574532112882444967558179384218488117032509074261209267537954140995839800852467832704775921501221649581326198130482360128167895750515902187781388686382629147958412775393090398002446840910323157273343570664067099856470016.000000 -37213669128821633246936547402195380557350758920551114159021954251065011699144178612009437452420835738240847487319980080558778694965424092913820643102277715526452734232733086310212087821442472438889840226765315167743490639992991446443812393259319332301264912384.000000
    -9034356007816875491724105843455411289217574532112882444967558179384218488117032509074261209267537954140995839800852467832704775921501221649581326198130482360128167895750515902187781388686382629147958412775393090398002446840910323157273343570664067099856470016.000000 -37213669128821633246936547402195380557350758920551114159021954251065011699144178612009437452420835738240847487319980080558778694965424092913820643102277715526452734232733086310212087821442472438889840226765315167743490639992991446443812393259319332301264912384.000000
    rate = Regression_helpers.Constant {alpha = 0.01}, momentum = Regression_helpers.Nesterov {
                                                                    momentum =
                                                                    0.9} on problem "171.693239 + 680.387389 * Regression_helpers.Log":
    -5854353490840506243426826485191891486183293370251102725551500547203414942069056289209005366194190600305123257843872259589204714406990968330991832628951106651339061128542472891903138592870355257408597209335428859213847396178982718065229852108866043366792347941575003311297093521710776320.000000 -24114831603137831427184491173301379673385495600945965460988248142252601363886890767624609644329042038537298597260640089554579086932604046817860276923017926768825349421634397020299481049886037824944151300813132460915116918745974731972263337532624155583600200022922158594960008420297539584.000000
    -5854353490840506243426826485191891486183293370251102725551500547203414942069056289209005366194190600305123257843872259589204714406990968330991832628951106651339061128542472891903138592870355257408597209335428859213847396178982718065229852108866043366792347941575003311297093521710776320.000000 -24114831603137831427184491173301379673385495600945965460988248142252601363886890767624609644329042038537298597260640089554579086932604046817860276923017926768825349421634397020299481049886037824944151300813132460915116918745974731972263337532624155583600200022922158594960008420297539584.000000 |}]
