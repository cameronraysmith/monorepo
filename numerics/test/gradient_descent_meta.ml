let () = Format.printf "gradient_descent_meta@."

let%expect_test "gradient_descent_meta" =
  let result =
    let module NM = Numerics.Impl.Float in
    let ( * ) = NM.Diff_scalar.mul in
    let ( + ) = NM.Diff_scalar.add in
    let ( - ) = NM.Diff_scalar.sub in
    let flt = NM.Diff_scalar.of_float in
    let f a =
      let x = NM.Diff_vec.get a 0 in
      let y = NM.Diff_vec.get a 1 in
      let sx = x - flt 3.0 in
      let sy = y - flt 22.0 in
      (sx * sx) + (sy * sy)
    in
    let (fwd, bwd) = NM.generate_vs ~input_dim:2 f in

    let fwd a = fwd (Float.Array.map_from_array Fun.id a) in

    let bwd a b =
      let a' = Float.Array.map_from_array Fun.id a in
      let b' = Float.Array.make (Array.length b) 0.0 in
      bwd a' b' ;
      Float.Array.iteri (fun i elt -> Array.set b i elt) b'
    in

    let module Num = Numerics_ocaml.Make (Numerics_ocaml.Default_pipeline) in
    let open Num in
    Gradient_descent.gradient_descent_generic
      ~stopping:
        (Impl.Gradient_descent.Stopping.gradient_norm
           ~bound:Lang.Field_cst.(const 0.001))
      ~rate:
        (Impl.Gradient_descent.Rate.constant ~rate:Lang.Field_cst.(const 0.001))
      ~momentum:
        (Impl.Gradient_descent.Momentum.rmsprop
           ~decay:Lang.Field_cst.(const 0.9))
      fwd
      bwd
      [| 0.0; 0.0 |]
  in
  for i = 0 to Int.pred @@ Array.length result do
    Format.printf "%f@." result.(i)
  done ;
  [%expect {|
    3.000000
    21.999582 |}]
