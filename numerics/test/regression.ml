(* This file contains remnants of hacks used during development *)
open Linalg
module NM = Numerics.Impl.Float
module R = NM.Regression
module BL = Basic_impl.Lang
module Vec_backend =
  Vec.Array_backed (BL.Empty) (BL.Codegen) (Tensor.Int)
    (Dense.Float_array.Native)

let rng_state = Random.State.make [| 0x1337; 0x533D |]

let conv = Float.Array.map_to_array Fun.id

let init = Float.Array.map_from_array Fun.id [| 700.0; 700.0 |]

let (pb, inputs, outputs) =
  Regression_helpers.Native.make_problem
    ~dimension:100
    ~coeff1:5.81
    ~coeff2:294.77
    ~basis:Regression_helpers.Square
    rng_state

let niter = 20.0

let stopping =
  let open NM.Gradient_descent.Stopping in
  conj (gradient_norm ~bound:1e-16) (max_iter ~niter)

(* let _traj = ref [] *)

(* let backtracking ~f k =
 *   let k k' =
 *     k (fun ~position ~descent ~gradient ~line_f ~step ->
 *         let grad_x = NM.Vec.get gradient 0 in
 *         let grad_y = NM.Vec.get gradient 1 in
 *         let pos = Float.Array.init 2 (NM.Vec.get position) in
 *         traj := (pos, f pos, grad_x, grad_y) :: !traj ;
 *         k' ~position ~descent ~gradient ~line_f ~step)
 *   in
 *   NM.Gradient_descent.Rate.backtracking
 *     ~alpha:1.
 *     ~c:0.1
 *     ~gamma:0.5
 *     ~max_iter:100
 *     ~f
 *     k *)

let rmsprop = NM.Gradient_descent.Momentum.rmsprop ~decay:0.9

let flag = ref true

let plot f =
  let xs = Array.init 100 (fun x -> 20. *. (float_of_int x -. 50.)) in
  let ys = Array.init 100 (fun x -> 20. *. (float_of_int x -. 50.)) in
  let fd = open_out "samples.dat" in
  let fmtr = Format.formatter_of_out_channel fd in
  Array.iter
    (fun x ->
      Array.iter
        (fun y ->
          let input = Float.Array.map_from_array Fun.id [| x; y |] in
          let res = f input in
          Format.fprintf fmtr "%f %f %f@." x y res)
        ys)
    xs ;
  close_out fd

(* let rate : NM.Gradient_descent.Rate.t =
 *  fun ~initial ~dimension ~f k ->
 *   let () =
 *     if !flag then (
 *       flag := false ;
 *       plot f)
 *     else ()
 *   in
 *   let k k' =
 *     k (fun ~position ~descent ~gradient ~line_f ~step ->
 *         let grad_x = NM.Vec.get gradient 0 in
 *         let grad_y = NM.Vec.get gradient 1 in
 *         let pos = Float.Array.init 2 (NM.Vec.get position) in
 *         traj := (pos, f pos, grad_x, grad_y) :: !traj ;
 *         k' ~position ~descent ~gradient ~line_f ~step)
 *   in
 *   (\* NM.Gradient_descent.Rate.constant ~alpha:1.0 ~f ~dimension k *\)
 *   (\* NM.Gradient_descent.Rate.decay ~a0:1.0 ~lambda:(15. /. niter) ~f ~dimension k *\)
 *   (\* NM.Gradient_descent.Rate.backtracking
 *    *   ~alpha:100.
 *    *   ~c:0.1
 *    *   ~gamma:0.5
 *    *   ~max_iter:100
 *    *   ~dimension
 *    *   ~f
 *    *   k *\)
 *   (\* NM.Gradient_descent.Rate.adaptive_backtracking
 *    *   ~alpha:100.
 *    *   ~c:0.01
 *    *   ~gamma:0.5
 *    *   ~max_iter:100.0
 *    *   ~dimension
 *    *   ~initial
 *    *   ~f
 *    *   k *\)
 *   NM.Gradient_descent.Rate.nonmonotone_line_search
 *     ~rolling_width:10
 *     ~alpha:100.
 *     ~gamma:0.5
 *     ~max_iter:50
 *     ~proximal:(NM.Gradient_descent.Proximal.lasso ~positive:true ~lambda:0.1)
 *     ~dimension
 *     ~initial
 *     ~f
 *     k *)

let _momentum = NM.Gradient_descent.Momentum.basic ~momentum:0.9

let _momentum = NM.Gradient_descent.Momentum.none

let _momentum = NM.Gradient_descent.Momentum.nesterov ~momentum:0.95

let _momentum = NM.Gradient_descent.Momentum.rmsprop ~decay:0.9

let _momentum = NM.Gradient_descent.Momentum.adagrad

let _momentum = NM.Gradient_descent.Momentum.adam ~beta1:0.9 ~beta2:0.99

let inputs_mat =
  NM.Mat.make Linalg.Tensor.Int.(rank_two 2 (Array.length inputs))
  @@ fun (c, r) -> inputs.(r).(c)

let outputs_vec =
  Vec_backend.in_of_array (Float.Array.map_from_array Fun.id outputs)

(* let result =
 *   R.least_squares
 *     ~stopping
 *     ~rate
 *     ~momentum:_momentum
 *     ~init
 *     ~inputs:inputs_mat
 *     ~outputs:outputs_vec
 *     () *)

let dump_mat file (m : (int * int) NM.Mat.t) =
  let rows = Linalg.Tensor.Int.numel @@ NM.Mat.rows m in
  let cols = Linalg.Tensor.Int.numel @@ NM.Mat.cols m in
  let oc = open_out file in
  let fmt = Format.formatter_of_out_channel oc in
  for i = 0 to rows - 1 do
    for j = 0 to cols - 1 do
      let elt = NM.Mat.get m (j, i) in
      Format.fprintf fmt "%f " elt
    done ;
    Format.fprintf fmt "\n"
  done ;
  close_out oc

let result =
  (* let () = dump_mat "inputs.dat" inputs_mat in
   * let () = dump_mat "outputs.dat" (NM.Mat.of_col outputs_vec) in *)
  let shape = Mat.Float.idim inputs_mat in
  let cols = Tensor.Int.(numel (fst shape)) in
  let rows = Tensor.Int.(numel (snd shape)) in
  let inputs = Float.Array.make (Tensor.Int.numel shape) 0.0 in
  let omat = NM.Mat_backend.out_of_array shape inputs in
  let outputs = Float.Array.map_from_array Fun.id outputs in
  NM.Mat.(omat := inputs_mat) ;
  R.least_squares_proximal
    ~stopping
    ~proximal:NM.Gradient_descent.Proximal.identity
    ~cols
    ~rows
    ()
    inputs
    outputs
    init

let%expect_test "regression" =
  Format.printf
    "result (%s): %a@."
    pb
    Regression_helpers.pp
    (Float.Array.map_to_array Fun.id result) ;
  [%expect
    {| result ("5.810000 + 294.770000 * Regression_helpers.Square"): 5.810 294.770 |}]

(* let points = Array.of_list (List.rev !traj) *)

(* let () =
     Format.printf
       "total backtracking function evals: %d@."
       !NM.Gradient_descent.Rate.total_calls *)

(* let () =
 *   let fd = open_out "trajectory.dat" in
 *   let fmtr = Format.formatter_of_out_channel fd in
 *   Array.iteri
 *     (fun _i (x, f_x, grad_x, grad_y) ->
 *       let x = Float.Array.map_to_array Fun.id x in
 *       Format.fprintf fmtr "%a %f %f %f@." Helpers.pp x f_x grad_x grad_y)
 *     points ;
 *   close_out fd *)
