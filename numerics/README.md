# Numerics

[numerics] is an **experimental** library.

It currently provides the following features:
- a small overlay on top of the [tangent] AD library
- gradient descent (not stochastic yet) with various momentum and line search modules
- proximal gradient descent (FASTA algorithm implementation) together with some basic proximal operators
  - soft thresholding
  - projection on the positive orthant
- linear regression based on the ingredients above
