open Basic_structures
open Ocaml_repr
module BL = Basic_impl.Lang

module Pipelines = struct
  module R = struct
    type t = int
  end

  module E = struct
    type t = float
  end

  open Simplifier.Components

  let repeat n x =
    List.flatten @@ List.init n (fun _ -> x)
    [@@ocaml.warning "-32"]

  let pair_simpl = pair_simplification (module R) (module E)

  let array_simpl = array_simplification (module R) (module E)

  let bool_simpl = constant_folding_bool (module BL.Int) (module E)

  let ring_simpl = constant_folding_ring (module BL.Int) (module E)

  let all_simpl = [pair_simpl; array_simpl; bool_simpl; ring_simpl]

  let unroll length = loop_unrolling length (module E)

  let dead_code = dead_code (module R) (module E)

  let o0 : (int, float) pass = compose []

  let o1 n : (int, float) pass = compose (repeat n all_simpl)

  let o2 unroll_strength n =
    compose (repeat n (all_simpl @ [unroll unroll_strength]))

  let o3 unroll_strength n =
    compose (repeat n (all_simpl @ [unroll unroll_strength; dead_code]))

  let default = o1 1
end

module O0 = (val Pipelines.o0)

module O1 (R : sig
  val repeat : int
end)
() : Simplifier.Components.Pass with type r = int and type e = float =
(val Pipelines.o1 R.repeat)

module O2 (R : sig
  val loop_unroll_strength : int

  val repeat : int
end)
() : Simplifier.Components.Pass with type r = int and type e = float =
(val Pipelines.o2 R.loop_unroll_strength R.repeat)

module O3 (R : sig
  val loop_unroll_strength : int

  val repeat : int
end)
() : Simplifier.Components.Pass with type r = int and type e = float =
(val Pipelines.o3 R.loop_unroll_strength R.repeat)

module Default_pipeline = (val Pipelines.default)

module Make
    (Pipeline : Simplifier.Components.Pass with type r = int and type e = float) =
struct
  module Simpl = Simpl.Make_float (Ocaml_lang.Int)
  module Lang = Simpl.Apply (Pipeline)

  module Embed (X : Basic_intf.Lang.Empty with type 'a m = 'a Ocaml_lang.Repr.m) =
  struct
    type 'a m = 'a Lang.m

    let inj = Lang.inj

    let prj = Lang.prj
  end

  module Mapper = Simplifier.Transform.Map (Ocaml_lang.Repr) (Lang.Repr) (Embed)

  module Field_storage = Mapper.Storage (Ocaml_lang.Make_storage (struct
    type t = float
  end))

  module State = Mapper.Enum (Ocaml_lang.Make_enum (struct
    include Grad.Native_impl.Node_state

    let const x = Ocaml_lang.Int_const.const (enum x)
  end))

  module State_storage = Mapper.Storage (Ocaml_lang.Make_storage (struct
    type t = Grad.Graph.state
  end))

  module Index_storage = Mapper.Storage (Ocaml_lang.Make_storage (struct
    type t = int
  end))

  module Index_constant = Mapper.Const (Ocaml_lang.Int_const)

  module Scalar :
    Numerics.Intf.Scalar with type 'a m = 'a Lang.m and type t = float = struct
    module Gen = Ocaml_lang
    include Mapper.Field (Gen.Float)

    let sqrt x = Lang.inj @@ Gen.call ["sqrt"] [Lang.prj x]

    let exp x = Lang.inj @@ Gen.call ["exp"] [Lang.prj x]

    let log x = Lang.inj @@ Gen.call ["log"] [Lang.prj x]

    let pow x y = Lang.inj @@ Gen.call ["( ** )"] [Lang.prj x; Lang.prj y]

    let pow_cst x (c : float) =
      Lang.inj @@ Gen.call ["( ** )"] [Lang.prj x; Gen.Float_const.const c]

    let cos x = Lang.inj @@ Gen.call ["cos"] [Lang.prj x]

    let sin x = Lang.inj @@ Gen.call ["sin"] [Lang.prj x]

    let acos x = Lang.inj @@ Gen.call ["acos"] [Lang.prj x]

    let asin x = Lang.inj @@ Gen.call ["asin"] [Lang.prj x]

    let tanh x = Lang.inj @@ Gen.call ["tanh"] [Lang.prj x]

    let atan x = Lang.inj @@ Gen.call ["atan"] [Lang.prj x]

    let sinh x = Lang.inj @@ Gen.call ["sinh"] [Lang.prj x]

    let cosh x = Lang.inj @@ Gen.call ["cosh"] [Lang.prj x]

    let is_inf x = Lang.inj @@ Gen.call ["Float"; "is_infinite"] [Lang.prj x]

    let is_nan x = Lang.inj @@ Gen.call ["Float"; "is_nan"] [Lang.prj x]

    let ( / ) = div

    let ( * ) = mul

    let ( + ) = add

    let ( - ) = sub

    let of_float x = Lang.inj (Gen.Float_const.const x)
  end

  module Codegen_monad =
    Basic_impl.Monad.Codegen_cps (Lang.Repr) (Lang.Sequencing)
  module Tensor =
    Linalg.Tensor.Make (Lang.Repr) (Codegen_monad) (Lang.Bool) (Lang.Int)
      (Lang.Int_ord)
      (Lang.Loop)
      (Lang.Product)
      (Lang.Exn)
      (Lang.Sequencing)
  module Impl =
    Numerics.Impl.Generic.Make (Lang.Repr) (Codegen_monad) (Tensor) (Lang.Bool)
      (Scalar)
      (Lang.Field_ord)
      (Field_storage)
      (State)
      (State_storage)
      (Lang.Int)
      (Lang.Int_ord)
      (Index_storage)
      (Index_constant)
      (Lang.Exn)
      (Lang.Loop)
      (Lang.Sequencing)
      (Lang.Lambda)
      (Lang.Product)
      (Lang.Array)

  module Compiler = Run_native.OCamlopt (struct
    let plugin_module_name = "numerics_ocaml_plugin__Plugin"
  end)

  let make_register ~register_fun ~key expr =
    let open Ast_helper in
    let register =
      Exp.ident
        (Ocaml_lang.loc
           (Longident.unflatten
              [ String.capitalize_ascii Compiler.Plugin_cmi.module_name;
                register_fun ]
           |> Option.get))
    in
    Str.value
      Asttypes.Nonrecursive
      [ Vb.mk
          (Pat.any ())
          Ocaml_lang.Lambda.(
            app (app register (Exp.constant (Const.string key))) expr) ]

  let make_module ~register_fun ~key result =
    let open Ast_helper in
    [ Str.value Asttypes.Nonrecursive [Vb.mk (Ocaml_lang.pvar "result") result];
      make_register
        ~register_fun
        ~key
        (Exp.ident (Ocaml_lang.loc (Longident.Lident "result"))) ]

  let make_module ~register_fun ~key result =
    let open Ast_helper in
    let module_body = make_module ~register_fun ~key result in
    Str.module_
    @@ Mb.mk (Ocaml_lang.loc (Some "Register")) (Mod.structure module_body)

  let codegen ~register_fun ~function_name expr =
    let expr = Lang.prj expr in
    let filename = Filename.temp_file "plugin" ".ml" in
    let oc = open_out filename in
    let fmtr = Format.formatter_of_out_channel oc in
    let generated_mod = make_module ~register_fun ~key:function_name expr in
    Format.fprintf fmtr "%a@." Pprintast.structure [generated_mod] ;
    close_out oc ;
    let md5 = Digest.(to_hex (file filename)) in
    let canonical_filename =
      Filename.concat
        (Filename.get_temp_dir_name ())
        (Format.asprintf "plugin_%s.ml" md5)
    in
    if Sys.file_exists canonical_filename then Sys.remove filename
    else Sys.rename filename canonical_filename ;
    canonical_filename

  let compile_and_load ~register_fun ~function_name code =
    let file = codegen ~register_fun ~function_name code in
    Log.debug "Emitted code for %s in %s@." function_name file ;

    Compiler.Plugin_cmi.load_embedded_cmi
      Embedded_cmis.numerics_ocaml_plugin__Plugin_cmi ;
    Compiler.Plugin_cmi.create_plugin_cmi ~dir:(Filename.get_temp_dir_name ()) ;
    Log.debug "Created plugin cmi@." ;

    let cmx =
      let cmx_file = Filename.chop_extension file ^ ".cmx" in
      if Sys.file_exists cmx_file then (
        Log.debug "Loading pre-compiled %s@." cmx_file ;
        cmx_file)
      else
        try
          let cmx_file' = Compiler.compile file in
          assert (String.equal cmx_file cmx_file') ;
          Log.debug "Compiled to %s@." cmx_file ;
          cmx_file
        with
        | Env.Error err ->
            Format.kasprintf (Log.error "%s@.") "%a" Env.report_error err ;
            exit 1
        | Typecore.Error (loc, env, err) ->
            let report = Typecore.report_error ~loc env err in
            Format.kasprintf
              (Log.error "%s@.")
              "%a"
              Location.print_report
              report ;
            exit 1
    in

    let output_prefix = Filename.chop_extension file in
    let cmxs = output_prefix ^ ".cmxs" in
    Compiler.link_shared cmxs [cmx] ;
    Dynlink.loadfile cmxs

  module Gradient_descent = struct
    open Impl

    let gradient_descent_generic :
        stopping:Gradient_descent.Stopping.t ->
        rate:Gradient_descent.Rate.t ->
        momentum:Gradient_descent.Momentum.t ->
        (float array -> float) ->
        (float array -> float array -> unit) ->
        float array ->
        float array =
     fun ~stopping ~rate ~momentum ->
      let term = Gradient_descent.gradient_descent ~stopping ~rate ~momentum in
      compile_and_load
        ~register_fun:"register_gradient_descent_generic"
        ~function_name:"gradient_descent_generic"
        term ;
      let (`Result f) =
        Numerics_ocaml_plugin.Plugin.get_gradient_descent_generic ()
      in
      f
  end

  module Regression = struct
    open Impl

    let least_squares_generic :
        ?stopping:Gradient_descent.Stopping.t ->
        ?rate:Gradient_descent.Rate.t ->
        ?momentum:Gradient_descent.Momentum.t ->
        cols:int ->
        rows:int ->
        float array ->
        float array ->
        float array ->
        float array =
     fun ?stopping ?rate ?momentum ~cols ~rows ->
      let term =
        Regression.least_squares_generic
          ?stopping
          ?rate
          ?momentum
          ~cols
          ~rows
          ()
      in
      compile_and_load
        ~register_fun:"register_least_squares_generic"
        ~function_name:"least_squares_generic"
        term ;
      let (`Result f) =
        Numerics_ocaml_plugin.Plugin.get_least_squares_generic ()
      in
      f

    let least_squares_proximal :
        ?stopping:Gradient_descent.Stopping.t ->
        proximal:Gradient_descent.Proximal.t ->
        cols:int ->
        rows:int ->
        float array ->
        float array ->
        float array ->
        float array =
     fun ?stopping ~proximal ~cols ~rows ->
      let term =
        Regression.least_squares_proximal ?stopping ~proximal ~cols ~rows ()
      in
      compile_and_load
        ~register_fun:"register_least_squares_proximal"
        ~function_name:"least_squares_proximal"
        term ;
      let (`Result f) =
        Numerics_ocaml_plugin.Plugin.get_least_squares_proximal ()
      in
      f
  end
end
