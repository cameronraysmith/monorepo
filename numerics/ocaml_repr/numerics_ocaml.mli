(** [Make] instantiates a module exposing functions whose implementation
    relies on generating, compiling and dynlinking OCaml code.

    The [Pipeline] argument corresponds to a composition of optimization passes.
    Note that stack consumption is proportional to O(pipeline size x generated code size).
 *)
module Make
    (Pipeline : Simplifier.Components.Pass with type r = int and type e = float) : sig
  module Lang :
    Simplifier.Components.S
      with type 'a base = Parsetree.expression
       and type r = int
       and type e = float

  module Impl : Numerics.Intf.S with type 'a m = 'a Lang.m and type elt = float

  module Gradient_descent : sig
    val gradient_descent_generic :
      stopping:Impl.Gradient_descent.Stopping.t ->
      rate:Impl.Gradient_descent.Rate.t ->
      momentum:Impl.Gradient_descent.Momentum.t ->
      (float array -> float) ->
      (float array -> float array -> unit) ->
      float array ->
      float array
  end

  module Regression : sig
    val least_squares_generic :
      ?stopping:Impl.Gradient_descent.Stopping.t ->
      ?rate:Impl.Gradient_descent.Rate.t ->
      ?momentum:Impl.Gradient_descent.Momentum.t ->
      cols:int ->
      rows:int ->
      float array ->
      float array ->
      float array ->
      float array

    val least_squares_proximal :
      ?stopping:Impl.Gradient_descent.Stopping.t ->
      proximal:Impl.Gradient_descent.Proximal.t ->
      cols:int ->
      rows:int ->
      float array ->
      float array ->
      float array ->
      float array
  end
end

(** The [O0] pipeline does nothing *)
module O0 : Simplifier.Components.Pass with type r = int and type e = float

(** The [O1] pipeline statically evaluates:
    - projections on statically known tuples
    - lengths of statically known arrays
    - boolean expressions
    - integer ring computations

    The [R.repeat] parameter specifies how many times these passes should
    be iterated.
 *)
module O1 (R : sig
  val repeat : int
end)
() : Simplifier.Components.Pass with type r = int and type e = float

(** In addition to what {!O1} does, [O2] performs loop unrolling for
    loops with statically known bounds, when the number of iterations
    is below [R.loop_unroll_strength].
*)
module O2 (R : sig
  val loop_unroll_strength : int

  val repeat : int
end)
() : Simplifier.Components.Pass with type r = int and type e = float

(** In addition to what {!O2} does, [O3] performs a limited form
    of dead code elimination. *)
module O3 (R : sig
  val loop_unroll_strength : int

  val repeat : int
end)
() : Simplifier.Components.Pass with type r = int and type e = float

(** The default pipeline is {!O1} witn [repeat] = 1 *)
module Default_pipeline :
  Simplifier.Components.Pass with type r = int and type e = float
