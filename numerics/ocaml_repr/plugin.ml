let least_squares_generic :
    (float array -> float array -> float array -> float array) option ref =
  ref None

let least_squares_proximal :
    (float array -> float array -> float array -> float array) option ref =
  ref None

let gradient_descent_generic :
    ((float array -> float) ->
    (float array -> float array -> unit) ->
    float array ->
    float array)
    option
    ref =
  ref None

(* let arr_arr_to_arr :
 *     (string, float array -> float array -> float array) Hashtbl.t =
 *   Hashtbl.create 13
 *
 * let register_arr_arr_to_arr key f =
 *   Format.printf "Plugin: registered %s@." key ;
 *   Hashtbl.add arr_arr_to_arr key f *)

let register_least_squares_generic _ f = least_squares_generic := Some f

let get_least_squares_generic () =
  match !least_squares_generic with
  | None -> invalid_arg "get_least_squares_generic"
  | Some f -> `Result f

let register_least_squares_proximal _ f = least_squares_proximal := Some f

let get_least_squares_proximal () =
  match !least_squares_proximal with
  | None -> invalid_arg "get_least_squares_proximal"
  | Some f -> `Result f

let register_gradient_descent_generic _ f = gradient_descent_generic := Some f

let get_gradient_descent_generic () =
  match !gradient_descent_generic with
  | None -> invalid_arg "get_gradient_descent_generic"
  | Some f -> `Result f
