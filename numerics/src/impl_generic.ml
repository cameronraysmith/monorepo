open Numerics_intf

module Make
    (Repr : Basic_intf.Lang.Empty)
    (Monad : Basic_intf.Codegen_monad with type 'a m = 'a Repr.m)
    (T : Linalg.Intf.Tensor
           with type 'a m = 'a Repr.m
            and type 'a k = 'a Monad.t
            and type pos = int)
    (B : Basic_intf.Lang.Bool with type 'a m = 'a Repr.m)
    (R : Scalar with type 'a m = 'a Repr.m)
    (R_ord : Basic_intf.Lang.Infix_order
               with type 'a m = 'a Repr.m
                and type t = R.t)
    (R_st : Basic_intf.Lang.Storage
              with type 'a m = 'a Repr.m
               and type elt = R.t)
    (S_enum : Basic_intf.Lang.Enum
                with type 'a m = 'a Repr.m
                 and type t = Grad.Graph.state)
    (S_st : Basic_intf.Lang.Storage
              with type 'a m = 'a Repr.m
               and type elt = Grad.Graph.state)
    (I_ring : Basic_intf.Lang.Ring with type 'a m = 'a Repr.m and type t = T.pos)
    (I_ord : Basic_intf.Lang.Infix_order
               with type 'a m = 'a Repr.m
                and type t = T.pos)
    (I_st : Basic_intf.Lang.Storage
              with type 'a m = 'a Repr.m
               and type elt = I_ring.t)
    (I_cst : Basic_intf.Lang.Const with type 'a m = 'a Repr.m and type t = T.pos)
    (E : Basic_intf.Lang.Exn with type 'a m = 'a Repr.m)
    (Loop : Basic_intf.Lang.Loop
              with type 'a m = 'a Repr.m
               and type index = I_ring.t)
    (M : Basic_intf.Lang.Sequencing with type 'a m = 'a Repr.m)
    (L : Basic_intf.Lang.Lambda with type 'a m = 'a Repr.m)
    (P : Basic_intf.Lang.Product with type 'a m = 'a Repr.m)
    (A : Basic_intf.Lang.Array
           with type 'a m = 'a Repr.m
            and type index = T.pos
            and type elt = R.t) :
  Numerics_intf.S
    with type 'a k = 'a Monad.t
     and type 'a m = 'a Repr.m
     and type 'a shape = 'a T.t
     and type arr = A.t
     and type elt = R.t = struct
  type 'a k = 'a Monad.t

  type 'a m = 'a Repr.m

  type 'a shape = 'a T.t

  type arr = A.t

  type elt = R.t

  module G =
    Grad.Make (Repr) (Monad) (T) (B) (R) (R_st) (S_enum) (S_st) (I_ring) (I_ord)
      (I_cst)
      (E)
      (M)
      (L)
      (P)
      (A)
  module Vec = Linalg.Vec.Make (Repr) (Monad) (T) (B) (R) (R_st) (E) (M)
  module Mat =
    Linalg.Mat.Make (Repr) (Monad) (T) (B) (R) (R_st) (I_ring) (E) (M) (P)
  module Vec_backend = Linalg.Vec.Array_backed (Repr) (Monad) (T) (A)
  module Mat_backend =
    Linalg.Mat.Array_backed_column_major (Repr) (Monad) (T) (B) (I_ring) (I_ord)
      (A)
      (E)
      (P)
      (M)

  module Generic_dimension = struct
    (* Dimension-independent nodes *)

    let div x y =
      G.create
        ~tag:"div"
        ~backward:[| x; y |]
        ~transfer:(fun v -> Vec.map2 R.div v.(0) v.(1))
        ~jacobian:(fun v ->
          let open Monad.Infix in
          let c1 = Mat.diagonal @@ Vec.map (fun y -> R.(one / y)) v.(1) in
          let* diag = Vec.map2 (fun x y -> R.(neg (x / (y * y)))) v.(0) v.(1) in
          let c2 = Mat.diagonal diag in
          Mat.(concat_horiz c1 c2))

    let pow x y =
      G.create
        ~tag:"pow"
        ~backward:[| x; y |]
        ~transfer:(fun v -> Vec.map2 R.pow v.(0) v.(1))
        ~jacobian:(fun v ->
          let open Monad.Infix in
          let* diag1 =
            Vec.map2 (fun x y -> R.(y * pow x (y - one))) v.(0) v.(1)
          in
          let c1 = Mat.diagonal diag1 in
          let* diag2 = Vec.map2 (fun x y -> R.(pow x y * log x)) v.(0) v.(1) in
          let c2 = Mat.diagonal diag2 in
          Mat.(concat_horiz c1 c2))

    let pow_cst x cst =
      G.create
        ~tag:"pow_cst"
        ~backward:[| x |]
        ~transfer:(fun v ->
          let exponent = Vec.(const (idim v.(0)) (R.of_float cst)) in
          Vec.map2 R.pow v.(0) exponent)
        ~jacobian:(fun v ->
          let open Monad.Infix in
          let exponent = Vec.(const (idim v.(0)) (R.of_float cst)) in
          let* diag1 =
            Vec.map2 (fun x y -> R.(y * pow x (y - one))) v.(0) exponent
          in
          let c1 = Mat.diagonal diag1 in
          let* diag2 =
            Vec.map2 (fun x y -> R.(y * pow x (y - one))) v.(0) exponent
          in
          let c2 = Mat.diagonal diag2 in
          Mat.(concat_horiz c1 c2))

    let lift1 tag f j x =
      G.create
        ~tag
        ~backward:[| x |]
        ~transfer:(fun v -> Monad.return @@ Vec.map f v.(0))
        ~jacobian:(fun v -> Monad.return @@ Mat.diagonal (Vec.map j v.(0)))

    let exp x = lift1 "exp" R.exp R.exp x

    let log x = lift1 "log" R.log (fun x -> R.(one / x)) x

    let cos x = lift1 "cos" R.cos (fun x -> R.(neg (sin x))) x

    let sin x = lift1 "sin" R.sin (fun x -> R.(cos x)) x

    let asin x = lift1 "asin" R.asin (fun x -> R.(one / sqrt (one - (x * x)))) x

    let acos x =
      lift1 "acos" R.acos (fun x -> R.(neg (one / sqrt (one - (x * x))))) x

    let atan x = lift1 "atan" R.atan (fun x -> R.(one / ((x * x) + one))) x

    let tanh x =
      lift1
        "tanh"
        R.tanh
        (fun x ->
          let c = R.cosh x in
          R.(one / c * c))
        x

    let cosh x =
      lift1 "cosh" R.cosh (fun x -> R.((exp x + exp (neg x)) / of_float 2.)) x

    let sinh x =
      lift1 "sinh" R.sinh (fun x -> R.((exp x - exp (neg x)) / of_float 2.)) x
  end

  module Diff_scalar = struct
    include G.Scalar

    let ( + ) = add

    let ( - ) = sub

    let ( * ) = mul

    let of_float f = inj (R.of_float f)

    let of_scalar = inj

    let to_scalar = prj

    let div x y =
      of_generic
      @@ Generic_dimension.div
           (to_generic x)
           (to_generic y)
           ~output_dim:G.Output_dim.one

    let ( / ) = div

    let pow x y =
      of_generic
      @@ Generic_dimension.pow
           (to_generic x)
           (to_generic y)
           ~output_dim:G.Output_dim.one

    let pow_cst x cst =
      of_generic
      @@ Generic_dimension.pow_cst
           (to_generic x)
           cst
           ~output_dim:G.Output_dim.one

    let ( ** ) = pow

    let sqrt x = pow x (inj (R.of_float 0.5))

    let exp x =
      of_generic
      @@ Generic_dimension.exp (to_generic x) ~output_dim:G.Output_dim.one

    let log x =
      of_generic
      @@ Generic_dimension.log (to_generic x) ~output_dim:G.Output_dim.one

    let sin x =
      of_generic
      @@ Generic_dimension.sin (to_generic x) ~output_dim:G.Output_dim.one

    let cos x =
      of_generic
      @@ Generic_dimension.cos (to_generic x) ~output_dim:G.Output_dim.one

    let asin x =
      of_generic
      @@ Generic_dimension.asin (to_generic x) ~output_dim:G.Output_dim.one

    let acos x =
      of_generic
      @@ Generic_dimension.acos (to_generic x) ~output_dim:G.Output_dim.one

    let atan x =
      of_generic
      @@ Generic_dimension.atan (to_generic x) ~output_dim:G.Output_dim.one

    let tanh x =
      of_generic
      @@ Generic_dimension.tanh (to_generic x) ~output_dim:G.Output_dim.one

    let cosh x =
      of_generic
      @@ Generic_dimension.cosh (to_generic x) ~output_dim:G.Output_dim.one

    let sinh x =
      of_generic
      @@ Generic_dimension.sinh (to_generic x) ~output_dim:G.Output_dim.one
  end

  module Diff_vec = struct
    type index = int

    type array = floatarray

    type scalar = Diff_scalar.t

    include G.Vector

    let to_array = prj

    let of_array = inj

    let output_dim = G.Output_dim.same_as 0

    let ( + ) = add

    let ( - ) = sub

    let ( * ) = mul

    let ( / ) x y =
      of_generic
      @@ Generic_dimension.div (to_generic x) (to_generic y) ~output_dim

    let of_vec (v : 'a Vec.t) =
      of_generic
      @@ G.create
           ~tag:"of_vec"
           ~backward:[||]
           ~transfer:(fun _v -> Monad.return v)
           ~jacobian:(fun _v ->
             Monad.return (Mat.const T.(tensor empty (Vec.idim v)) R.zero))
           ~output_dim

    let broadcast (dim : int Repr.m) (x : Diff_scalar.t) =
      let shape = T.rank_one dim in
      of_generic
      @@ G.create
           ~tag:"broadcast"
           ~backward:[| Diff_scalar.to_generic x |]
           ~transfer:(fun v ->
             let open Monad.Infix in
             let* elt = Vec.get v.(0) I_ring.zero in
             Monad.return Vec.(const shape elt))
           ~jacobian:(fun _v ->
             Monad.return (Mat.of_col (Vec.const shape R.one)))
           ~output_dim

    let reduce_scalar_array (a : scalar Array.t) =
      Diff_scalar.of_generic
      @@ G.create
           ~tag:"of_scalar_array"
           ~backward:(Array.map Diff_scalar.to_generic a)
           ~transfer:(fun vecs ->
             let open Monad.Infix in
             let rec loop i acc =
               if i = 0 then
                 let* elt = Vec.get vecs.(i) I_ring.zero in
                 Monad.return (R.add acc elt)
               else
                 let* elt = Vec.get vecs.(i) I_ring.zero in
                 loop (Int.pred i) (R.add acc elt)
             in
             let* acc = loop (Int.pred (Array.length vecs)) R.zero in
             let shape = T.rank_one I_ring.one in
             Monad.return @@ Vec.make shape (fun _i -> acc))
           ~jacobian:(fun vecs ->
             let total_length =
               T.rank_one
               @@ Array.fold_left
                    (fun acc vec -> I_ring.add acc (T.numel (Vec.idim vec)))
                    I_ring.zero
                    vecs
             in
             Monad.return (Mat.of_row (Vec.const total_length R.one)))
           ~output_dim:G.Output_dim.one

    let reduce (x : t) =
      Diff_scalar.of_generic
      @@ G.create
           ~tag:"reduce"
           ~backward:[| to_generic x |]
           ~transfer:(fun v ->
             let (Vec (length, v)) = v.(0) in
             let result =
               T.fold
                 (module R_st)
                 (fun i acc ->
                   let vi = v i in
                   R.add acc vi)
                 length
                 R.zero
             in
             Monad.return (Vec.const T.scalar result))
           ~jacobian:(fun v ->
             let (Vec (length, _)) = v.(0) in
             Monad.return (Mat.of_row (Vec.const length R.one)))
           ~output_dim:G.Output_dim.one

    let pow x y =
      of_generic
      @@ Generic_dimension.pow (to_generic x) (to_generic y) ~output_dim

    let ( ** ) = pow

    let pow_cst x c =
      of_generic @@ Generic_dimension.pow_cst (to_generic x) c ~output_dim

    let sqrt x = pow_cst x 0.5

    let exp x = of_generic @@ Generic_dimension.exp (to_generic x) ~output_dim

    let log x = of_generic @@ Generic_dimension.log (to_generic x) ~output_dim

    let sin x = of_generic @@ Generic_dimension.sin (to_generic x) ~output_dim

    let cos x = of_generic @@ Generic_dimension.cos (to_generic x) ~output_dim

    let asin x = of_generic @@ Generic_dimension.asin (to_generic x) ~output_dim

    let acos x = of_generic @@ Generic_dimension.acos (to_generic x) ~output_dim

    let atan x = of_generic @@ Generic_dimension.atan (to_generic x) ~output_dim

    let tanh x = of_generic @@ Generic_dimension.tanh (to_generic x) ~output_dim

    let cosh x = of_generic @@ Generic_dimension.cosh (to_generic x) ~output_dim

    let sinh x = of_generic @@ Generic_dimension.sinh (to_generic x) ~output_dim

    let l2_squared x = reduce (pow_cst x 2.0)

    let l2 x = Diff_scalar.sqrt (l2_squared x)

    let dot x y = reduce (mul x y)

    let const_mm (mat : (int * int) Mat.t) (v : t) =
      let shape = Mat.idim mat in
      let rows = T.numel (T.snd shape) in
      of_generic
      @@ G.create
           ~tag:"const_mm"
           ~backward:[| to_generic v |]
           ~transfer:(fun v ->
             let open Monad.Infix in
             let* res = Mat.mm mat (Mat.of_col v.(0)) in
             let* col = Mat.col res I_ring.zero in
             Monad.return col)
           ~jacobian:(fun _v -> Monad.return mat)
           ~output_dim:(G.Output_dim.dim rows)
  end

  let generate_ss :
      (Diff_scalar.t -> Diff_scalar.t Monad.t) ->
      ((R.t -> R.t) * (R.t -> R.t)) m Monad.t =
    G.generate_ss

  let generate_sv :
      (Diff_scalar.t -> Diff_vec.t Monad.t) ->
      ((R.t -> arr -> unit) * (R.t -> arr -> unit)) m Monad.t =
    G.generate_sv

  let generate_vs :
      input_dim:int m ->
      (Diff_vec.t -> Diff_scalar.t Monad.t) ->
      ((arr -> R.t) * (arr -> arr -> unit)) m Monad.t =
    G.generate_vs

  let generate_vv :
      input_dim:int m ->
      (Diff_vec.t -> Diff_vec.t Monad.t) ->
      ((arr -> arr -> unit) * (arr -> arr -> unit)) m Monad.t =
    G.generate_vv

  module Gradient_descent =
    Gradient.Make (Repr) (Monad) (T) (B) (I_ring) (I_ord) (I_st) (R) (R_ord)
      (R_st)
      (E)
      (Loop)
      (M)
      (L)
      (A)

  let default_stopping =
    Gradient_descent.Stopping.gradient_norm ~bound:(R.of_float 1e-8)

  let default_rate =
    Gradient_descent.Rate.adaptive_backtracking
      ~max_rate:(R.of_float 100.0)
      ~slack:(R.of_float 0.1)
      ~shrinking_factor:(R.of_float 0.5)
      ~max_iter:(R.of_float 100.0)

  let default_momentum =
    Gradient_descent.Momentum.adam
      ~beta1:(R.of_float 0.9)
      ~beta2:(R.of_float 0.99)

  module Regression = struct
    (* Specialize a given function vector -> vector -> scalar function
       for the given input dimensions. *)
    let _specialize_vvs ~input_dim1 ~input_dim2
        (f : Diff_vec.t -> Diff_vec.scalar k) =
      let open Monad.Infix in
      let*! input_dim = I_ring.add input_dim1 input_dim2 in
      let* f_with_grad = G.generate_vs ~input_dim f in
      let*! fwd = P.fst f_with_grad in
      let*! bwd = P.snd f_with_grad in
      let*! fbuff = A.make input_dim R.zero in
      let*! jbuff = A.make input_dim R.zero in
      let* jbuff_in =
        Mat_backend.in_of_array (T.rank_two input_dim I_ring.one) jbuff
      in
      let copy_to_fbuff arrs =
        let open M in
        seq (A.blit arrs.(0) I_ring.zero fbuff I_ring.zero input_dim1)
        @@ fun () -> A.blit arrs.(1) I_ring.zero fbuff input_dim1 input_dim2
      in
      Monad.return @@ fun x y ->
      G.Scalar.of_generic
      @@ G.unsafe_create
           ~tag:""
           ~backward:[| G.Vector.to_generic x; G.Vector.to_generic y |]
           ~transfer:(fun ~inp ~out ->
             copy_to_fbuff inp >>! fun () ->
             let*! res = L.app fwd fbuff in
             let*! _ = A.set out I_ring.zero res in
             Monad.return M.unit)
           ~jacobian:(fun arrs ->
             copy_to_fbuff arrs >>! fun () ->
             L.(app (app bwd fbuff) jbuff) >>! fun () -> Monad.return jbuff_in)
           ~output_dim:G.Output_dim.one

    module Raw = struct
      let least_squares_generic ?(stopping = default_stopping)
          ?(rate = default_rate) ?(momentum = default_momentum) ~cols ~rows
          inputs_data outputs_data initial_data =
        let open Monad.Infix in
        let*! cols' = I_cst.const cols in
        let*! rows' = I_cst.const rows in

        let*! odim = A.length outputs_data in
        let* inputs =
          Mat_backend.in_of_array (T.rank_two cols' rows') inputs_data
        in
        let outputs = Vec_backend.in_of_array outputs_data in
        let to_be_optimized (unknowns : Diff_vec.t) =
          let transformed = Diff_vec.const_mm inputs unknowns in
          let delta = Diff_vec.sub transformed (Diff_vec.const outputs) in
          let delta_pow2 = Diff_vec.pow_cst delta 2.0 in
          Monad.return (Diff_vec.reduce delta_pow2)
        in
        Monad.return
        @@ B.dispatch I_ord.(rows' = odim)
        @@ function
        | true ->
            Monad.run
            @@ let* f_with_grad =
                 G.generate_vs ~input_dim:cols' to_be_optimized
               in
               let*! f = P.fst f_with_grad in
               let*! f_grad = P.snd f_with_grad in
               Gradient_descent.Raw.(
                 gradient_descent
                   ~stopping
                   ~rate
                   ~momentum
                   f
                   f_grad
                   initial_data)
        | false -> E.raise_ (Failure "least_squares: dimensions mismatch")

      let least_squares_proximal ?(stopping = default_stopping) ~proximal ~cols
          ~rows inputs_data outputs_data initial_data =
        let open Monad.Infix in
        let*! cols' = I_cst.const cols in
        let*! rows' = I_cst.const rows in
        let*! odim = A.length outputs_data in
        let* inputs =
          Mat_backend.in_of_array (T.rank_two cols' rows') inputs_data
        in
        let outputs = Vec_backend.in_of_array outputs_data in
        let to_be_optimized (unknowns : Diff_vec.t) =
          let transformed = Diff_vec.const_mm inputs unknowns in
          let delta = Diff_vec.sub transformed (Diff_vec.const outputs) in
          let delta_pow2 = Diff_vec.pow_cst delta 2.0 in
          Monad.return (Diff_vec.reduce delta_pow2)
        in
        Monad.return
        @@ B.dispatch I_ord.(rows' = odim)
        @@ function
        | true ->
            Monad.run
            @@ let* f_with_grad =
                 G.generate_vs ~input_dim:cols' to_be_optimized
               in
               let*! f = P.fst f_with_grad in
               let*! f_grad = P.snd f_with_grad in
               Gradient_descent.Raw.(
                 fasta
                   ~rolling_width:(I_cst.const 10)
                   ~max_rate:R.one
                   ~shrinking_factor:(R.of_float 0.2)
                   ~max_iter:(I_cst.const 50)
                   ~stopping
                   ~proximal
                   f
                   f_grad
                   initial_data)
        | false ->
            E.raise_ (Failure "least_squares_proximal: dimensions mismatch")
    end

    let least_squares_generic ?(stopping = default_stopping)
        ?(rate = default_rate) ?(momentum = default_momentum) ~cols ~rows () =
      L.lam @@ fun (inputs_data : arr m) ->
      L.lam @@ fun (outputs : arr m) ->
      L.lam @@ fun (init : arr m) ->
      Monad.run
      @@ Raw.least_squares_generic
           ~stopping
           ~rate
           ~momentum
           ~cols
           ~rows
           inputs_data
           outputs
           init

    let least_squares_proximal ?(stopping = default_stopping) ~proximal ~cols
        ~rows () =
      L.lam @@ fun (inputs_data : arr m) ->
      L.lam @@ fun (outputs : arr m) ->
      L.lam @@ fun (init : arr m) ->
      Monad.run
      @@ Raw.least_squares_proximal
           ~stopping
           ~proximal
           ~cols
           ~rows
           inputs_data
           outputs
           init
  end
end
