open Basic_intf.Lang

module Make_ring
    (Repr : Empty)
    (I_ring : Ring with type 'a m = 'a Repr.m)
    (I_ord : Basic_intf.Lang.Infix_order
               with type 'a m = 'a Repr.m
                and type t = I_ring.t)
    (I_st : Storage with type 'a m = 'a Repr.m and type elt = I_ring.t) =
struct
  open I_ring
  open I_st
  include I_ord

  let one = one

  let zero = zero

  let ( + ) = add

  let ( * ) = mul

  let ( - ) = sub

  let ( ~- ) = neg

  let ref = create

  let ( := ) = set

  let ( !! ) = get
end
[@@inline]

module Make_field
    (Repr : Empty)
    (F : Field with type 'a m = 'a Repr.m)
    (F_ord : Basic_intf.Lang.Infix_order
               with type 'a m = 'a Repr.m
                and type t = F.t)
    (F_st : Storage with type 'a m = 'a Repr.m and type elt = F.t) =
struct
  open F
  open F_st
  include F_ord

  let one = one

  let zero = zero

  let ( + ) = add

  let ( * ) = mul

  let ( / ) = div

  let ( ~- ) = neg

  let ref = create

  let ( := ) = set

  let ( !! ) = get
end
[@@inline]
