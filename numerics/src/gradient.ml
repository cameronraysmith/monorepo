open Basic_intf.Lang
open Numerics_intf

module Make
    (Repr : Empty)
    (Monad : Basic_intf.Codegen_monad with type 'a m = 'a Repr.m)
    (T : Linalg.Intf.Tensor
           with type 'a m = 'a Repr.m
            and type 'a k = 'a Monad.t)
    (B : Bool with type 'a m = 'a Repr.m)
    (I_ring : Ring with type 'a m = 'a Repr.m and type t = T.pos)
    (I_ord : Basic_intf.Lang.Infix_order
               with type 'a m = 'a Repr.m
                and type t = I_ring.t)
    (I_st : Storage with type 'a m = 'a Repr.m and type elt = I_ring.t)
    (F : Scalar with type 'a m = 'a Repr.m)
    (F_ord : Infix_order with type 'a m = 'a Repr.m and type t = F.t)
    (F_st : Storage with type 'a m = 'a Repr.m and type elt = F.t)
    (E : Exn with type 'a m = 'a Repr.m)
    (Loop : Loop with type 'a m = 'a Repr.m and type index = I_ring.t)
    (M : Sequencing with type 'a m = 'a Repr.m)
    (L : Lambda with type 'a m = 'a Repr.m)
    (A : Array
           with type 'a m = 'a Repr.m
            and type index = I_ring.t
            and type elt = F.t) :
  Gradient_S
    with type 'a k = 'a Monad.t
     and type 'a m = 'a Repr.m
     and type index = I_ring.t
     and type elt = F.t
     and type arr = A.t = struct
  open Monad.Infix

  type 'a k = 'a Monad.t

  type 'a m = 'a Repr.m

  type elt = F.t

  type index = I_ring.t

  type arr = A.t

  module Vec = struct
    module Vec = Linalg.Vec.Make (Repr) (Monad) (T) (B) (F) (F_st) (E) (M)
    module Vec_backend = Linalg.Vec.Array_backed (Repr) (Monad) (T) (A)
    include Vec
    include Vec_backend
  end

  module V = Vec

  type vec = index Vec.t

  type ovec = index Vec.out

  type array_with_overlay = A.t m * vec * ovec

  (* Syntactic heleprs *)

  module I = Stitched_syntax.Make_ring (Repr) (I_ring) (I_ord) (I_st)

  module F = struct
    include F
    include Stitched_syntax.Make_field (Repr) (F) (F_ord) (F_st)
  end

  module Syntax = struct
    include Loop
    include M
    include L
    include B
  end

  let ( >>> ) = M.seq

  let ( $ ) = L.app

  let pp_debug fmtr vec =
    Format.fprintf
      fmtr
      "%.10f %.10f"
      (Obj.magic (Vec.get vec I_ring.zero))
      (Obj.magic (Vec.get vec I_ring.one))
    [@@ocaml.warning "-32"]

  module Helpers = struct
    let max x y =
      B.dispatch F.(x < y) @@ function true -> y | false -> x
      [@@inline]

    let incr_mod x max =
      I.(x := !!x + one) >>> fun () ->
      B.dispatch I.(!!x = max) @@ function
      | true -> I.(x := zero)
      | false -> M.unit
      [@@inline]

    let array_max array =
      let open Syntax in
      let* len = A.length array in
      let* acc = F.ref (A.get array I.zero) in
      ( for_ ~start:I.one ~stop:I.(len - one) @@ fun i ->
        F.(acc := max !!acc (A.get array i)) )
      >>> fun () -> F.(!!acc)
      [@@inline]
  end

  module Stopping = struct
    type t = grad:vec -> (unit -> bool Repr.m k) k

    let max_iter ~niter : t =
     fun ~grad:_ ->
      let*! step = F.(ref zero) in
      let code () =
        Monad.return
        @@ Syntax.(
             let* cond = F.(!!step < niter) in
             F.(step := !!step + one) >>> fun () -> cond)
      in
      Monad.return code

    let squared_gradient_norm ~bound : t =
     fun ~grad ->
      let code () =
        let* dot = Vec.dot grad grad in
        Monad.return F.(dot >= bound)
      in
      Monad.return code

    let gradient_norm ~bound : t =
     fun ~grad ->
      let code () =
        let* dot = Vec.dot grad grad in
        Monad.return F.(sqrt dot >= bound)
      in
      Monad.return code

    let conj (cond1 : t) (cond2 : t) : t =
     fun ~grad ->
      let* pred1 = cond1 ~grad in
      let* pred2 = cond2 ~grad in
      Monad.return (fun () ->
          let* res1 = pred1 () in
          Monad.return
            (B.dispatch res1 @@ function
             | false -> B.false_
             | true -> Monad.run (pred2 ())))

    let disj (cond1 : t) (cond2 : t) : t =
     fun ~grad ->
      let* pred1 = cond1 ~grad in
      let* pred2 = cond2 ~grad in
      Monad.return (fun () ->
          let* res1 = pred1 () in
          Monad.return
            (B.dispatch res1 @@ function
             | true -> B.true_
             | false -> Monad.run (pred2 ())))
  end

  module Proximal = struct
    (* proximal(x, tau) *)
    type t = vec -> elt m -> vec k

    let identity : t = fun vec _tau -> Monad.return vec

    let lasso_positive (lambda : elt m) : t =
     fun x tau ->
      let*! prod = F.mul lambda tau in
      Monad.return
      @@ (Vec.map (fun xi ->
              B.dispatch F.(xi >= prod) @@ function
              | true -> F.(xi - prod)
              | false -> F.zero))
           x
     [@@inline]

    let lasso_unconstrained lambda x tau =
      let*! prod = F.mul lambda tau in
      Monad.return
      @@ Vec.map
           (fun xi ->
             B.dispatch F.(xi >= prod) @@ function
             | true -> F.(xi - prod)
             | false -> (
                 B.dispatch F.(xi <= ~-prod) @@ function
                 | true -> F.(xi + prod)
                 | false -> F.zero))
           x
      [@@inline]

    let lasso ~positive ~lambda : t =
      if positive then lasso_positive lambda else lasso_unconstrained lambda
      [@@inline]

    let positive x _tau =
      Monad.return
      @@ Vec.map
           (fun xi ->
             B.dispatch F.(xi < zero) @@ function true -> F.zero | false -> xi)
           x
  end

  module Rate = struct
    type rate_function =
      position:vec ->
      descent:vec ->
      gradient:vec ->
      line_f:(F.t m -> vec k) ->
      step:F.t m ->
      F.t m k

    type t =
      initial:A.t m -> dimension:I.t m -> f:(A.t -> F.t) m -> rate_function k

    let constant ~rate : t =
     fun ~initial:_ ~dimension:_ ~f:_ ->
      Monad.return (fun ~position:_ ~descent:_ ~gradient:_ ~line_f:_ ~step:_ ->
          Monad.return rate)

    let decay ~initial_rate ~decay : t =
     fun ~initial:_ ~dimension:_ ~f:_ ->
      Monad.return
        F.(
          fun ~position:_ ~descent:_ ~gradient:_ ~line_f:_ ~step ->
            Monad.return @@ (initial_rate / (one + (decay * step))))

    (* Vanilla line search, using the Armijo-Goldstein condition *)
    let backtracking ~max_rate ~slack ~shrinking_factor ~max_iter : t =
     fun ~initial:_ ~dimension ~f ->
      let*! p = A.make dimension F.zero in
      let*! back_step = F.ref F.zero in
      let*! niter = I.ref I.zero in
      let p_ = Vec.out_of_array p in
      let code ~position ~descent ~gradient ~line_f ~step:_ =
        let* dot = Vec.dot descent gradient in
        let*! neg_grad_sq = F.(neg (mul slack dot)) in
        let*! f_x = f $ p in
        let continue () =
          Monad.run
          @@ let*! s = F.(!!back_step) in
             let* line_v = line_f s in
             V.(p_ := line_v) >> fun () ->
             let*! f_p = f $ p in
             (* Evaluate the Armijo-Goldstein condition; return
                true if it is _not_ verified. *)
             Monad.return
               B.(I.(!!niter < max_iter) && F.(f_p > f_x + (s * neg_grad_sq)))
        in
        let body () =
          I.(niter := !!niter + one) >>> fun () ->
          F.(back_step := !!back_step * shrinking_factor)
        in
        (* initialisation *)
        Vec.(p_ := position) >> fun () ->
        F.(back_step := max_rate) >>! fun () ->
        I.(niter := zero) >>! fun () ->
        Syntax.while_ ~cond:continue body >>! fun () ->
        Monad.return F.(!!back_step)
      in
      Monad.return code

    (* Modification of the vanilla line search where the
       [shrinking_factor] parameter for the _next_ call of the line search
       is set to [shrinking_factor^{niter+1}] where [niter] is the number
       of line search loops performed in the last call.

       The goal is to perform less iterations in the next call
       by starting closer to a point satisfying the Armijo-Goldstein condition.
       This works well when eg the step size is decreasing monotonically. *)
    let adaptive_backtracking ~max_rate ~slack ~shrinking_factor ~max_iter : t =
     fun ~initial:_ ~dimension ~f ->
      let*! p = A.make dimension F.zero in
      let*! back_step = F.ref F.zero in
      let*! niter = F.ref F.zero in
      let*! adaptive_shrinking = F.ref shrinking_factor in
      let p_ = Vec.out_of_array p in
      let code ~position ~descent ~gradient ~line_f ~step:_ =
        let* dot = V.dot descent gradient in
        let*! neg_grad_sq = F.(neg (mul slack dot)) in
        V.(p_ := position) >> fun () ->
        let*! f_x = f $ p in
        F.(back_step := max_rate) >>! fun () ->
        F.(niter := zero) >>! fun () ->
        let continue () =
          Monad.run
          @@ let*! s = F.(!!back_step) in
             let* line_v = line_f s in
             V.(p_ := line_v) >> fun () ->
             let*! f_p = f $ p in
             (* Evaluate the Armijo-Goldstein condition; return
                true if it is _not_ verified. *)
             Monad.return
               B.(F.(!!niter < max_iter) && F.(f_p > f_x + (s * neg_grad_sq)))
        in
        let body () =
          F.(niter := !!niter + one) >>> fun () ->
          F.(back_step := !!back_step * !!adaptive_shrinking)
        in
        Syntax.while_ ~cond:continue body >>! fun () ->
        F.(
          adaptive_shrinking := shrinking_factor * pow shrinking_factor !!niter)
        >>! fun () -> Monad.return F.(!!back_step)
      in
      Monad.return code

    (* Nonmonotone line search, adapted from the algorithm described in
       "A field guide to forward-backward splitting with a fasta implementation"
       by Goldstein et al.
       Taken originally from "A Nonmonotone Line Search Technique for Newton's Method"
         (Grippo et al.) *)
    let nonmonotone_line_search ~rolling_width ~max_rate ~shrinking_factor
        ~max_iter ~proximal : t =
     fun ~initial ~dimension ~f ->
      let*! position'_storage = A.make dimension F.zero in
      let*! delta_storage = A.make dimension F.zero in
      let*! back_step = F.ref F.zero in
      let*! niter = I.ref I.zero in
      let*! rolling_i = I.ref I.zero in
      let*! rolling = A.make rolling_width F.zero in
      let*! f_p' = F.ref F.zero in
      let (position', position'_) = Vec.of_array position'_storage in
      let (delta, delta_) = Vec.of_array delta_storage in
      (* initialisation *)
      F.(f_p' := f $ initial) >>! fun () ->
      A.set rolling I.(!!rolling_i) F.(!!f_p') >>! fun () ->
      Helpers.incr_mod rolling_i rolling_width >>! fun () ->
      let code ~position ~descent ~gradient:_ ~line_f:_ ~step:_ =
        let continue f_r () =
          Monad.run
          @@
          let open B in
          let* dot = Vec.dot delta descent in
          let* delta_sq = Vec.dot delta delta in
          Monad.return
            (I.(!!niter < max_iter)
            && F.(
                 !!f_p'
                 > f_r + dot + (one / (of_float 2. * F.(!!back_step)) * delta_sq))
            )
        in
        let proximal_step () =
          let* pos = V.Infix.(~!position - (F.(!!back_step) %* ~!descent)) in
          V.Infix.(position'_ := proximal pos F.(!!back_step))
        in
        let body () =
          Monad.run
            ( I.(niter := !!niter + one) >>! fun () ->
              F.(back_step := !!back_step * shrinking_factor) >>! fun () ->
              proximal_step () >> fun () ->
              F.(f_p' := f $ position'_storage) >>! fun () ->
              V.Infix.(delta_ := ~!position' - ~!position) )
        in
        (* per-call initialisation *)
        I.(niter := zero) >>! fun () ->
        F.(back_step := max_rate) >>! fun () ->
        proximal_step () >> fun () ->
        F.(f_p' := f $ position'_storage) >>! fun () ->
        V.Infix.(delta_ := ~!position' - ~!position) >> fun () ->
        let*! f_r = Helpers.array_max rolling in
        Syntax.while_ ~cond:(continue f_r) body >>! fun () ->
        A.set rolling I.(!!rolling_i) F.(!!f_p') >>! fun () ->
        Helpers.incr_mod rolling_i rolling_width >>! fun () ->
        Monad.return F.(!!back_step)
      in
      Monad.return code

    let nonmonotone_adaptive ~rolling_width ~max_rate ~shrinking_factor
        ~max_iter ~(proximal : Proximal.t) : t =
     fun ~initial ~dimension ~f ->
      let*! pos_storage = A.make dimension F.zero in
      let*! fpos_storage = A.make dimension F.zero in
      let*! bpos_storage = A.make dimension F.zero in
      let*! delta_storage = A.make dimension F.zero in
      let*! back_step = F.ref max_rate in
      let*! niter = I.ref I.zero in
      let*! rolling_i = I.ref I.zero in
      let*! rolling = A.make rolling_width F.zero in
      let*! f_val = F.ref F.zero in
      let (pos, pos_) = Vec.of_array pos_storage in
      let (fpos, fpos_) = Vec.of_array fpos_storage in
      let (bpos, bpos_) = Vec.of_array bpos_storage in
      let (delta, delta_) = Vec.of_array delta_storage in
      (* initialisation *)
      V.(pos_ := in_of_array initial) >> fun () ->
      V.(fpos_ := in_of_array initial) >> fun () ->
      V.(bpos_ := in_of_array initial) >> fun () ->
      F.(f_val := f $ initial) >>! fun () ->
      A.set rolling I.(!!rolling_i) F.(!!f_val) >>! fun () ->
      Helpers.incr_mod rolling_i rolling_width >>! fun () ->
      let code ~position ~descent ~gradient ~line_f:_ ~step:_ =
        let continue () =
          Monad.run
          @@
          let open B in
          let*! f_r = Helpers.array_max rolling in
          let* dot = Vec.dot delta descent in
          let* delta_sq = Vec.dot delta delta in
          Monad.return
            (I.(!!niter < max_iter)
            && F.(
                 !!f_val - of_float 1e-12
                 > f_r + dot + (one / (of_float 2. * !!back_step) * delta_sq)))
        in
        let proximal_step () =
          let*! shrink = F.(!!back_step) in
          V.Infix.(fpos_ := ~!position - (shrink %* ~!descent)) >> fun () ->
          V.Infix.(bpos_ := proximal fpos shrink)
        in
        let body () =
          Monad.run
            ( I.(niter := !!niter + one) >>! fun () ->
              F.(back_step := !!back_step * shrinking_factor) >>! fun () ->
              proximal_step () >> fun () ->
              F.(f_val := f $ bpos_storage) >>! fun () ->
              V.Infix.(delta_ := ~!bpos - ~!position) )
        in
        (* per-call initialisation
           ----------------------- *)
        I.(niter := zero) >>! fun () ->
        let*! shrink = F.(!!back_step) in
        (* estimate subgradient of g *)
        let* grad_g =
          V.Infix.(~!gradient + (F.(one / shrink) %* (~!fpos - ~!pos)))
        in
        let* dotprod = V.dot grad_g delta in
        let* tau_s =
          let* delta_sq = V.dot delta delta in
          Monad.return F.(delta_sq / dotprod)
        in
        let* tau_m =
          let* grad_g_sq = V.dot grad_g grad_g in
          Monad.return (Helpers.max F.zero F.(dotprod / grad_g_sq))
        in
        V.(pos_ := position) >> fun () ->
        (B.dispatch F.(of_float 2. * tau_m > tau_s) @@ function
         | true -> F.(back_step := tau_m)
         | false -> F.(back_step := tau_s - (of_float 0.5 * tau_m)))
        >>! fun () ->
        (* first iteration *)
        (B.dispatch F.(!!back_step <= zero) @@ function
         | true -> F.(back_step := shrink * of_float 1.5)
         | false -> M.unit)
        >>! fun () ->
        proximal_step () >> fun () ->
        F.(f_val := f $ bpos_storage) >>! fun () ->
        V.Infix.(delta_ := ~!bpos - ~!position) >> fun () ->
        Syntax.while_ ~cond:continue body >>! fun () ->
        (* update rolling max *)
        A.set rolling I.(!!rolling_i) F.(!!f_val) >>! fun () ->
        Helpers.incr_mod rolling_i rolling_width >>! fun () ->
        Monad.return F.(!!back_step)
      in
      Monad.return code
  end

  module Momentum = struct
    type direction_update = step:F.t m -> unit m k

    type gradient_update = unit -> unit m

    type update =
      { direction_update : direction_update; gradient_update : gradient_update }

    type t =
      dimension:I.t m ->
      f_grad:(A.t -> A.t -> unit) m ->
      grad:array_with_overlay ->
      pos:array_with_overlay ->
      dir:array_with_overlay ->
      compute_rate:Rate.rate_function ->
      update k

    let none : t =
     fun ~dimension:_
         ~f_grad
         ~grad:(gradient_storage, gradient, _)
         ~pos:(position_storage, position, _)
         ~dir:(_, _, direction_)
         ~compute_rate ->
      let direction_update ~step =
        let* rate =
          compute_rate
            ~position
            ~gradient
            ~descent:gradient
            ~line_f:(fun s ->
              let scaled_gradient = V.smul s gradient in
              V.(sub position scaled_gradient))
            ~step
        in
        V.Infix.(direction_ := rate %* ~!gradient)
      in
      let gradient_update () = f_grad $ position_storage $ gradient_storage in
      Monad.return { direction_update; gradient_update }

    let basic ~momentum : t =
     fun ~dimension:_
         ~f_grad
         ~grad:(gradient_storage, gradient, _)
         ~pos:(position_storage, position, _)
         ~dir:(_, direction, direction_)
         ~compute_rate ->
      let direction_update ~step =
        let* rate =
          compute_rate
            ~position
            ~descent:gradient
            ~gradient
            ~line_f:(fun s -> V.Infix.(~!position - (s %* ~!gradient)))
            ~step
        in
        V.Infix.(
          direction_ := ~-(momentum %* ~!direction) + (rate %* ~!gradient))
      in
      let gradient_update () = f_grad $ position_storage $ gradient_storage in
      Monad.return { direction_update; gradient_update }

    let nesterov ~momentum : t =
     fun ~dimension
         ~f_grad
         ~grad:(gradient_storage, gradient, _)
         ~pos:(_, position, _)
         ~dir:(_, direction, direction_)
         ~compute_rate ->
      let*! u_storage = A.(make dimension F.zero) in
      let u_ = Vec.out_of_array u_storage in
      let direction_update ~step =
        let* rate =
          compute_rate
            ~position (* TODO: how to adapt line search *)
            ~descent:gradient
            ~gradient
            ~line_f:(fun s ->
              let scaled_gradient = V.smul s gradient in
              let scaled_dir = V.smul momentum direction in
              let* tmp = V.sub position scaled_gradient in
              V.add tmp scaled_dir)
            ~step
        in
        V.Infix.(
          let scaled_dir = momentum %* ~!direction in
          (u_ := ~!position + scaled_dir) >> fun () ->
          direction_ := ~-scaled_dir + (rate %* ~!gradient))
      in
      let gradient_update () = f_grad $ u_storage $ gradient_storage in
      Monad.return { direction_update; gradient_update }

    let adagrad : t =
     fun ~dimension
         ~f_grad
         ~grad:(gradient_storage, gradient, _)
         ~pos:(position_storage, position, _)
         ~dir:(_, direction, direction_)
         ~compute_rate ->
      let*! gradient_acc_storage = A.(make dimension F.zero) in
      let (gradient_acc, gradient_acc_) = Vec.of_array gradient_acc_storage in
      let direction_update ~step =
        V.Infix.(
          (gradient_acc_ := ~!gradient_acc + (~!gradient * ~!gradient))
          >> fun () ->
          (direction_ :=
             V.map2
               F.(fun acc grad -> one / sqrt (acc + of_float 1e-16) * grad)
               gradient_acc
               gradient)
          >> fun () ->
          let* rate =
            compute_rate
              ~position
              ~descent:direction
              ~gradient
              ~line_f:(fun s -> ~!position - (s %* ~!direction))
              ~step
          in
          direction_ := rate %* ~!direction)
      in
      let gradient_update () = f_grad $ position_storage $ gradient_storage in
      Monad.return { direction_update; gradient_update }

    let rmsprop ~decay : t =
     fun ~dimension
         ~f_grad
         ~grad:(gradient_storage, gradient, _)
         ~pos:(position_storage, position, _)
         ~dir:(_, direction, direction_)
         ~compute_rate ->
      let*! gradient_acc_storage = A.(make dimension F.zero) in
      let (gradient_acc, gradient_acc_) = Vec.of_array gradient_acc_storage in
      let direction_update ~step =
        V.Infix.(
          (gradient_acc_ :=
             (decay %* ~!gradient_acc)
             + (F.(one - decay) %* (~!gradient * ~!gradient)))
          >> fun () ->
          (direction_ :=
             V.map2
               F.(fun acc grad -> one / sqrt (acc + of_float 1e-16) * grad)
               gradient_acc
               gradient)
          >> fun () ->
          let* rate =
            compute_rate
              ~position
              ~descent:direction
              ~gradient
              ~line_f:(fun s -> ~!position - (s %* ~!direction))
              ~step
          in
          direction_ := rate %* ~!direction)
      in
      let gradient_update () = f_grad $ position_storage $ gradient_storage in
      Monad.return { direction_update; gradient_update }

    let adam ~beta1 ~beta2 : t =
     fun ~dimension
         ~f_grad
         ~grad:(gradient_storage, gradient, _)
         ~pos:(position_storage, position, _)
         ~dir:(_, direction, direction_)
         ~compute_rate ->
      let*! first_moment_storage = A.(make dimension F.zero) in
      let*! second_moment_storage = A.(make dimension F.zero) in
      let*! beta1_powers = F.ref F.one in
      let*! beta2_powers = F.ref F.one in
      let (first_moment, first_moment_) = Vec.of_array first_moment_storage in
      let (second_moment, second_moment_) =
        Vec.of_array second_moment_storage
      in
      let direction_update ~step =
        F.(beta1_powers := !!beta1_powers * beta1) >>! fun () ->
        F.(beta2_powers := !!beta2_powers * beta2) >>! fun () ->
        V.Infix.(
          (first_moment_ :=
             (beta1 %* ~!first_moment) + (F.(one - beta1) %* ~!gradient))
          >> fun () ->
          (second_moment_ :=
             (beta2 %* ~!second_moment)
             + (F.(one - beta2) %* ~!gradient * ~!gradient))
          >> fun () ->
          let* unbiased_first_moment =
            F.(one / (one - !!beta1_powers + of_float 1e-16)) %* ~!first_moment
          in
          let* unbiased_second_moment =
            F.(one / (one - !!beta2_powers + of_float 1e-16)) %* ~!second_moment
          in
          (direction_ :=
             V.map2
               F.(
                 fun first_moment second_moment ->
                   one / sqrt (second_moment + of_float 1e-16) * first_moment)
               unbiased_first_moment
               unbiased_second_moment)
          >> fun () ->
          let* rate =
            compute_rate
              ~position
              ~descent:direction
              ~gradient
              ~line_f:(fun s -> ~!position - (s %* ~!direction))
              ~step
          in
          direction_ := rate %* ~!direction)
      in
      let gradient_update () = f_grad $ position_storage $ gradient_storage in
      Monad.return { direction_update; gradient_update }
  end

  type objective_fun = arr -> elt

  type gradient_fun = arr -> arr -> unit

  module Raw = struct
    let gradient_descent ~(stopping : Stopping.t) ~(rate : Rate.t)
        ~(momentum : Momentum.t) (f : objective_fun m) (f_grad : gradient_fun m)
        x0 =
      let*! dimension = A.length x0 in
      let*! position_storage = A.copy x0 in
      let*! gradient_storage = A.make dimension F.zero in
      let*! direction_storage = A.make dimension F.zero in
      let*! step = F.ref F.zero in
      let (position, position_) = Vec.of_array position_storage in
      let (gradient, gradient_) = Vec.of_array gradient_storage in
      let (direction, direction_) = Vec.of_array direction_storage in
      let* compute_rate = rate ~initial:x0 ~dimension ~f in
      let* cond = stopping ~grad:gradient in
      let* { direction_update; gradient_update } =
        (((((momentum ~dimension [@inlined]) ~f_grad [@inlined])
             ~grad:(gradient_storage, gradient, gradient_) [@inlined])
            ~pos:(position_storage, position, position_) [@inlined])
           ~dir:(direction_storage, direction, direction_) [@inlined])
          ~compute_rate
      in
      f_grad $ position_storage $ gradient_storage >>! fun () ->
      Syntax.while_
        ~cond:(fun () -> Monad.run (cond ()))
        (fun () ->
          Monad.run
          @@ let*! current_step = F.(!!step) in
             direction_update ~step:current_step >> fun () ->
             V.Infix.(position_ := ~!position - ~!direction) >> fun () ->
             gradient_update () >>! fun () ->
             Monad.return F.(step := current_step + one))
      >>! fun () -> Monad.return position_storage
      [@@inline]

    (* basic forward-backward splitting scheme *)
    let forward_backward ~rolling_width ~max_rate ~shrinking_factor ~max_iter
        ~(stopping : Stopping.t) ~proximal f f_grad x0 =
      let*! dimension = A.length x0 in
      let*! fpos_storage = A.make dimension F.zero in
      let*! bpos_storage = A.make dimension F.zero in
      let*! grad_storage = A.make dimension F.zero in
      let*! step = F.ref F.zero in
      let gradient = Vec.in_of_array grad_storage in
      let (fpos, fpos_) = Vec.of_array fpos_storage in
      let (bpos, bpos_) = Vec.of_array bpos_storage in
      let* cond = stopping ~grad:gradient in
      let* compute_rate =
        Rate.nonmonotone_adaptive
          ~rolling_width
          ~max_rate
          ~shrinking_factor
          ~max_iter
          ~proximal
          ~initial:x0
          ~dimension
          ~f
      in
      V.(fpos_ := in_of_array x0) >> fun () ->
      V.(bpos_ := in_of_array x0) >> fun () ->
      f_grad $ bpos_storage $ grad_storage >>! fun () ->
      Syntax.while_
        ~cond:(fun () -> Monad.run (cond ()))
        (fun () ->
          Monad.run
            ( V.Infix.(
                let* rate =
                  compute_rate
                    ~position:bpos
                    ~descent:gradient
                    ~gradient
                    ~line_f:(fun _ -> assert false)
                    ~step:F.(!!step)
                in
                (fpos_ := ~!bpos - (rate %* ~!gradient)) >> fun () ->
                V.Infix.(bpos_ := proximal fpos rate))
            >> fun () ->
              F.(step := !!step + one) >>! fun () ->
              Monad.return (f_grad $ bpos_storage $ grad_storage) ))
      >>! fun () -> Monad.return bpos_storage

    (* Adapted from the implementation by Eric Chi
       https://cran.r-project.org/web/packages/fasta/index.html
       See "A FIELD GUIDE TO FORWARD-BACKWARD SPLITTINGWITH A FASTA IMPLEMENTATION" by Goldstein et al *)
    let fasta ~rolling_width ~max_rate ~shrinking_factor ~max_iter
        ~(stopping : Stopping.t) ~proximal f f_grad x0 =
      let*! dimension = A.length x0 in
      (* -------- storage allocation -------- *)
      let*! pos_storage = A.make dimension F.zero in
      let*! fpos_storage = A.make dimension F.zero in
      let*! bpos_storage = A.make dimension F.zero in
      let*! grad_storage = A.make dimension F.zero in
      let*! dpos_storage = A.make dimension F.zero in
      let*! back_step = F.ref max_rate in
      let*! step = F.ref F.zero in
      let*! niter = I.ref I.zero in
      let*! rolling_i = I.ref I.zero in
      let*! rolling = A.make rolling_width F.zero in
      let*! f_val = F.ref F.zero in
      (* -------- overlay initialisation -------- *)
      let gradient = Vec.in_of_array grad_storage in
      let (pos, pos_) = Vec.of_array pos_storage in
      let (fpos, fpos_) = Vec.of_array fpos_storage in
      let (bpos, bpos_) = Vec.of_array bpos_storage in
      let (delta, delta_) = Vec.of_array dpos_storage in
      let residual =
        V.make (T.rank_one dimension) (fun i ->
            let open M in
            let* eps = F.(!!back_step) in
            F.(V.unsafe_get delta i / (eps * eps)))
      in
      (* -------- prepare stopping predicate -------- *)
      let* cond = stopping ~grad:residual in
      (* Initialise delta to a non-zero value, in order not to trigger
         the termination condition on the first iteration. *)
      V.(delta_ := one (T.rank_one dimension)) >> fun () ->
      (* -------- storage initialisation -------- *)
      V.(pos_ := in_of_array x0) >> fun () ->
      V.(fpos_ := in_of_array x0) >> fun () ->
      V.(bpos_ := in_of_array x0) >> fun () ->
      F.(f_val := f $ x0) >>! fun () ->
      f_grad $ pos_storage $ grad_storage >>! fun () ->
      A.set rolling I.(!!rolling_i) F.(!!f_val) >>! fun () ->
      Helpers.incr_mod rolling_i rolling_width >>! fun () ->
      (* -------- main computation loop -------- *)
      Syntax.while_
        ~cond:(fun () -> Monad.run (cond ()))
        (fun () ->
          Monad.run
            (let*! rate = F.(!!back_step) in
             V.(pos_ := bpos) >> fun () ->
             (* perform forward_backward step *)
             V.Infix.(fpos_ := ~!bpos - (rate %* ~!gradient)) >> fun () ->
             V.Infix.(bpos_ := proximal fpos rate) >> fun () ->
             (* prepare backtracking search *)
             V.Infix.(delta_ := ~!bpos - ~!pos) >> fun () ->
             F.(f_val := f $ bpos_storage) >>! fun () ->
             let*! f_r = Helpers.array_max rolling in
             I.(niter := zero) >>! fun () ->
             (* backtracking search loop *)
             Syntax.while_
               ~cond:(fun () ->
                 Monad.run
                 @@
                 let open B in
                 let* dot = Vec.dot delta gradient in
                 let* grad_sq = V.dot delta delta in
                 Monad.return
                   (I.(!!niter < max_iter)
                   && F.(
                        !!f_val - of_float 1e-12
                        > f_r + dot
                          + (one / (of_float 2. * !!back_step) * grad_sq))))
               (fun () ->
                 Monad.run
                   ( I.(niter := !!niter + one) >>! fun () ->
                     F.(back_step := !!back_step * shrinking_factor)
                     >>! fun () ->
                     let*! shrink = F.(!!back_step) in
                     (* internal forward-backward step *)
                     V.Infix.(fpos_ := ~!pos - (shrink %* ~!gradient))
                     >> fun () ->
                     V.Infix.(bpos_ := proximal fpos shrink) >> fun () ->
                     F.(f_val := f $ bpos_storage) >>! fun () ->
                     V.Infix.(delta_ := ~!bpos - ~!pos) ))
             >>! fun () ->
             (* update ring buffer *)
             A.set rolling I.(!!rolling_i) F.(!!f_val) >>! fun () ->
             Helpers.incr_mod rolling_i rolling_width >>! fun () ->
             (* re-evaluate gradient *)
             f_grad $ bpos_storage $ grad_storage >>! fun () ->
             F.(step := !!step + one) >>! fun () ->
             (* compute next stepsize using BB adaptive rule *)
             let*! shrink = F.(!!back_step) in
             let* grad_g =
               V.Infix.(~!gradient + (F.(one / shrink) %* (~!fpos - ~!pos)))
             in
             (* when we're near convergence, [tau_s] can become [nan], hence the check at the end. *)
             let* dotprod = V.dot grad_g delta in
             let* delta_sq = V.dot delta delta in
             let*! tau_s = F.(delta_sq / dotprod) in
             let* grad_g_sq = V.dot grad_g grad_g in
             let*! tau_m = Helpers.max F.zero F.(dotprod / grad_g_sq) in
             (B.dispatch F.(of_float 2. * tau_m > tau_s) @@ function
              | true -> F.(back_step := tau_m)
              | false -> F.(back_step := tau_s - (of_float 0.5 * tau_m)))
             >>! fun () ->
             let*! tau = F.(!!back_step) in
             (B.(dispatch F.(tau <= zero || is_nan tau || is_inf tau))
              @@ function
              | true -> F.(back_step := shrink * of_float 1.5)
              | false -> M.unit)
             >>! fun () -> Monad.return M.unit))
      >>! fun () -> Monad.return pos_storage
  end

  let gradient_descent ~stopping ~rate ~momentum =
    L.lam @@ fun f ->
    L.lam @@ fun f_grad ->
    L.lam @@ fun x0 ->
    Monad.run @@ Raw.gradient_descent ~stopping ~rate ~momentum f f_grad x0

  (* basic forward-backward splitting scheme *)
  let forward_backward ~rolling_width ~max_rate ~shrinking_factor ~max_iter
      ~(stopping : Stopping.t) ~proximal =
    L.lam @@ fun f ->
    L.lam @@ fun f_grad ->
    L.lam @@ fun x0 ->
    Monad.run
    @@ Raw.forward_backward
         ~rolling_width
         ~max_rate
         ~shrinking_factor
         ~max_iter
         ~stopping
         ~proximal
         f
         f_grad
         x0

  let fasta ~rolling_width ~max_rate ~shrinking_factor ~max_iter
      ~(stopping : Stopping.t) ~proximal =
    L.lam @@ fun f ->
    L.lam @@ fun f_grad ->
    L.lam @@ fun x0 ->
    Monad.run
    @@ Raw.fasta
         ~rolling_width
         ~max_rate
         ~shrinking_factor
         ~max_iter
         ~stopping
         ~proximal
         f
         f_grad
         x0
end
[@@inline]
