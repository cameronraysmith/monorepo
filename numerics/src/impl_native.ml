open Numerics_intf
module BL = Basic_impl.Lang

module Scalar : Scalar with type 'a m = 'a and type t = float = struct
  include BL.Float

  let ( + ) = add

  let ( - ) = sub

  let ( * ) = mul

  let ( / ) = div

  let pow_cst = pow

  let of_float x = x

  let is_nan = Float.is_nan

  let is_inf = Float.is_infinite
end

include
  Impl_generic.Make (BL.Empty) (BL.Codegen) (Linalg.Tensor.Int) (BL.Bool)
    (Scalar)
    (BL.Float)
    (BL.Make_storage (struct
      type t = float
    end))
    (Grad.Native_impl.Node_state_enum)
    (Grad.Native_impl.Node_state_storage)
    (BL.Int)
    (BL.Int)
    (BL.Make_storage (struct
      type t = int
    end))
    (BL.Make_const (struct
      type t = int
    end))
    (BL.Exn)
    (BL.Loop)
    (BL.Sequencing)
    (BL.Lambda)
    (BL.Product)
    (Dense.Float_array.Native)
