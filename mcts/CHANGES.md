## 0.0.2
- Breaking: switch to a monadic interface
- Breaking: incremental API
- Proper doc

## 0.0.1
- First release
