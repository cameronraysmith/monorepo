## 0.0.2
- Expose bandit module type
- Expose `find_best_arm`
- Cleaner pretty-printing of statistics
- Fix bug arising when all rewards were equal to 0
- Fix NaN
- Check that rewards are in unit interval

## 0.0.1
- First release
