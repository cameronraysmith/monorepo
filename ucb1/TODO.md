x fix nans when setting rewards to 0.0
x better pretty-printing
- can have rewards in [0, x] interval if we rescale the radius term by x
x bugfix in [find_best_arm] when all arms have 0 rewards
x expose function to return arm with best empirical reward
